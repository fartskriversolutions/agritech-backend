# README #

### What is this repository for? ###

* Agritex project summary
* Version: 0.1.0-snapshot

### How do I get set up? ###

#### Database

* set up postgres database
* create user agritech with password agritech

#### Build
* run from project dir:  ./gradlew bootRun 
* build spring-boot jar: ./gradlew bootRepackage - out file will in /build/libs/agritex.jar

#### Defaults
* after 1st start in system will create tenant with name admin 
* admin username admin_super with password 12345678

#### Teants
* now tenants can be add to system only using database query: insert into tenants (id,name,enabled) into default schema
* after restart system - required schema will created

#### development servers fro backed
* now development server serv 2 domain http://atgl.agritex-dev.tk and http://ogf.agritex-dev.tk
* api swagger documentation can be foud at /swagger-ui.html on any server

#### Authentication 
* authentication api endpoints are not represents by swagger documentator
* default time for token now is 12h, for refresh token is 24h 
* auth request example (POST /api/auth/login): 

```

Request method:	POST
Request path:	http://some.domain/api/auth/login
Headers:	X-Requested-With=XMLHttpRequest
		Accept=*/*
		Content-Type=application/json; charset=UTF-8
Body:
{
    "username": "some_user",
    "password": "very_secred_password"
}
```

* valid response (http code 200)

```
#!json

{
"token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbl9zdXBlciIsInNjb3BlcyI6WyJST0xFX0FETUlOIl0sInRlbmFudF9pZCI6ImF0Z2wiLCJpc3MiOiJodHRwOi8vY2xvdWRjb3JlLm5vIiwiaWF0IjoxNDg0NTY0OTg2LCJleHAiOjE0ODQ2NTEzODZ9.sQjWjWcvsF_5kRLWStPf306q62uD87fmWvV7ML1DaX-xdtR1SZutAYuBZ-WICaQOWpAlFDR3jedFmlWz43fx1g",
"refreshToken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbl9zdXBlciIsInNjb3BlcyI6WyJST0xFX1JFRlJFU0hfVE9LRU4iXSwidGVuYW50X2lkIjoiYXRnbCIsImlzcyI6Imh0dHA6Ly9jbG91ZGNvcmUubm8iLCJqdGkiOiIwNDFkYTNlYS0wMDhlLTRkY2YtODJlOS1mMThmODk3OGQ1MzMiLCJpYXQiOjE0ODQ1NjQ5ODcsImV4cCI6MTQ4NDczNzc4N30.EMMQo3r8YxeKkvr8zShWi72ac7G0sPH6NheFU_86VlqfRN2egLK0JH1oFvvNOJL7G_lKdoER33gZEszxwEhVCA"
}
```

* invalid response (http code 401)


```
#!json

{
"status": 401,
"message": "Invalid username or password",
"errorCode": "AUTHENTICATION",
"timestamp": 1484565128966
}
```
* Refresh token is long-lived token used to request new Access tokens. It's expiration time is greater than expiration time of Access token.
* Refresh token request (GET /api/auth/token) 
```
Request method:	GET
Request path:	http://some.domain/api/auth/token
Headers:	X-Requested-With=XMLHttpRequest
		X-Authorization=Bearer <refresh token content>

```

* valid response (new valid token)
```
#!json
{
"token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbl9zdXBlciIsInNjb3BlcyI6WyJST0xFX0FETUlOIl0sInRlbmFudF9pZCI6ImF0Z2wiLCJpc3MiOiJodHRwOi8vY2xvdWRjb3JlLm5vIiwiaWF0IjoxNDg0NTY4NTM5LCJleHAiOjE0ODQ2NTQ5Mzl9.OKep3cTxycUnkNCSzVxn1ccxFGf1OvHJaxUWBKYgCpZkySflixs2xVZM3ND-T6C3499ggSENrkLSFIPKw9-EFg"
}
```

#### Sample api request
```
Request method:	GET
Request path:	http://some.domain/api/v1/stub
Headers:	X-Requested-With=XMLHttpRequest
		X-Authorization=Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbl9zdXBlciIsInNjb3BlcyI6WyJST0xFX0FETUlOIl0sInRlbmFudF9pZCI6ImFkbWluIiwiaXNzIjoiaHR0cDovL2Nsb3VkY29yZS5ubyIsImlhdCI6MTQ4NDU2NDkyOSwiZXhwIjoxNDg0NjUxMzI5fQ.ewQmocWvbrOnxbJwACLD64zLZ5YPgV8B6RBw9TDiRq3SmSenqI6v6XWILLeAZh_oGPCNyZ1H3-XC92D80Dk-Ng
		Accept=*/*
		Content-Type=application/json; charset=UTF-8
```