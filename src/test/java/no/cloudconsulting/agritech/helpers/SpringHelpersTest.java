package no.cloudconsulting.agritech.helpers;

import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.FarmField;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
/**
 * Created by denz0x13 on 20.01.17.
 */
public class SpringHelpersTest {
    @Test
    public void testGetChildClass() throws Exception {
        assertEquals(SpringHelpers.getChildClass(Cluster.class), Farm.class);
        assertEquals(SpringHelpers.getChildClass(Farm.class), FarmField.class);
    }

}