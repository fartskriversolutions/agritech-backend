package no.cloudconsulting.agritech.helpers;

import com.fasterxml.jackson.databind.JsonNode;
import no.cloudconsulting.agritech.datatypes.AbstractJsonType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.testng.Assert.*;

/**
 * Created by denz0x13 on 13.01.17.
 */

public class ExtractorHelperTest {


    @Test
    public void testNormalizeParams() throws Exception {
        String params = "p[][a]=a&p[][b]=b&p[][c]=c";
        ExtractorHelper.parseQueryString(params);
    }


    String _int_string = "1234568";
    String _float_string="123456.98";

    MultiValueMap<String,String> getParamsMap(){
        MultiValueMap<String,String> _result = new LinkedMultiValueMap<String,String>(){{
            add("int_param",_int_string);
            add("float_param",_float_string);
            add("string_param","string");
            add("array",_int_string);
            add("array",_float_string);
            add("array","string");
            add("array[]","val1");
            add("array[]","val2");
        }};

        return _result;
    }

    @Test
    public void testIsInteger() throws Exception {
        assertTrue(ExtractorHelper.isInteger(_int_string));
        assertFalse(ExtractorHelper.isFloat(_int_string));
    }

    @Test
    public void testIsFloat() throws Exception {
        assertTrue(ExtractorHelper.isFloat(_float_string));
        assertFalse(ExtractorHelper.isInteger(_float_string));
    }

    @Test
    public void testExtractObject() throws Exception {
        assertTrue(ExtractorHelper.extractObject(_int_string).getClass().isAssignableFrom(Long.class));
        assertTrue(ExtractorHelper.extractObject(_float_string).getClass().isAssignableFrom(Double.class));
    }

    @Test
    public void testExtractFromMap() throws Exception {
        //List<String> keys = Arrays.asList("int_param","float_param","string_param","array");
        MultiValueMap<String,String> paramsMap = getParamsMap();
        HashMap _result = ExtractorHelper.extractFromMap(paramsMap);
        assertNotNull(_result.get("int_param"));
        assertTrue(_result.get("int_param").getClass().isAssignableFrom(Long.class));
        assertNotNull(_result.get("float_param"));
        assertTrue(_result.get("float_param").getClass().isAssignableFrom(Double.class));
        assertNotNull(_result.get("string_param"));
        assertTrue(_result.get("string_param").getClass().isAssignableFrom(String.class));
        assertNotNull(_result.get("array"));
        assertTrue(_result.get("array").getClass().isAssignableFrom(ArrayList.class));
        assertEquals(((ArrayList)_result.get("array")).size(),
                paramsMap.get("array").size()+paramsMap.get("array[]").size());
    }

    @Test
    public void testAsJsonType() throws Exception {
        AbstractJsonType _result = ExtractorHelper.asJsonType(getParamsMap());
        assertNotNull(_result);
        JsonNode jsonNode = _result.asJson();
        assertNotNull(jsonNode);
    }

    @Test
    public void testIsHashKey() throws Exception {
        assertTrue(ExtractorHelper.isHashKey("hash[key]"));
        assertTrue(ExtractorHelper.isHashKey("hash[key1]"));
        assertTrue(ExtractorHelper.isHashKey("hash[1]"));
        assertFalse(ExtractorHelper.isHashKey("hash[]"));
    }

    @Test
    public void testIsValidKey() throws Exception {
        assertTrue(ExtractorHelper.isValidKey("key1"));
        assertTrue(ExtractorHelper.isValidKey("key_1"));
        assertTrue(ExtractorHelper.isValidKey("k1_0k"));
        assertFalse(ExtractorHelper.isValidKey("1_0k"));
        assertFalse(ExtractorHelper.isValidKey("1_0k[]"));
        assertFalse(ExtractorHelper.isValidKey("1_0k[2]"));
    }

    @Test
    public void testIsArrayKey() throws Exception {
        assertTrue(ExtractorHelper.isArrayKey("key1[]"));
        assertFalse(ExtractorHelper.isArrayKey("key1"));
        assertFalse(ExtractorHelper.isArrayKey("key1["));
        assertFalse(ExtractorHelper.isArrayKey("key1]"));

    }

}