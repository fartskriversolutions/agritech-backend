package no.cloudconsulting.agritech;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.specification.RequestSpecification;
import no.cloudconsulting.agritech.config.aaa.WebSecurityConfig;
import no.cloudconsulting.agritech.config.aaa.auth.jwt.extractor.JwtHeaderTokenExtractor;
import no.cloudconsulting.agritech.config.aaa.model.AccessJwtToken;
import no.cloudconsulting.agritech.config.aaa.model.JwtTokenFactory;
import no.cloudconsulting.agritech.config.aaa.pojo.LoginForm;
import no.cloudconsulting.agritech.config.aaa.pojo.LoginResponse;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.services.BaseService;
import no.cloudconsulting.agritech.domain.services.ServiceException;
import no.cloudconsulting.agritech.domain.services.UserManagementService;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import no.cloudconsulting.agritech.tenant.TenantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import javax.servlet.http.HttpServletResponse;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by denz0x13 on 06.12.16.
 */
@SpringBootTest(classes = {AppMain.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class TestBase extends AbstractTestNGSpringContextTests {

    public final Logger getLogger(){
        return LoggerFactory.getLogger(this.getClass());
    }

    @Autowired
    private TenantService tenantService;

    public final String _default_tenant = "admin";

    @Autowired
    private JwtTokenFactory tokenFactory;


    @Autowired
    private UserManagementService userManagementService;

    @Value("${local.server.port}")
    int port;

    public int getPort(){
        return port;
    }

    public User getUserByLogin(String login,String tenant){
        String _ct = TenantHelper.getCurrentTenantName();
        TenantHelper.setTenant(tenant);
        User user = userManagementService.getByUsername(login);
        assertNotNull(user);
        TenantHelper.setTenant(_ct);
        return user;
    }

    public  User getUserByLogin(String login){
        return getUserByLogin(login,_default_tenant);
    }

    public User loginUser(String login,String tenant){
        User user = getUserByLogin(login,tenant);
        String _ct = TenantHelper.getCurrentTenantName();
        TenantHelper.setTenant(tenant);
        user = userManagementService.loginUser(user);
        TenantHelper.setTenant(_ct);
        return user;
    }

    public AccessJwtToken loginApiUser(String login,String tenant){
        User user = getUserByLogin(login,tenant);
        String _ct = TenantHelper.getCurrentTenantName();
        TenantHelper.setTenant(tenant);
        user = userManagementService.loginUser(user);
        assertNotNull(user);
        AccessJwtToken jwt = tokenFactory.createAccessJwtToken(user);
        assertNotNull(jwt);
        TenantHelper.setTenant(_ct);
        return jwt;
    }

    public AccessJwtToken loginApiUser(String login){
        return loginApiUser(login,_default_tenant);
    }



    public void logoutUser(){
        userManagementService.logoutUser();
    }

    public User loginUser(String login){
        return loginUser(login,_default_tenant);
    }

    public <T> T createEntity(BaseService service,BaseApiPojo pojo,String tenant) throws ServiceException {
        String _ct = TenantHelper.getCurrentTenantName();
        TenantHelper.setTenant(tenant);
        T e = (T)service.create(pojo);
        TenantHelper.setTenant(_ct);
        return  e;
    }

    public <T> T createEntity(BaseService service,BaseApiPojo pojo) throws ServiceException {
        return createEntity(service,pojo,_default_tenant);
    }

    public Header getXHTTPHeader(){
        return new Header("X-Requested-With","XMLHttpRequest");
    }


    public void setUpServer4Tenant(String tenant){
        RestAssured.baseURI = String.format("http://%s.lvh.me",tenant);
        RestAssured.port = getPort();
//        TenantHelper.setTenant(tenant);
//        userManagementService.checkUsers();
//        TenantHelper.setDefaultTenant();

    }

    public RequestSpecification givenAjax(){
        return given().log().all().contentType(ContentType.JSON).
                header(getXHTTPHeader());
    }

    public RequestSpecification givenAjax(String token){
        return given().log().all().
                contentType(ContentType.JSON).
                header(getXHTTPHeader()).
                header(new Header(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM, JwtHeaderTokenExtractor.HEADER_PREFIX+token));
    }

    public void setUpServer4Tenant(){
        setUpServer4Tenant(_default_tenant);
    }



    public LoginResponse login(String username, String password){
        return givenAjax().
                body(new LoginForm(username,password)).
                when().
                post(WebSecurityConfig.FORM_BASED_LOGIN_ENTRY_POINT).
                then().
                statusCode(HttpServletResponse.SC_OK).extract().as(LoginResponse.class);
    }

    public void createTenant(String tenant) throws Exception{
        TenantHelper.setDefaultTenant();
        tenantService.createTenant(tenant,true);
        assertTrue(tenantService.getStatus(tenant).equals(TenantService.TenantStatus.PRESENT));
        //tenantService.setupTenant(tenant);
    }

    @BeforeClass
    public void gSetUp() throws Exception {
        FixtureFactoryLoader.loadTemplates("no.cloudconsulting.agritech.fixtures");
    }

    @BeforeMethod
    public void setUp() throws Exception {
        createTenant("admin");

    }

    @AfterMethod
    public void tearDown() throws Exception {
        TenantHelper.setDefaultTenant();
        tenantService.getAllTenants().stream().forEach(t -> {
            tenantService.dropTenant(t.tenantId);
        });
    }

//    @Test
//    public void runApp() throws Exception {
//
//        getLogger().info("test");
//    }
}
