package no.cloudconsulting.agritech.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import no.cloudconsulting.agritech.domain.model.Role;
import no.cloudconsulting.agritech.domain.model.RolePermission;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.UserApiPojo;
import org.springframework.security.acls.domain.BasePermission;

import java.util.ArrayList;

/**
 * Created by denz0x13 on 28.12.16.
 */
public class UserTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(User.class).addTemplate("valid", new Rule(){{
            add("loginName",random("user1","user2"));
            add("firstName",random("User1","User2"));
            add("lastName",random("Last1","Last2"));
            add("email","${loginName}@example.com");
            add("password","12345678");
        }});

        Fixture.of(Role.class).addTemplate("valid", new Rule(){{
            add("name","SOME_ROLE");
        }});

        Fixture.of(RolePermission.class).addTemplate("admin", new Rule(){{
            add("mask", BasePermission.ADMINISTRATION.getMask());
            add("granting",true);

        }});


        Fixture.of(UserApiPojo.class).addTemplate("valid", new Rule(){{
            add("loginName",random("user1","user2"));
            add("firstName",random("User1","User2"));
            add("lastName",random("Last1","Last2"));
            add("email","${loginName}@example.com");
            add("password","12345678");
            add("roleIds",new ArrayList());
        }});
    }
}
