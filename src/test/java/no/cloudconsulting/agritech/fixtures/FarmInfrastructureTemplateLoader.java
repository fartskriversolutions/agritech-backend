package no.cloudconsulting.agritech.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.base.Range;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.Location;
import no.cloudconsulting.agritech.domain.model.api.ClusterApiPojo;
import no.cloudconsulting.agritech.domain.model.api.FarmApiPojo;
import no.cloudconsulting.agritech.domain.model.api.FarmFieldApiPojo;

/**
 * Created by denz0x13 on 1/6/17.
 */
public class FarmInfrastructureTemplateLoader implements TemplateLoader {

    public static Range getGpsRange(){
        return new Range(-180D,180D);
    }

    @Override
    public void load() {
        Fixture.of(Cluster.class).addTemplate("valid", new Rule(){{
            add("name", random("cluster1","cluster2","cluster3"));
            add("typography",random("typography"));
            add("infrastructure",random("infrastructure"));
            add("irrigationSource",random("irrigation_source"));
            add("soilAnalysis",random("soil_analysis"));

        }});

        Fixture.of(Location.class).addTemplate("valid",new Rule(){{
            add("latitude",random(Double.class,getGpsRange()));
            add("longitude",random(Double.class,getGpsRange()));
        }});

        Fixture.of(ClusterApiPojo.class).addTemplate("valid",new Rule(){{
            add("name", random("cluster1","cluster2","cluster3"));
            add("typography",random("typography"));
            add("infrastructure",random("infrastructure"));
            add("irrigationSource",random("irrigation_source"));
            add("soilAnalysis",random("soil_analysis"));
            add("latitude",random(Double.class,getGpsRange()));
            add("longitude",random(Double.class,getGpsRange()));
        }});

        Fixture.of(Farm.class).addTemplate("valid", new Rule(){{
            add("name",random("farm1","farm2","farm3"));

        }});

        Fixture.of(FarmApiPojo.class).addTemplate("valid",new Rule(){{
            add("name", random("farm1","farm2","farm3"));
            add("latitude",random(Double.class,getGpsRange()));
            add("longitude",random(Double.class,getGpsRange()));
        }});

        Fixture.of(FarmFieldApiPojo.class).addTemplate("valid",new Rule(){{
            add("name", random("fl1","f12","fl3"));
            add("latitude",random(Double.class,getGpsRange()));
            add("longitude",random(Double.class,getGpsRange()));
        }});
    }
}
