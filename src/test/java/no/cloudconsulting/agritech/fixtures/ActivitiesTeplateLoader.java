package no.cloudconsulting.agritech.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import no.cloudconsulting.agritech.domain.model.api.ActivityApiPojo;
import no.cloudconsulting.agritech.domain.model.api.ActivitySchedulerApiPojo;
import no.cloudconsulting.agritech.domain.model.api.ActivityTypeApiPojo;

/**
 * Created by denz0x13 on 11.01.17.
 */
public class ActivitiesTeplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(ActivityTypeApiPojo.class).addTemplate("valid", new Rule(){{
            add("name",random("type1","type2","type3","type4"));
        }});

        Fixture.of(ActivityApiPojo.class).addTemplate("valid", new Rule(){{
            add("name",random("activity1","activity2","activity3","activity4"));
            add("description","some description");

        }});

        Fixture.of(ActivitySchedulerApiPojo.class).addTemplate("valid",new Rule(){{
            add("name",random("sch1","sch2","sch3","sch4"));

        }});
    }
}
