package no.cloudconsulting.agritech.api.v1.controllers;

import br.com.six2six.fixturefactory.Fixture;
import no.cloudconsulting.agritech.TestBase;
import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.ClusterApiPojo;
import no.cloudconsulting.agritech.domain.model.api.FarmApiPojo;
import no.cloudconsulting.agritech.domain.model.api.FarmFieldApiPojo;
import no.cloudconsulting.agritech.domain.repositories.ClusterRepository;
import no.cloudconsulting.agritech.domain.repositories.FarmFieldRepository;
import no.cloudconsulting.agritech.domain.repositories.FarmRepository;
import no.cloudconsulting.agritech.domain.services.ClusterService;
import no.cloudconsulting.agritech.domain.services.FarmFieldService;
import no.cloudconsulting.agritech.domain.services.FarmService;
import no.cloudconsulting.agritech.domain.services.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertNotNull;

/**
 * Created by denz0x13 on 10.01.17.
 */

public class FarmBaseControllersTest extends TestBase {
    protected final String _def_user = "admin_super";
    protected final String _def_passwd = "12345678";
    protected String token;

    @Autowired
    ClusterRepository clusterRepository;

    @Autowired
    ClusterService clusterService;

    @Autowired
    FarmRepository farmRepository;
    @Autowired
    FarmService farmService;
    @Autowired
    FarmFieldService farmFieldService;
    @Autowired
    FarmFieldRepository farmFieldRepository;

    @BeforeMethod
    public void setUp() throws Exception {
        super.setUp();
        setUpServer4Tenant();
        if(token == null) {
            token = loginApiUser(_def_user).getToken();
        }
        assertNotNull(token);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        super.tearDown();
    }

    Cluster createCluster() throws ServiceException {
        User user = loginUser(_def_user);
        ClusterApiPojo clusterApiPojo = Fixture.from(ClusterApiPojo.class).gimme("valid");
        clusterApiPojo.userId = user.getId();
        Cluster cluster = createEntity(clusterService,clusterApiPojo);
        assertNotNull(cluster);
        logoutUser();
        return cluster;
    }

    Farm createFarm() throws ServiceException {
        Cluster cluster = createCluster();
        FarmApiPojo farmApiPojo = Fixture.from(FarmApiPojo.class).gimme("valid");
        farmApiPojo.clusterId = cluster.getId();
        farmApiPojo.userId = cluster.getUserId();
        Farm farm = createEntity(farmService,farmApiPojo);
        assertNotNull(farm);
        return farm;
    }

    Farm createFarm(Long clusterId,Long userId) throws ServiceException {
        FarmApiPojo farmApiPojo = Fixture.from(FarmApiPojo.class).gimme("valid");
        farmApiPojo.clusterId = clusterId;
        farmApiPojo.userId = userId;
        Farm farm = createEntity(farmService,farmApiPojo);
        assertNotNull(farm);
        return farm;
    }

    FarmField createFarmField() throws ServiceException {
        Farm farm = createFarm();
        FarmFieldApiPojo fieldApiPojo = Fixture.from(FarmFieldApiPojo.class).gimme("valid");
        fieldApiPojo.userId = farm.getUserId();
        fieldApiPojo.farmId = farm.getId();
        FarmField farmField = createEntity(farmFieldService,fieldApiPojo);
        assertNotNull(farmField);
        return farmField;
    }

    FarmField createFarmField(Long farmId,Long userId) throws ServiceException {
        FarmFieldApiPojo fieldApiPojo = Fixture.from(FarmFieldApiPojo.class).gimme("valid");
        fieldApiPojo.userId = userId;
        fieldApiPojo.farmId = farmId;
        FarmField farmField = createEntity(farmFieldService,fieldApiPojo);
        assertNotNull(farmField);
        return farmField;
    }





}
