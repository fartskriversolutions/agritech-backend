package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.domain.model.Activity;
import no.cloudconsulting.agritech.domain.model.ActivityType;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.ActivityApiPojo;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletResponse;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.testng.Assert.*;
/**
 * Created by denz0x13 on 11.01.17.
 */
public class ActivityControllerTest extends ActivityBaseControllersTest{

    private final String _base_api_path = "/api/v1/activities";

    @Test
    public void testGetAll() throws Exception {
        ActivityType at1 = createActivityType("type1");
        ActivityType at2 = createActivityType("type2");
        FarmField farmField = createFarmField();
        Activity activity = createActivity(farmField.getFarmId(),at1.getId());

        givenAjax(token)
                .when()
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data[0].id",equalTo(activity.getId().intValue()))
                .body("data[0].name",equalTo(activity.getName()))
                .body("data[0].activityTypeId",equalTo(at1.getId().intValue()))
                .body("data[0].farmFieldId",equalTo(farmField.getId().intValue()));

        givenAjax(token)
                .when()
                .param("activity_type_id",at1.getId())
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.size()",is(1));

        givenAjax(token)
                .when()
                .param("activity_type_id",at2.getId())
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.size()",is(0));


    }

    @Test
    public void testGet() throws Exception {
        ActivityType at1 = createActivityType("type1");
        FarmField farmField = createFarmField();
        Activity activity = createActivity(farmField.getFarmId(),at1.getId());

        givenAjax(token)
                .when()
                .get(_base_api_path+"/"+activity.getId())
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.id",equalTo(activity.getId().intValue()))
                .body("data.name",equalTo(activity.getName()))
                .body("data.activityTypeId",equalTo(at1.getId().intValue()))
                .body("data.farmFieldId",equalTo(farmField.getId().intValue()));
    }

    @Test
    public void testCreate() throws Exception {
        ActivityType at1 = createActivityType("type1");
        FarmField farmField = createFarmField();
        ActivityApiPojo pojo = createActivityApiPojo();
        pojo.activityTypeId = at1.getId();
        pojo.farmFieldId = farmField.getFarmId();

        Long aid = givenAjax(token)
                .body(pojo)
                .when()
                .post(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");

        TenantHelper.setTenant(_default_tenant);
        Activity activity = activityRepository.findOne(aid);
        assertNotNull(activity);
        assertEquals(activity.getName(),pojo.name);
        assertEquals(activity.getActivityTypeId(),at1.getId());
        assertEquals(activity.getFarmFieldId(),pojo.farmFieldId);
        assertEquals(activity.getDescription(),pojo.description);



    }

    @Test
    public void testUpdate() throws Exception {
        ActivityType at1 = createActivityType("type1");
        FarmField farmField1 = createFarmField();
        Activity activity = createActivity(farmField1.getId(),at1.getId());

        assertEquals(activity.getFarmFieldId(),farmField1.getId());
        assertEquals(activity.getActivityTypeId(), at1.getId());

        ActivityType at2 = createActivityType("type2");
        FarmField farmField2 = createFarmField(farmField1.getFarmId(),farmField1.getUserId());

        ActivityApiPojo pojo = createActivityApiPojo();
        pojo.activityTypeId = at2.getId();
        pojo.farmFieldId = farmField2.getId();
        pojo.name = "new name";
        pojo.description = "new desc";

        Long aid = givenAjax(token)
                .body(pojo)
                .when()
                .put(_base_api_path+"/"+activity.getId())
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED)
                .extract().jsonPath().getLong("data");
        assertNotNull(aid);
        assertEquals(aid,activity.getId());
        TenantHelper.setTenant(_default_tenant);
        activity = activityRepository.findOne(aid);
        assertNotNull(activity);
        assertEquals(activity.getName(),pojo.name);
        assertEquals(activity.getDescription(),pojo.description);
        assertEquals(activity.getFarmFieldId(),farmField2.getId());
        assertEquals(activity.getActivityTypeId(), at2.getId());


    }

    @Test
    public void testDelete() throws Exception {
        ActivityType at1 = createActivityType("type1");
        FarmField farmField1 = createFarmField();
        Activity activity = createActivity(farmField1.getId(),at1.getId());
        TenantHelper.setTenant(_default_tenant);
        assertEquals(activityRepository.count(),1);
        TenantHelper.setDefaultTenant();

        givenAjax(token)
                .body(Arrays.asList(activity.getId()))
                .when()
                .delete(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);

        TenantHelper.setTenant(_default_tenant);
        assertEquals(activityRepository.count(),0);
        TenantHelper.setDefaultTenant();

    }

}