package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.domain.model.Activity;
import no.cloudconsulting.agritech.domain.model.ActivityScheduler;
import no.cloudconsulting.agritech.domain.model.ActivityType;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.ActivitySchedulerApiPojo;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletResponse;

import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.testng.Assert.*;

/**
 * Created by denz0x13 on 11.01.17.
 */

public class ActivitySchedulersControllerTest extends ActivityBaseControllersTest{

    private final String _base_api_path = "/api/v1/activities/schedulers";

    @Test
    public void testGetAll() throws Exception {
        ActivityType at1 = createActivityType("type1");
        ActivityType at2 = createActivityType("type2");
        FarmField farmField = createFarmField();
        Activity activity1 = createActivity(farmField.getFarmId(),at1.getId());
        Activity activity2 = createActivity(farmField.getFarmId(),at2.getId());
        ActivityScheduler ac1 = createActivityScheduler(activity1.getId());

        givenAjax(token)
                .when()
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data[0].id",equalTo(ac1.getId().intValue()))
                .body("data[0].name",equalTo(ac1.getName()))
                .body("data[0].activityId",equalTo(ac1.getActivityId().intValue()));

        ActivityScheduler ac2 = createActivityScheduler(activity2.getId());

        givenAjax(token)
                .when()
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.size()",is(2));

        givenAjax(token)
                .when()
                .param("activity_id",activity1.getId())
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.size()",is(1));


    }

    @Test
    public void testGet() throws Exception {
        ActivityType at1 = createActivityType("type1");
        FarmField farmField = createFarmField();
        Activity activity1 = createActivity(farmField.getFarmId(),at1.getId());
        ActivityScheduler ac1 = createActivityScheduler(activity1.getId());

        givenAjax(token)
                .when()
                .get(_base_api_path+"/"+ ac1.getId())
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.id",equalTo(ac1.getId().intValue()))
                .body("data.name",equalTo(ac1.getName()))
                .body("data.activityId",equalTo(ac1.getActivityId().intValue()));

    }

    @Test
    public void testCreate() throws Exception {
        ActivityType at1 = createActivityType("type1");
        FarmField farmField = createFarmField();
        Activity activity1 = createActivity(farmField.getFarmId(),at1.getId());
        ActivitySchedulerApiPojo pojo = createActivitySchedulerPojo();
        pojo.activityId = activity1.getId();

        Long acid = givenAjax(token)
                .body(pojo)
                .when()
                .post(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");

        assertNotNull(acid);
        TenantHelper.setTenant(_default_tenant);
        ActivityScheduler ac= activitySchedulerRepository.findOne(acid);
        assertEquals(ac.getName(),pojo.name);
        assertEquals(ac.getActivityId(),activity1.getId());
        assertEquals(ac.getStartTime().getTime(),pojo.startTime.longValue());
        assertEquals(ac.getEndTime().getTime(),pojo.endTime.longValue());
    }

    @Test
    public void testUpdate() throws Exception {
        ActivityType at1 = createActivityType("type1");
        FarmField farmField = createFarmField();
        Activity activity1 = createActivity(farmField.getFarmId(),at1.getId());
        Activity activity2 = createActivity(farmField.getFarmId(),at1.getId());

        ActivityScheduler ac1 = createActivityScheduler(activity1.getId());
        ActivitySchedulerApiPojo pojo = createActivitySchedulerPojo();
        pojo.activityId = activity2.getId();
        pojo.name = "new name";
        pojo.endTime = pojo.endTime - ChronoUnit.HOURS.getDuration().toMillis();

        Long acid = givenAjax(token)
                .body(pojo)
                .when()
                .put(_base_api_path+"/"+ac1.getId())
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED)
                .extract().jsonPath().getLong("data");
        assertNotNull(acid);
        TenantHelper.setTenant(_default_tenant);
        ac1 = activitySchedulerRepository.findOne(acid);
        assertNotNull(ac1);
        assertEquals(ac1.getName(),pojo.name);
        assertEquals(ac1.getActivityId(),pojo.activityId);
        assertEquals(ac1.getEndTime().getTime(),pojo.endTime.longValue());


    }

    @Test
    public void testDelete() throws Exception {
        ActivityType at1 = createActivityType("type1");
        FarmField farmField = createFarmField();
        Activity activity1 = createActivity(farmField.getFarmId(),at1.getId());
        ActivityScheduler ac1 = createActivityScheduler(activity1.getId());

        TenantHelper.setTenant(_default_tenant);
        assertEquals(activitySchedulerRepository.count(),1);
        TenantHelper.setDefaultTenant();

        givenAjax(token)
                .body(Arrays.asList(ac1.getId()))
                .when()
                .delete(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);

        TenantHelper.setTenant(_default_tenant);
        assertEquals(activitySchedulerRepository.count(),0);
        TenantHelper.setDefaultTenant();
    }

}