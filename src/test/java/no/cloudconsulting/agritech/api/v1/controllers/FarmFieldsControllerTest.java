package no.cloudconsulting.agritech.api.v1.controllers;

import br.com.six2six.fixturefactory.Fixture;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.FarmFieldApiPojo;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

import static org.testng.Assert.*;
import static org.hamcrest.Matchers.*;
/**
 * Created by denz0x13 on 10.01.17.
 */
public class FarmFieldsControllerTest extends FarmBaseControllersTest {

    private String _base_api_path = "/api/v1/farm-fields";

    @Test
    public void testGetAll() throws Exception {
        FarmField farmField = createFarmField();
        givenAjax(token)
                .when()
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data[0].id",equalTo(farmField.getId().intValue()))
                .body("data[0].name",equalTo(farmField.getName()))
                .body("data[0].farmId",equalTo(farmField.getParentId().intValue()))
                .body("data[0].userId",equalTo(farmField.getUserId().intValue()))
                .body("data[0].latitude",equalTo(farmField.latitude().floatValue()))
                .body("data[0].longitude",equalTo(farmField.longitude().floatValue()));
    }

    @Test
    public void testGet() throws Exception {
        FarmField farmField = createFarmField();
        givenAjax(token)
                .when()
                .get(_base_api_path+"/"+farmField.getId())
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.id",equalTo(farmField.getId().intValue()))
                .body("data.name",equalTo(farmField.getName()))
                .body("data.farmId",equalTo(farmField.getParentId().intValue()))
                .body("data.userId",equalTo(farmField.getUserId().intValue()))
                .body("data.latitude",equalTo(farmField.latitude().floatValue()))
                .body("data.longitude",equalTo(farmField.longitude().floatValue()));

    }

    @Test
    public void testCreate() throws Exception {
        Farm farm = createFarm();
        FarmFieldApiPojo fieldApiPojo = Fixture.from(FarmFieldApiPojo.class).gimme("valid");
        fieldApiPojo.farmId = farm.getId();
        fieldApiPojo.userId = farm.getUserId();
        Long ffid = givenAjax(token)
                .when()
                .body(fieldApiPojo)
                .post(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");
        assertNotNull(ffid);
        TenantHelper.setTenant(_default_tenant);
        FarmField farmField = farmFieldRepository.findOne(ffid);
        assertEquals(farmField.getName(),fieldApiPojo.name);
        assertEquals(farmField.latitude(),fieldApiPojo.latitude);
        assertEquals(farmField.longitude(),fieldApiPojo.longitude);
        assertEquals(farmField.getParentId(),farm.getId());
        assertEquals(farmField.getUserId(),farm.getUserId());
    }

    @Test
    public void testUpdate() throws Exception {
        FarmField farmField = createFarmField();
        FarmFieldApiPojo fieldApiPojo = Fixture.from(FarmFieldApiPojo.class).gimme("valid");
        fieldApiPojo.name = "new name";

        Long ffid = givenAjax(token)
                .body(fieldApiPojo)
                .when()
                .put(_base_api_path+"/"+farmField.getId())
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED)
                .extract().jsonPath().getLong("data");
        assertNotNull(ffid);
        TenantHelper.setTenant(_default_tenant);
        farmField = farmFieldRepository.findOne(ffid);
        assertNotNull(ffid);
        assertEquals(farmFieldRepository.count(),1);
        assertEquals(farmField.getName(),fieldApiPojo.name);
        assertEquals(farmField.latitude(),fieldApiPojo.latitude);
        assertEquals(farmField.longitude(),fieldApiPojo.longitude);

    }

    @Test
    public void testDelete() throws Exception {
        FarmField farmField = createFarmField();
        TenantHelper.setTenant(_default_tenant);
        assertEquals(farmFieldRepository.count(),1);
        TenantHelper.setDefaultTenant();
        givenAjax(token)
                .body(Arrays.asList(farmField.getId()))
                .when()
                .delete(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);
        TenantHelper.setTenant(_default_tenant);
        assertEquals(farmFieldRepository.count(),0);
        TenantHelper.setDefaultTenant();

    }

}