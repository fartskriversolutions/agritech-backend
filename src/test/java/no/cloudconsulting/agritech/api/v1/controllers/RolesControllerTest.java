package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.TestBase;
import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Role;
import no.cloudconsulting.agritech.domain.model.RolePermission;
import no.cloudconsulting.agritech.domain.model.api.RoleApiPojo;
import no.cloudconsulting.agritech.domain.repositories.RolePermissionRepository;
import no.cloudconsulting.agritech.domain.repositories.RoleRepository;
import no.cloudconsulting.agritech.domain.services.RoleService;
import no.cloudconsulting.agritech.domain.services.RolifyEntityService;
import no.cloudconsulting.agritech.helpers.SpringHelpers;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.jayway.restassured.path.json.JsonPath.from;
import static junit.framework.TestCase.assertTrue;
import static org.testng.Assert.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

/**
 * Created by denz0x13 on 26.12.16.
 */
public class RolesControllerTest extends TestBase{

    private final String _def_user = "admin_super";
    private final String _def_passwd = "12345678";
    private String token;

    @Autowired
    RolifyEntityService rolifyEntityService;

    @Autowired
    RoleService roleService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    RolePermissionRepository rolePermissionRepository;


    private List<Role> _getRolesFromApi(){
        String resp = givenAjax(token).when().
                get("/api/v1/roles").then().statusCode(HttpServletResponse.SC_OK).extract().asString();
        List<Role> roles = from(resp).getList("data");
        assertNotNull(roles);
        return roles;
    }

    @BeforeMethod
    public void setUp() throws Exception {
        super.setUp();
        setUpServer4Tenant();
        if(token==null){
            token = loginApiUser(_def_user).getToken();
        }
        assertNotNull(token);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testGetPermissions() throws Exception {
        String resp = givenAjax(token).when().
                get("/api/v1/roles/permissions").then().statusCode(HttpServletResponse.SC_OK).extract().asString();
        List<String> permissions = from(resp).getList("data");
        assertNotNull(permissions);
        assertTrue(rolifyEntityService.getAvailPermissions().containsAll(permissions));
    }

    @Test
    public void testGetRolifyEntities() throws Exception {
        String resp = givenAjax(token).when().
                get("/api/v1/roles/entities").then().statusCode(HttpServletResponse.SC_OK).extract().asString();
        List<RolifyEntityService.RolifyEntry> entities = from(resp).getList("data");
        assertNotNull(entities);
        assertTrue(entities.size()>0);
        assertEquals(entities.size(),rolifyEntityService.getEntityToDictionary().size());
    }

    @Test
    public void testGetAll() throws Exception {
        TenantHelper.setTenant(_default_tenant);
        assertTrue(_getRolesFromApi().containsAll(roleService.findAll()));
    }

    @Test
    public void testGet() throws Exception {

    }

    @Test
    public void testCreate() throws Exception {
        RolifyEntityService.RolifyEntry re = rolifyEntityService.getEntityToDictionary().get(0);
        RoleApiPojo roleApiPojo = new RoleApiPojo();
        roleApiPojo.name = "SOME_ROLE";
        roleApiPojo.addEntityPermission(re.id,"ADMIN");
        Long rid = givenAjax(token).body(roleApiPojo).when().post("/api/v1/roles").then().statusCode(HttpServletResponse.SC_CREATED).extract().jsonPath().getLong("data");
        assertNotNull(rid);
        TenantHelper.setTenant(_default_tenant);
        Role role = roleRepository.findOne(rid);
        assertEquals(roleApiPojo.name,role.getName());
    }

    @Test
    public void testUpdate() throws Exception {
        RolifyEntityService.RolifyEntry re = rolifyEntityService.getEntityToDictionary().get(0);
        RoleApiPojo roleApiPojo = new RoleApiPojo();
        roleApiPojo.name = "SOME_ROLE";
        roleApiPojo.addEntityPermission(re.id,"ADMIN");
        TenantHelper.setTenant(_default_tenant);
        Role role = roleService.create(roleApiPojo);
        roleApiPojo.entityPermissions = null;
        roleApiPojo.addEntityPermission(re.id,"READ");
        TenantHelper.setDefaultTenant();

        Long rid = givenAjax(token).body(roleApiPojo).when().
                put("/api/v1/roles/"+role.getId().toString()).then().statusCode(HttpServletResponse.SC_ACCEPTED).extract().jsonPath().getLong("data");
        assertNotNull(rid);
        TenantHelper.setTenant(_default_tenant);
        assertTrue(rolePermissionRepository.findByRoleId(rid).size()>0);
        TenantHelper.setDefaultTenant();

        roleApiPojo.entityPermissions = null;
        RolifyEntityService.RolifyEntry re2 = rolifyEntityService.getEntityToDictionary().get(1);
        roleApiPojo.addEntityPermission(re.id,"ADMIN");
        roleApiPojo.addEntityPermission(re2.id,"ADMIN");

        rid = givenAjax(token).body(roleApiPojo).when().
                put("/api/v1/roles/"+role.getId().toString()).then().statusCode(HttpServletResponse.SC_ACCEPTED).extract().jsonPath().getLong("data");
        assertNotNull(rid);
        TenantHelper.setTenant(_default_tenant);
        assertTrue(rolePermissionRepository.findByRoleIdAndClassName(rid,re.klazz.getName()).size()>0);
        assertTrue(rolePermissionRepository.findByRoleIdAndClassName(rid,re2.klazz.getName()).size()>0);
        TenantHelper.setDefaultTenant();
    }

    @Test
    public void testDelete() throws Exception {

    }

    @Test
    public void testGrantPermissionService() throws Exception {
        RoleApiPojo roleApiPojo = new RoleApiPojo();
        roleApiPojo.name = "SOME_ROLE";
        roleApiPojo.addEntityPermission("Cluster","ADMIN");
        Role role = createEntity(roleService,roleApiPojo);
        assertNotNull(role);
        TenantHelper.setTenant(_default_tenant);
        List<RolePermission> rpCluster = rolePermissionRepository.findByRoleIdAndClassName(role.getId(), Cluster.class.getName());
        assertTrue(rpCluster.size()>0);
        Class clChild = SpringHelpers.getChildClass(Cluster.class);
        assertNotNull(clChild);
        List<RolePermission> rpChild = rolePermissionRepository.findByRoleIdAndClassName(role.getId(),clChild.getName());
        assertEquals(rpCluster.size(),rpChild.size());
        Class clChildChild = SpringHelpers.getChildClass(clChild);
        assertNotNull(clChildChild);
        List<RolePermission> rpChildChild = rolePermissionRepository.findByRoleIdAndClassName(role.getId(),clChildChild.getName());
        assertEquals(rpChildChild.size(),rpChild.size());

    }

    @Test
    public void testGrantPermissionRest() throws Exception {
        RolifyEntityService.RolifyEntry re = rolifyEntityService.fromString("Cluster");
        assertNotNull(re);
        RoleApiPojo roleApiPojo = new RoleApiPojo();
        roleApiPojo.name = "SOME_ROLE";
        Role role = createEntity(roleService,roleApiPojo);
        assertNotNull(role);

        TenantHelper.setTenant(_default_tenant);
        assertTrue(rolePermissionRepository.findByRoleIdAndClassName(role.getId(),re.klazz.getName()).size() == 0);
        TenantHelper.setDefaultTenant();

        givenAjax(token)
                .when()
                .put("/api/v1/roles/"+role.getId()+"/grant/"+"ADMIN"+"/to/"+re.id)
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);

        TenantHelper.setTenant(_default_tenant);
        assertTrue(rolePermissionRepository.findByRoleIdAndClassName(role.getId(),re.klazz.getName()).size() > 0);
        TenantHelper.setDefaultTenant();

        givenAjax(token)
                .when()
                .delete("/api/v1/roles/"+role.getId()+"/grant/"+"ADMIN"+"/to/"+re.id)
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);

        TenantHelper.setTenant(_default_tenant);
        assertTrue(rolePermissionRepository.findByRoleIdAndClassName(role.getId(),re.klazz.getName()).size() == 0);
        TenantHelper.setDefaultTenant();

    }

}