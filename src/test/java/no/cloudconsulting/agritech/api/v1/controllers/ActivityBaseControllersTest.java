package no.cloudconsulting.agritech.api.v1.controllers;

import br.com.six2six.fixturefactory.Fixture;
import no.cloudconsulting.agritech.domain.model.Activity;
import no.cloudconsulting.agritech.domain.model.ActivityScheduler;
import no.cloudconsulting.agritech.domain.model.ActivityType;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.ActivityApiPojo;
import no.cloudconsulting.agritech.domain.model.api.ActivitySchedulerApiPojo;
import no.cloudconsulting.agritech.domain.model.api.ActivityTypeApiPojo;
import no.cloudconsulting.agritech.domain.repositories.ActivityRepository;
import no.cloudconsulting.agritech.domain.repositories.ActivitySchedulerRepository;
import no.cloudconsulting.agritech.domain.repositories.ActivityTypeRepository;
import no.cloudconsulting.agritech.domain.services.ActivitySchedulerService;
import no.cloudconsulting.agritech.domain.services.ActivityService;
import no.cloudconsulting.agritech.domain.services.ActivityTypeService;
import no.cloudconsulting.agritech.domain.services.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.testng.Assert.assertNotNull;
/**
 * Created by denz0x13 on 11.01.17.
 */
public class ActivityBaseControllersTest extends FarmBaseControllersTest {

    @Autowired
    ActivityTypeService activityTypeService;

    @Autowired
    ActivityTypeRepository activityTypeRepository;

    @Autowired
    ActivityService activityService;

    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    ActivitySchedulerService activitySchedulerService;

    @Autowired
    ActivitySchedulerRepository activitySchedulerRepository;

    public ActivityTypeApiPojo createActivityTypePojo(){
        return Fixture.from(ActivityTypeApiPojo.class).gimme("valid");
    }

    public ActivityApiPojo createActivityApiPojo(){
        return Fixture.from(ActivityApiPojo.class).gimme("valid");
    }

    public ActivityType createActivityType() throws ServiceException {
        return createActivityType(null);
    }

    public ActivityType createActivityType(String name) throws ServiceException {
        User user = loginUser(_def_user);
        ActivityTypeApiPojo pojo = Fixture.from(ActivityTypeApiPojo.class).gimme("valid");
        if(name !=null){
            pojo.name=name;
        }
        ActivityType activityType = createEntity(activityTypeService,pojo);
        logoutUser();
        return activityType;
    }

    public Activity createActivity(Long fieldId,Long typeId) throws ServiceException {
        User user = loginUser(_def_user);
        ActivityApiPojo pojo = createActivityApiPojo();
        pojo.farmFieldId = fieldId;
        pojo.activityTypeId = typeId;
        Activity activity = createEntity(activityService,pojo);
        assertNotNull(activity);
        return activity;
    }

    public ActivitySchedulerApiPojo createActivitySchedulerPojo(){
        ActivitySchedulerApiPojo pojo = Fixture.from(ActivitySchedulerApiPojo.class).gimme("valid");
        Instant _now = Instant.now();
        pojo.startTime = _now.toEpochMilli();
        pojo.endTime = pojo.startTime + ChronoUnit.WEEKS.getDuration().toMillis();
        return pojo;
    }

    public ActivityScheduler createActivityScheduler(Long activityId) throws ServiceException {
        ActivitySchedulerApiPojo pojo = createActivitySchedulerPojo();
        pojo.activityId = activityId;
        ActivityScheduler ac = createEntity(activitySchedulerService,pojo);
        return ac;

    }
}
