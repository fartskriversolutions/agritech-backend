package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.domain.model.Activity;
import no.cloudconsulting.agritech.domain.model.ActivityType;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.ActivityTypeApiPojo;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletResponse;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Created by denz0x13 on 11.01.17.
 */
public class ActivityTypesControllerTest extends ActivityBaseControllersTest{

    private final String _base_api_path = "/api/v1/activities/types";

    @Test
    public void testGetAll() throws Exception {
        ActivityType at = createActivityType();
        givenAjax(token)
                .when()
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data[0].id",equalTo(at.getId().intValue()))
                .body("data[0].name",equalTo(at.getName()));

    }

    @Test
    public void testGet() throws Exception {
        ActivityType at = createActivityType();
        givenAjax(token)
                .when()
                .get(_base_api_path+"/"+at.getId())
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.id",equalTo(at.getId().intValue()))
                .body("data.name",equalTo(at.getName()));
    }

    @Test
    public void testCreate() throws Exception {
        ActivityTypeApiPojo pojo = createActivityTypePojo();
        Long atid = givenAjax(token)
                .body(pojo)
                .when()
                .post(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");
        assertNotNull(atid);
        TenantHelper.setTenant(_default_tenant);
        assertEquals(activityTypeRepository.count(),1);
        ActivityType at = activityTypeRepository.findOne(atid);
        assertNotNull(at);
        assertEquals(at.getName(),pojo.name);
        TenantHelper.setDefaultTenant();

        atid = givenAjax(token)
                .body(pojo)
                .when()
                .post(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");
        assertNotNull(atid);
        TenantHelper.setTenant(_default_tenant);
        assertEquals(activityTypeRepository.count(),1);


    }

    @Test
    public void testUpdate() throws Exception {
        ActivityType at = createActivityType();
        ActivityTypeApiPojo pojo = createActivityTypePojo();
        pojo.name = "new name";
        Long atid = givenAjax(token)
                .body(pojo)
                .when()
                .put(_base_api_path+"/"+at.getId())
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED)
                .extract().jsonPath().getLong("data");
        assertNotNull(atid);
        TenantHelper.setTenant(_default_tenant);
        at = activityTypeRepository.findFirstByName(pojo.name);
        assertNotNull(at);
        assertEquals(atid,at.getId());
    }

    @Test
    public void testDelete() throws Exception {
        ActivityType at1 = createActivityType("type1");
        ActivityType at2 = createActivityType("type2");
        FarmField farmField = createFarmField();
        Activity activity = createActivity(farmField.getFarmId(),at1.getId());
        TenantHelper.setTenant(_default_tenant);
        assertEquals(activityTypeRepository.count(),2);
        TenantHelper.setDefaultTenant();
        givenAjax(token)
                .body(Arrays.asList(at1.getId(),at2.getId()))
                .when()
                .delete(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);
        TenantHelper.setTenant(_default_tenant);
        assertEquals(activityTypeRepository.count(),1);
        assertNotNull(activityTypeRepository.findOne(at1.getId()));
    }

}