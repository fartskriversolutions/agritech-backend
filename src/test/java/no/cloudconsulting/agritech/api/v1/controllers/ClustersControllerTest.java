package no.cloudconsulting.agritech.api.v1.controllers;

import br.com.six2six.fixturefactory.Fixture;
import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.ClusterApiPojo;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.testng.Assert.*;

/**
 * Created by denz0x13 on 1/6/17.
 */
public class ClustersControllerTest extends FarmBaseControllersTest {




    @Test
    public void testGetAll() throws Exception {
        User user = loginUser(_def_user);
        ClusterApiPojo clusterApiPojo = Fixture.from(ClusterApiPojo.class).gimme("valid");
        clusterApiPojo.userId = user.getId();
        Cluster cluster = createEntity(clusterService,clusterApiPojo);
        assertNotNull(cluster);
        logoutUser();
        givenAjax(token)
                .when()
                .get("/api/v1/clusters")
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data[0].id",equalTo(cluster.getId().intValue()))
                .body("data[0].name",equalTo(cluster.getName()))
                .body("data[0].typography",equalTo(cluster.getTypography()))
                .body("data[0].irrigationSource",equalTo(cluster.getIrrigationSource()))
                .body("data[0].soilAnalysis",equalTo(cluster.getSoilAnalysis()))
                .body("data[0].infrastructure",equalTo(cluster.getInfrastructure()))
                .body("data[0].latitude",equalTo(cluster.latitude().floatValue()))
                .body("data[0].longitude",equalTo(cluster.longitude().floatValue()));

    }

    @Test
    public void testGet() throws Exception {
        User user = loginUser(_def_user);
        ClusterApiPojo clusterApiPojo = Fixture.from(ClusterApiPojo.class).gimme("valid");
        clusterApiPojo.userId = user.getId();
        Cluster cluster = createEntity(clusterService,clusterApiPojo);
        assertNotNull(cluster);
        logoutUser();
        givenAjax(token)
                .when()
                .get("/api/v1/clusters/"+cluster.getId())
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.id",equalTo(cluster.getId().intValue()))
                .body("data.name",equalTo(cluster.getName()))
                .body("data.typography",equalTo(cluster.getTypography()))
                .body("data.irrigationSource",equalTo(cluster.getIrrigationSource()))
                .body("data.soilAnalysis",equalTo(cluster.getSoilAnalysis()))
                .body("data.infrastructure",equalTo(cluster.getInfrastructure()))
                .body("data.latitude",equalTo(cluster.latitude().floatValue()))
                .body("data.longitude",equalTo(cluster.longitude().floatValue()));
    }

    @Test
    public void testCreate() throws Exception {
        ClusterApiPojo clusterApiPojo = Fixture.from(ClusterApiPojo.class).gimme("valid");
        clusterApiPojo.userId = getUserByLogin(_def_user).getId();
        Long cid = givenAjax(token)
                .body(clusterApiPojo)
                .when()
                .post("/api/v1/clusters")
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");
        assertNotNull(cid);
        TenantHelper.setTenant(_default_tenant);
        Cluster cluster = clusterRepository.findOne(cid);
        assertNotNull(cluster);
        assertEquals(cluster.getName(),clusterApiPojo.name);
        assertEquals(cluster.getInfrastructure(),clusterApiPojo.infrastructure);
        assertEquals(cluster.getIrrigationSource(),clusterApiPojo.irrigationSource);
        assertEquals(cluster.getSoilAnalysis(),clusterApiPojo.soilAnalysis);
        assertEquals(cluster.getTypography(),clusterApiPojo.typography);
    }

    @Test
    public void testUpdate() throws Exception {
        User user = loginUser(_def_user);
        ClusterApiPojo clusterApiPojo = Fixture.from(ClusterApiPojo.class).gimme("valid");
        clusterApiPojo.userId = user.getId();
        Cluster cluster = createEntity(clusterService,clusterApiPojo);
        assertNotNull(cluster);
        logoutUser();

        clusterApiPojo.longitude = Math.random()*180;
        clusterApiPojo.latitude = Math.random()*180;
        clusterApiPojo.name = "newname";
        clusterApiPojo.infrastructure = "new val 1";
        clusterApiPojo.soilAnalysis = !clusterApiPojo.soilAnalysis;
        clusterApiPojo.typography = "new val 2";

        Long cid = givenAjax(token)
                .body(clusterApiPojo)
                .when()
                .put("/api/v1/clusters/"+cluster.getId().toString())
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED)
                .extract().jsonPath().getLong("data");
        assertNotNull(cid);
        TenantHelper.setTenant(_default_tenant);
        cluster = clusterRepository.findOne(cid);
        assertNotNull(cluster);
        assertEquals(cid,cluster.getId());
        assertEquals(cluster.getName(),clusterApiPojo.name);
        assertEquals(cluster.getInfrastructure(),clusterApiPojo.infrastructure);
        assertEquals(cluster.getIrrigationSource(),clusterApiPojo.irrigationSource);
        assertEquals(cluster.getSoilAnalysis(),clusterApiPojo.soilAnalysis);
        assertEquals(cluster.getTypography(),clusterApiPojo.typography);
        assertEquals(cluster.latitude(),clusterApiPojo.latitude);
        assertEquals(cluster.longitude(),clusterApiPojo.longitude);

    }

    @Test
    public void testDelete() throws Exception {
        User user = loginUser(_def_user);
        ClusterApiPojo clusterApiPojo = Fixture.from(ClusterApiPojo.class).gimme("valid");
        clusterApiPojo.userId = user.getId();
        Cluster cluster = createEntity(clusterService,clusterApiPojo);
        assertNotNull(cluster);
        logoutUser();

        givenAjax(token)
                .body(Arrays.asList(cluster.getId()))
                .when()
                .delete("/api/v1/clusters")
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);
        TenantHelper.setTenant(_default_tenant);
        assertNull(clusterRepository.findOne(cluster.getId()));
        TenantHelper.setDefaultTenant();

        cluster = createCluster();
        Farm farm = createFarm(cluster.getId(),cluster.getUserId());

        givenAjax(token)
                .body(Arrays.asList(cluster.getId()))
                .when()
                .delete("/api/v1/clusters")
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);
        TenantHelper.setTenant(_default_tenant);
        assertNotNull(clusterRepository.findOne(cluster.getId()));
        assertNotNull(farmRepository.findOne(farm.getId()));

    }

    @Test
    public void testGetAllClustersFarms() throws Exception {
        Farm farm1 = createFarm();
        Farm farm2 = createFarm(farm1.getClusterId(),farm1.getUserId());
        Farm farm3 = createFarm();

        givenAjax(token)
                .when()
                .get("/api/v1/clusters/"+farm1.getClusterId().toString()+"/farms")
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.size()",is(2));
        TenantHelper.setTenant(_default_tenant);
        assertEquals(farmRepository.count(),3);

    }

}