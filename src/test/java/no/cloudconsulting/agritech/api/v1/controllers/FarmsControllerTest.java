package no.cloudconsulting.agritech.api.v1.controllers;

import br.com.six2six.fixturefactory.Fixture;
import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.FarmApiPojo;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.testng.Assert.*;

/**
 * Created by denz0x13 on 10.01.17.
 */
public class FarmsControllerTest extends FarmBaseControllersTest {

    private String _base_api_path = "/api/v1/farms";

    @Test
    public void testCreate() throws Exception {
        Cluster cluster = createCluster();
        FarmApiPojo farmApiPojo = Fixture.from(FarmApiPojo.class).gimme("valid");
        farmApiPojo.clusterId = cluster.getId();
        farmApiPojo.userId = cluster.getUserId();
        Long fid = givenAjax(token)
                .when()
                .body(farmApiPojo)
                .post(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");
        assertNotNull(fid);
        TenantHelper.setTenant(_default_tenant);
        Farm farm = farmRepository.findOne(fid);
        assertNotNull(farm);
        assertEquals(farm.getName(),farmApiPojo.name);
        assertEquals(farm.getClusterId(),farmApiPojo.clusterId);
        assertEquals(farm.getUserId(),farmApiPojo.userId);
        assertEquals(farm.longitude(),farmApiPojo.longitude);
        assertEquals(farm.latitude(),farmApiPojo.latitude);

    }

    @Test
    public void testGet() throws Exception {
        Farm farm = createFarm();
        givenAjax(token)
                .when()
                .get(_base_api_path+"/"+farm.getId().toString())
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.id",equalTo(farm.getId().intValue()))
                .body("data.name",equalTo(farm.getName()))
                .body("data.userId",equalTo(farm.getUserId().intValue()))
                .body("data.clusterId",equalTo(farm.getParentId().intValue()))
                .body("data.latitude",equalTo(farm.latitude().floatValue()))
                .body("data.longitude",equalTo(farm.longitude().floatValue()));
    }

    @Test
    public void testUpdate() throws Exception {
        Farm farm = createFarm();
        Cluster cluster = createCluster();
        FarmApiPojo farmApiPojo = Fixture.from(FarmApiPojo.class).gimme("valid");
        Long fid = givenAjax(token)
                .body(farmApiPojo)
                .when()
                .put(_base_api_path+"/"+farm.getId())
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED)
                .extract().jsonPath().getLong("data");
        assertNotNull(fid);

        TenantHelper.setTenant(_default_tenant);
        farm = farmRepository.findOne(fid);
        assertEquals(fid,farm.getId());
        assertEquals(farm.getName(),farmApiPojo.name);
        assertEquals(farm.longitude(),farmApiPojo.longitude);
        assertEquals(farm.latitude(),farmApiPojo.latitude);
        TenantHelper.setDefaultTenant();

        farmApiPojo.clusterId = cluster.getId();

        fid = givenAjax(token)
                .body(farmApiPojo)
                .when()
                .put(_base_api_path+"/"+farm.getId())
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED)
                .extract().jsonPath().getLong("data");
        assertNotNull(fid);
        TenantHelper.setTenant(_default_tenant);
        farm = farmRepository.findOne(fid);
        assertEquals(fid,farm.getId());
        assertEquals(farm.getClusterId(),cluster.getId());
    }

    @Test
    public void testDelete() throws Exception {
        Farm farm = createFarm();
        givenAjax(token)
                .body(Arrays.asList(farm.getId()))
                .when()
                .delete(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);
        TenantHelper.setTenant(_default_tenant);
        assertNull(farmRepository.findOne(farm.getId()));

        farm = createFarm();
        FarmField farmField = createFarmField(farm.getId(),farm.getUserId());

        givenAjax(token)
                .body(Arrays.asList(farm.getId()))
                .when()
                .delete(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);
        TenantHelper.setTenant(_default_tenant);
        assertNotNull(farmRepository.findOne(farm.getId()));
        assertNotNull(farmFieldRepository.findOne(farmField.getId()));

    }


    @Test
    public void testGetAll() throws Exception {
        Farm farm = createFarm();
        givenAjax(token)
                .when()
                .get(_base_api_path)
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data[0].id",equalTo(farm.getId().intValue()))
                .body("data[0].name",equalTo(farm.getName()))
                .body("data[0].userId",equalTo(farm.getUserId().intValue()))
                .body("data[0].clusterId",equalTo(farm.getParentId().intValue()))
                .body("data[0].latitude",equalTo(farm.latitude().floatValue()))
                .body("data[0].longitude",equalTo(farm.longitude().floatValue()));
    }


    @Test
    public void testGetAllFields() throws Exception {
        Farm farm1 = createFarm();
        FarmField farmField11 = createFarmField(farm1.getId(),farm1.getUserId());
        Farm farm2 = createFarm();
        FarmField farmField21 = createFarmField(farm2.getId(),farm1.getUserId());
        FarmField farmField22 = createFarmField(farm2.getId(),farm1.getUserId());

        givenAjax(token)
                .when()
                .get(_base_api_path+"/"+farm1.getId()+"/farm-fields")
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.size()",is(1));

        givenAjax(token)
                .when()
                .get(_base_api_path+"/"+farm2.getId()+"/farm-fields")
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.size()",is(2));

        TenantHelper.setTenant(_default_tenant);
        assertEquals(farmFieldRepository.count(),3);

    }



}