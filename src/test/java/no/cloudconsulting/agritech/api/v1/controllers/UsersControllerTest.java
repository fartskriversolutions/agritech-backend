package no.cloudconsulting.agritech.api.v1.controllers;

import br.com.six2six.fixturefactory.Fixture;
import no.cloudconsulting.agritech.TestBase;
import no.cloudconsulting.agritech.config.aaa.pojo.LoginResponse;
import no.cloudconsulting.agritech.domain.model.Role;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.UserApiPojo;
import no.cloudconsulting.agritech.domain.repositories.RoleRepository;
import no.cloudconsulting.agritech.domain.repositories.UserRepository;
import no.cloudconsulting.agritech.domain.services.UserManagementService;
import no.cloudconsulting.agritech.domain.services.UserService;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.*;

/**
 * Created by denz0x13 on 12/22/16.
 */
public class UsersControllerTest extends TestBase {

    private final String _def_user = "admin_super";
    private final String _def_passwd = "12345678";
    private String token;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserManagementService userManagementService;

    @BeforeMethod
    public void setUp() throws Exception {
        super.setUp();
        setUpServer4Tenant();
        LoginResponse lr = login(_def_user,_def_passwd);
        token = lr.token;
        assertNotNull(token);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test(priority = 1)
    public void testCreate() throws Exception {
        UserApiPojo userApiPojo = new UserApiPojo();
        userApiPojo.loginName = "login1";
        userApiPojo.lastName = "lastName";
        userApiPojo.firstName = "first";
        userApiPojo.email = "login1@example.com";
        userApiPojo.password = "password";
        userApiPojo.roleIds = new ArrayList<>();
        Role role = Fixture.from(Role.class).gimme("valid");
        TenantHelper.setTenant("admin");
        role = roleRepository.save(role);
        userApiPojo.roleIds.add(role.getId());
        TenantHelper.setDefaultTenant();
        Long uid = givenAjax(token)
                .body(userApiPojo)
                .when()
                .post("/api/v1/users")
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");
        assertNotNull(uid);
        assertNotEquals(uid,0);
        TenantHelper.setTenant("admin");
        User newUser = userManagementService.getByPasswordAndUserName(userApiPojo.loginName,userApiPojo.password);
        assertNotNull(newUser);
        assertEquals(newUser.getId(),uid);
        assertEquals(newUser.getFirstName(),userApiPojo.firstName);
        assertEquals(newUser.getLastName(),userApiPojo.lastName);
        assertEquals(newUser.getUsername(),userApiPojo.loginName);
        assertTrue(newUser.getAuthRoles().contains(role));
        TenantHelper.setDefaultTenant();
    }

    @Test(priority = 0)
    public void testUpdate() throws Exception {
        UserApiPojo userApiPojo = new UserApiPojo();
        userApiPojo.loginName = "login1";
        userApiPojo.lastName = "lastName";
        userApiPojo.firstName = "first";
        userApiPojo.email = "login1@example.com";
        userApiPojo.password = "password";
        userApiPojo.roleIds = new ArrayList<>();
        Role role = Fixture.from(Role.class).gimme("valid");
        TenantHelper.setTenant("admin");
        role = roleRepository.save(role);
        userApiPojo.roleIds.add(role.getId());
        TenantHelper.setDefaultTenant();
        Long uid = givenAjax(token)
                .body(userApiPojo)
                .when()
                .post("/api/v1/users")
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().jsonPath().getLong("data");
        assertNotNull(uid);

        userApiPojo.firstName = "newname";
        userApiPojo.lastName = "newlastname";
        userApiPojo.password="new_password";

        Long nid = givenAjax(token)
                .body(userApiPojo)
                .when()
                .put("/api/v1/users/"+uid.toString())
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED)
                .extract().jsonPath().getLong("data");
        assertEquals(uid,nid);

        TenantHelper.setTenant("admin");
        User newUser = userManagementService.getByPasswordAndUserName(userApiPojo.loginName,userApiPojo.password);
        assertNotNull(newUser);
        assertEquals(newUser.getId(),nid);
        assertEquals(newUser.getFirstName(),userApiPojo.firstName);
        assertEquals(newUser.getLastName(),userApiPojo.lastName);
        assertTrue(newUser.getAuthRoles().contains(role));
        TenantHelper.setDefaultTenant();
    }


    @Test(priority = 2)
    public void testAssignToRole() throws Exception {
        User user = Fixture.from(User.class).gimme("valid");
        TenantHelper.setTenant("admin");
        user = userService.save(user);
        assertNotNull(user.getId());
        Role role = Fixture.from(Role.class).gimme("valid");
        role = roleRepository.save(role);
        assertNotNull(role.getId());
        TenantHelper.setDefaultTenant();
        givenAjax(token).when().put(String.format("/api/v1/users/%d/add-role/%d",user.getId(),role.getId()))
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);
        TenantHelper.setTenant("admin");
        user = userService.findOne(user.getId());
        assertNotNull(user);
        assertTrue(user.getAuthRoles().contains(role));
        TenantHelper.setDefaultTenant();
    }

    @Test(priority = 3)
    public void testGetAll() throws Exception {

    }

    @Test(priority = 2)
    public void testGet() throws Exception {
        User user = Fixture.from(User.class).gimme("valid");
        TenantHelper.setTenant("admin");
        user = userService.save(user);
        assertNotNull(user.getId());
        TenantHelper.setDefaultTenant();
        givenAjax(token).when()
                .get(String.format("/api/v1/users/%d",user.getId()))
                .then()
                .statusCode(HttpServletResponse.SC_OK)
                .body("data.id",equalTo(user.getId().intValue()));

    }


    @Test(priority = 2)
    public void testDelete() throws Exception {
        User user = Fixture.from(User.class).gimme("valid");
        TenantHelper.setTenant("admin");
        user = userService.save(user);
        assertNotNull(user.getId());
        TenantHelper.setDefaultTenant();
        givenAjax(token)
                .body(Arrays.asList(user.getId()))
                .when()
                .delete("/api/v1/users")
                .then()
                .statusCode(HttpServletResponse.SC_ACCEPTED);
        TenantHelper.setTenant("admin");
        assertNull(userRepository.findByLoginName(user.getLoginName()));

    }

}