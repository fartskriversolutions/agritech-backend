package no.cloudconsulting.agritech.config.aaa.auth.ajax;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import no.cloudconsulting.agritech.TestBase;
import no.cloudconsulting.agritech.config.aaa.JwtSettings;
import no.cloudconsulting.agritech.config.aaa.model.RawAccessJwtToken;
import no.cloudconsulting.agritech.config.aaa.pojo.LoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.*;

import javax.servlet.http.HttpServletResponse;

import static org.testng.Assert.*;
/**
 * Created by denz0x13 on 06.12.16.
 */

public class AjaxAuthenticationProviderTest extends TestBase {

    @Autowired
    private JwtSettings settings;


    private final String _def_user = "admin_super";
    private final String _def_passwd = "12345678";

    @BeforeGroups(value = "ajax_auth_test")
    public void beforeGroup() throws Exception {
        super.setUp();
        setUpServer4Tenant();
    }

    @AfterGroups(value = "ajax_auth_test")
    public void afterGroup() throws Exception {
        super.tearDown();
    }

    @BeforeMethod
    public void setUp() throws Exception {
    }

    @AfterMethod
    public void tearDown() throws Exception {
    }

    @Test(groups = {"ajax_auth_test"})
    public void testAuthenticate() throws Exception {
        LoginResponse _resp = login(_def_user,_def_passwd);
        assertNotNull(_resp.token);
        assertNotNull(_resp.refreshToken);
        RawAccessJwtToken rt = new RawAccessJwtToken(_resp.token);
        Jws<Claims> _claims = rt.parseClaims(settings.getTokenSigningKey());
        assertEquals(_claims.getBody().getSubject(),_def_user);
        assertNotNull(_claims.getBody().get("tenant_id"));
        assertEquals((String)_claims.getBody().get("tenant_id"),"admin");
    }

    @Test(groups = {"ajax_auth_test"})
    void testGetAuthResourceSuccess() throws Exception {
        LoginResponse _resp = login(_def_user,_def_passwd);
        givenAjax(_resp.token).when().get("/api/v1/stub").then().statusCode(HttpServletResponse.SC_OK);

    }


    @Test(groups = {"ajax_auth_test"})
    void testGetAuthResourceFail() throws Exception {
        givenAjax().when().get("/api/v1/stub").then().statusCode(HttpServletResponse.SC_UNAUTHORIZED);

    }

    @Test(groups = {"ajax_auth_test"})
    void testGetAuthResourceFailCrossTenant() throws Exception {
        LoginResponse _resp = login(_def_user,_def_passwd);
        givenAjax(_resp.token).when().get("/api/v1/stub").then().statusCode(HttpServletResponse.SC_OK);
        createTenant("tenant2");
        setUpServer4Tenant("tenant2");
        givenAjax(_resp.token).when().get("/api/v1/stub").then().statusCode(HttpServletResponse.SC_UNAUTHORIZED);
    }
}