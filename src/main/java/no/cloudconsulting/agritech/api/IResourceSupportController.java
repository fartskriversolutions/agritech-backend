package no.cloudconsulting.agritech.api;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by denz0x13 on 16.12.15.
 */
public interface IResourceSupportController {
    ResourceSupport getSupportedResources();
}
