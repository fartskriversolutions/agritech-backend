package no.cloudconsulting.agritech.api;

import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.api.exceptions.ApiException;
import no.cloudconsulting.agritech.domain.services.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by denz0x13 on 30.10.15.
 */
@ControllerAdvice
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(AuthenticationException.class)
    public
    @ResponseBody
    ResponseEntity handleAuthException(AuthenticationException e) {
        // LOG.error("Error: " + e.getMessage(), e);
        return new ResponseEntity(e.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    public
    @ResponseBody
    ResponseEntity<ApiResponse> handleException(Exception e) {
        LOG.error("Error: " + e.getMessage(), e);
        List<String> _st = new LinkedList<>();
        for(StackTraceElement el: e.getStackTrace()){
            _st.add(el.toString());
        }
        return new ResponseEntity(
                ApiResponse.builder().errors(5000, e.getMessage()).setData(_st).build(),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(ApiException.class)
    public
    @ResponseBody
    ResponseEntity<ApiResponse> handleApiException(ApiException e) {
        // LOG.error("Error: " + e.getMessage(), e);
        return e.getResponse();
    }

    @ExceptionHandler(ServiceException.class)
    public
    @ResponseBody
    ResponseEntity<ApiResponse> handleApiException(ServiceException e) {
        // LOG.error("Error: " + e.getMessage(), e);
        return e.getResponse();
    }

}