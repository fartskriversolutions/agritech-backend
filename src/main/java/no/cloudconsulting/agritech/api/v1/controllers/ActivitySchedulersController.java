package no.cloudconsulting.agritech.api.v1.controllers;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import no.cloudconsulting.agritech.api.AbstractGenericCrudController;
import no.cloudconsulting.agritech.api.ApiConstants;
import no.cloudconsulting.agritech.api.datatypes.PageApiResponse;
import no.cloudconsulting.agritech.domain.model.ActivityScheduler;
import no.cloudconsulting.agritech.domain.model.api.ActivitySchedulerApiPojo;
import no.cloudconsulting.agritech.domain.services.ActivitySchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.List;

/**
 * Created by denz0x13 on 11.01.17.
 */
@RestController("activities-schedulers-controller")
@RequestMapping(ApiConstants.API_BASE_V1+"/activities/schedulers")
public class ActivitySchedulersController extends AbstractGenericCrudController<ActivityScheduler,ActivitySchedulerService,ActivitySchedulerApiPojo> {
    @Override
    protected List<String> extractAdditionalList() {
        return Arrays.asList("activity_type_id","activity_id");
    }

    @Autowired
    ActivitySchedulerService activitySchedulerService;

    @Override
    protected ActivitySchedulerService getBaseService() {
        return activitySchedulerService;
    }

    @Override
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activity_type_id", value = "Activity type id", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "activity_id", value = "Activity id", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "Max entities", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "offset", value = "start page number", dataType = "long",paramType = "query"),
            @ApiImplicitParam(name = "date_start", value = "timestamp in format yyyy-[m]m-[d]d hh:mm:ss[.f...]", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "date_end", value = "timestamp in format yyyy-[m]m-[d]d hh:mm:ss[.f...]", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "zoneOffset", value = "(+|-) minutes - time zone offset from UTC", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "ids", value = "get only object with given ids", dataType = "string", paramType = "query", example = "ids=3,5,78")
    })
    public PageApiResponse<ActivityScheduler> getAll(@ApiIgnore @RequestParam(required = false) MultiValueMap<String, String> allParams) {
        return super.getAll(allParams);
    }
}
