package no.cloudconsulting.agritech.api.v1;


import no.cloudconsulting.agritech.api.RootAPIController;

/**
 * Created by denz0x13 on 10.09.15.
 */
public abstract class RootV1Controller extends RootAPIController {
    public final static String API_BASE = API_ROOT + "/v1";
    public RootV1Controller(){
        super();
    }
}
