package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.api.ApiConstants;
import no.cloudconsulting.agritech.api.IResourceSupportController;
import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.api.v1.RootV1Controller;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by denz0x13 on 29.10.15.
 */
@RestController("stub")
@RequestMapping(ApiConstants.API_BASE_V1+"/stub")
public class StubController extends RootV1Controller implements IResourceSupportController {
    @RequestMapping(value = "",method = RequestMethod.GET)
    public @ResponseBody
    ApiResponse stub(){
        HashMap<String, Object> _result = new HashMap<String, Object>();
        _result.put("timestamp",new Date());
        _result.put("params", getCommonParams());
        return responseBuilder().setData(_result).build();

    }

    @Override
    public ResourceSupport getSupportedResources() {
        ResourceSupport resourceSupport = new ResourceSupport();
        resourceSupport.add(linkTo(methodOn(StubController.class).stub()).withSelfRel());
        return resourceSupport;
    }
}
