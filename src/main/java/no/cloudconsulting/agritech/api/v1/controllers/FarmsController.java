package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.api.AbstractGenericCrudController;
import no.cloudconsulting.agritech.api.ApiConstants;
import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.FarmApiPojo;
import no.cloudconsulting.agritech.domain.services.FarmFieldService;
import no.cloudconsulting.agritech.domain.services.FarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * Created by denz0x13 on 28.12.16.
 */
@RestController("farms-controller")
@RequestMapping(ApiConstants.API_BASE_V1+"/farms")
public class FarmsController extends AbstractGenericCrudController<Farm,FarmService,FarmApiPojo> {

    @Autowired
    FarmService farmService;

    @Autowired
    FarmFieldService farmFieldService;

    @Override
    protected FarmService getBaseService() {
        return farmService;
    }

    @RequestMapping(value = "/{id}/farm-fields",method = RequestMethod.GET)
    @ResponseBody
    public ApiResponse<List<FarmField>> getAllFields(@PathVariable(name = "id")Long id){
        HashMap _p = getCommonParams().asHash();
        _p.put("farmId",id);
        Page<FarmField> result = farmFieldService.findAll(_p);
        return ApiResponse.buildPageAble(result);
    }
}
