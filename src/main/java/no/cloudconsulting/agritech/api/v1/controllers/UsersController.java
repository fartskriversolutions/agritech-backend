package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.api.AbstractGenericCrudController;
import no.cloudconsulting.agritech.api.ApiConstants;
import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.api.exceptions.ApiException;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.UserApiPojo;
import no.cloudconsulting.agritech.domain.services.ServiceException;
import no.cloudconsulting.agritech.domain.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by denz0x13 on 13.12.16.
 */
@RestController("users-controller")
@RequestMapping(ApiConstants.API_BASE_V1+"/users")
public class UsersController extends AbstractGenericCrudController<User,UserService,UserApiPojo> {

    @Autowired
    UserService userService;

    @Override
    protected UserService getBaseService() {
        return userService;
    }

    @RequestMapping(value = "/{id}/add-role/{role_id}",method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    @ResponseBody
    public ApiResponse assignToRole(@PathVariable("id") Long id, @PathVariable("role_id") Long role_id) throws ApiException, ServiceException {
        if(!getBaseService().canUpdate(id)){
            throw new ApiException(HttpStatus.FORBIDDEN);
        }
        userService.assignToRole(id,role_id);
        return responseBuilder().build();
    }

}
