package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.api.AbstractGenericCrudController;
import no.cloudconsulting.agritech.api.ApiConstants;
import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.api.exceptions.ApiException;
import no.cloudconsulting.agritech.domain.model.Role;
import no.cloudconsulting.agritech.domain.model.api.RoleApiPojo;
import no.cloudconsulting.agritech.domain.services.RoleService;
import no.cloudconsulting.agritech.domain.services.RolifyEntityService;
import no.cloudconsulting.agritech.domain.services.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by denz0x13 on 14.12.16.
 */
@RestController("roles-controller")
@RequestMapping(ApiConstants.API_BASE_V1 + "/roles")
public class RolesController extends AbstractGenericCrudController<Role, RoleService, RoleApiPojo> {

    @Autowired
    RoleService roleService;

    @Autowired
    RolifyEntityService rolifyEntityService;

    @Override
    protected RoleService getBaseService() {
        return roleService;
    }


    @RequestMapping(value = "/permissions", method = RequestMethod.GET)
    @ResponseBody
    ApiResponse<List<String>> getPermissions() {
        return responseBuilder().setData(rolifyEntityService.getAvailPermissions()).build();
    }

    @RequestMapping(value = "/entities", method = RequestMethod.GET)
    @ResponseBody
    ApiResponse<List<RolifyEntityService.RolifyEntry>> getRolifyEntities() {
        return responseBuilder().setData(rolifyEntityService.getEntityToDictionary()).build();
    }

    @RequestMapping(value = "/{id}/grant/{permission}/to/{entity}", method = {RequestMethod.PUT, RequestMethod.DELETE})
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    @ResponseBody
    ApiResponse grantPermission(@PathVariable("id") Long roleId,
                                @PathVariable("permission") String permission,
                                @PathVariable("entity") String entity, HttpServletRequest request) throws ApiException, ServiceException {
        if (!roleService.canGrant(entity)) {
            throw new ApiException(HttpStatus.FORBIDDEN);
        }
        roleService.grantToRoleEntity(entity, roleId, permission, !request.getMethod().equals("DELETE"));
        return responseBuilder().build();
    }


}
