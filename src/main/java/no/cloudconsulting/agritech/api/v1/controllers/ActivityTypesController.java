package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.api.AbstractGenericCrudController;
import no.cloudconsulting.agritech.api.ApiConstants;
import no.cloudconsulting.agritech.domain.model.ActivityType;
import no.cloudconsulting.agritech.domain.model.api.ActivityTypeApiPojo;
import no.cloudconsulting.agritech.domain.services.ActivityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by denz0x13 on 11.01.17.
 */
@RestController("activity-types-controller")
@RequestMapping(ApiConstants.API_BASE_V1+"/activities/types")
public class ActivityTypesController extends AbstractGenericCrudController<ActivityType,ActivityTypeService,ActivityTypeApiPojo>{
    @Autowired
    ActivityTypeService activityTypeService;

    @Override
    protected ActivityTypeService getBaseService() {
        return activityTypeService;
    }
}
