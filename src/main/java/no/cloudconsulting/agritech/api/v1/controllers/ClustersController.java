package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.api.AbstractGenericCrudController;
import no.cloudconsulting.agritech.api.ApiConstants;
import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.api.ClusterApiPojo;
import no.cloudconsulting.agritech.domain.services.ClusterService;
import no.cloudconsulting.agritech.domain.services.FarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * Created by denz0x13 on 28.12.16.
 */
@RestController("clusters-controller")
@RequestMapping(ApiConstants.API_BASE_V1+"/clusters")
public class ClustersController extends AbstractGenericCrudController<Cluster,ClusterService,ClusterApiPojo> {
    @Autowired
    ClusterService clusterService;

    @Autowired
    FarmService farmService;

    @Override
    protected ClusterService getBaseService() {
        return clusterService;
    }

    @RequestMapping(value = "/{id}/farms",method = RequestMethod.GET)
    @ResponseBody
    public ApiResponse<List<Farm>> getAllFarms(@PathVariable(name = "id")Long id){
        HashMap _p = getCommonParams().asHash();
        _p.put("clusterId",id);
        Page<Farm> result = farmService.findAll(_p);
        return ApiResponse.buildPageAble(result);
    }
}
