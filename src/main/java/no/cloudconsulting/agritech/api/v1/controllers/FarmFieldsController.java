package no.cloudconsulting.agritech.api.v1.controllers;

import no.cloudconsulting.agritech.api.AbstractGenericCrudController;
import no.cloudconsulting.agritech.api.ApiConstants;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.FarmFieldApiPojo;
import no.cloudconsulting.agritech.domain.services.FarmFieldService;
import no.cloudconsulting.agritech.domain.services.FarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by denz0x13 on 29.12.16.
 */

@RestController("farm-fields-controller")
@RequestMapping(ApiConstants.API_BASE_V1+"/farm-fields")
public class FarmFieldsController extends AbstractGenericCrudController<FarmField,FarmFieldService,FarmFieldApiPojo>{

    @Autowired
    FarmFieldService farmFieldService;

    @Autowired
    FarmService farmService;

    @Override
    protected FarmFieldService getBaseService() {
        return farmFieldService;
    }


}
