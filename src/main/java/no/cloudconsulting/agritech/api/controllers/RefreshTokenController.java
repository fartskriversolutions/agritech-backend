package no.cloudconsulting.agritech.api.controllers;

import no.cloudconsulting.agritech.config.aaa.JwtSettings;
import no.cloudconsulting.agritech.config.aaa.WebSecurityConfig;
import no.cloudconsulting.agritech.config.aaa.auth.jwt.extractor.TokenExtractor;
import no.cloudconsulting.agritech.config.aaa.auth.jwt.verifier.TokenVerifier;
import no.cloudconsulting.agritech.config.aaa.exceptions.InvalidJwtToken;
import no.cloudconsulting.agritech.config.aaa.model.*;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.services.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by denz0x13 on 05.12.16.
 */
@RestController
public class RefreshTokenController {
    @Autowired
    private JwtTokenFactory tokenFactory;
    @Autowired
    private JwtSettings jwtSettings;
    @Autowired
    private UserManagementService userService;
    @Autowired
    private TokenVerifier tokenVerifier;
    @Autowired
    @Qualifier("jwtHeaderTokenExtractor") private TokenExtractor tokenExtractor;

    @RequestMapping(value=WebSecurityConfig.TOKEN_REFRESH_ENTRY_POINT, method= RequestMethod.GET, produces={ MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody
    JwtToken refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));

        RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
        RefreshToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());

        String jti = refreshToken.getJti();
        if (!tokenVerifier.verify(jti)) {
            throw new InvalidJwtToken();
        }

        String subject = refreshToken.getSubject();
        User user = userService.getByUsername(subject);
        if(user==null){
            throw new UsernameNotFoundException("User not found: " + subject);
        }

        if(!user.tenantId().equals(refreshToken.getTenant())){
            throw new UsernameNotFoundException("User not found: " + subject);
        }


        if (user.getRoles() == null) throw new InsufficientAuthenticationException("User has no roles assigned");

        return tokenFactory.createAccessJwtToken(user);
    }
}
