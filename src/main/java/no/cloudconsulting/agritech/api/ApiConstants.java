package no.cloudconsulting.agritech.api;

/**
 * Created by denz0x13 on 18.12.15.
 */
public class ApiConstants {

    public static final String API_ROOT ="/api";
    public static final String API_BASE_V1 = API_ROOT+"/v1";
    public static final String API_BASE_V2 = API_ROOT+"/v2";
}
