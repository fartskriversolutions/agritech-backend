package no.cloudconsulting.agritech.api.datatypes;

import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * Created by denz0x13 on 05.12.16.
 */
public class ErrorResponse {
    // HTTP Response Status Code
    private final HttpStatus status;

    // General Error message
    private final String message;

    // Error code
    private final SYSTEM_ERROR errorCode;

    private final Date timestamp;

    protected ErrorResponse(final String message, final SYSTEM_ERROR errorCode, HttpStatus status) {
        this.message = message;
        this.errorCode = errorCode;
        this.status = status;
        this.timestamp = new java.util.Date();
    }

    public static ErrorResponse of(final String message, final SYSTEM_ERROR errorCode, HttpStatus status) {
        return new ErrorResponse(message, errorCode, status);
    }

    public Integer getStatus() {
        return status.value();
    }

    public String getMessage() {
        return message;
    }

    public SYSTEM_ERROR getErrorCode() {
        return errorCode;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
