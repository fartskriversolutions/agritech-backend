package no.cloudconsulting.agritech.api.datatypes;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by denz0x13 on 19.10.15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonParams {

    public static final long MONTH_SEC = 30*24*3600*1000;
    @JsonProperty
    public Timestamp dateStart;
    @JsonProperty
    public Timestamp dateEnd;
    @JsonProperty
    public int limit;
    @JsonProperty
    public int offset;
    @JsonProperty
    public long zoneOffset=0;
    @JsonIgnore
    List<Long> ids;

    public CommonParams(){
        limit = 1000;
        offset = 0;
        dateEnd = null;
        dateStart = null;
        zoneOffset = 0;
    }

    public CommonParams(HttpServletRequest request){
        this();

        if(request.getParameter("zoneOffset")!=null){
            try{
                Integer o = Integer.parseInt(request.getParameter("zoneOffset"));
                this.zoneOffset = o.longValue();
            }catch (NumberFormatException ex){}
        }

        try {
            try {
                this.dateStart = new Timestamp(((long) Integer.parseInt(request.getParameter("date_start"))));
            } catch (NumberFormatException e) {
                try {
                    this.dateStart = Timestamp.valueOf(request.getParameter("date_start"));
                    this.dateStart = Timestamp.from(Instant.ofEpochMilli(this.dateStart.getTime()).minus(zoneOffset, ChronoUnit.MINUTES));
                } catch (IllegalArgumentException e1) {

                }
            }

            try {
                this.dateEnd = new Timestamp(((long) Integer.parseInt(request.getParameter("date_end"))));
            } catch (NumberFormatException e) {
                try {
                    this.dateEnd = Timestamp.valueOf(request.getParameter("date_end"));
                    this.dateEnd = Timestamp.from(Instant.ofEpochMilli(this.dateEnd.getTime()).minus(zoneOffset, ChronoUnit.MINUTES));
                } catch (IllegalArgumentException e1) {

                }
            }
        }catch (Exception e){

        }



        if(request.getParameter("limit")!=null) {
            Integer l = Integer.parseInt(request.getParameter("limit"));
            if(l!=null) {
                this.limit=l;
            }
        }
        if(request.getParameter("offset")!=null) {
            Integer o = Integer.parseInt(request.getParameter("offset"));
            if(o!=null) {
                this.offset=o;
            }
        }
        if(request.getParameter("ids")!=null){
            String[] _s_ids = request.getParameter("ids").split(",");
            this.ids = new ArrayList<>();
            for(int i=0 ;i<_s_ids.length;i++){
                try{
                    this.ids.add(Long.parseLong(_s_ids[i]));
                }catch (NumberFormatException ex){}
            }

        }

    }

    @JsonIgnore
    public HashMap<String,Object> asHash(){
        HashMap<String,Object> _result = new HashMap<>();
        _result.put("limit",Integer.valueOf(limit));
        _result.put("offset",Integer.valueOf(offset));
        _result.put("zoneOffset",Long.valueOf(zoneOffset));
        if(dateStart!=null){
            _result.put("date_start",dateStart);
        }
        if(dateEnd!=null){
            _result.put("data_end",dateEnd);
        }
        if(this.ids!=null){
            if(this.ids.size() > 0){
                _result.put("ids",new ArrayList(this.ids));
            }
        }
        return _result;
    }

    @JsonIgnore
    public CommonParams setDateStart(int secs){
        dateStart= new Timestamp(secs*1000L);
        return this;
    }

    @JsonIgnore
    public CommonParams setDateStart(long secs){
        dateStart= new Timestamp(secs);
        return this;
    }

    @JsonIgnore
    public CommonParams setDateEnd(int secs){
        dateEnd = new Timestamp(secs*1000L);
        return this;
    }

    @JsonIgnore
    public CommonParams setDateEnd(long secs){
        dateEnd = new Timestamp(secs);
        return this;
    }

    @JsonIgnore
    public CommonParams setLimit(int l){
        limit=l;
        return this;
    }

    @JsonIgnore
    public CommonParams setOffset(int o){
        offset=o;
        return this;
    }

}
