package no.cloudconsulting.agritech.api.datatypes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;
import no.cloudconsulting.agritech.datatypes.AbstractJsonType;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denz0x13 on 10.09.15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse<T> extends AbstractJsonType {

    public ArrayList<MessageItem> getSuccess() {
        return success;
    }

    @JsonSetter
    public void setSuccess(ArrayList<MessageItem> success) {
        this.success = success;
    }

    public void setSuccess(MessageItem messageItem) {
        if(this.success == null){
            this.success= new ArrayList<>();
        }
        this.success.add(messageItem);
    }


    public ArrayList<MessageItem> getErrors() {
        return errors;
    }

    @JsonSetter
    public void setErrors(ArrayList<MessageItem> errors) {
        this.errors = errors;
    }

    public void setErrors(MessageItem messageItem) {
        if(this.errors == null){
            this.errors = new ArrayList<>();
        }
        this.errors.add(messageItem);
    }

    public ArrayList<MessageItem> getWarnings() {
        return warnings;
    }

    @JsonSetter
    public void setWarnings(ArrayList<MessageItem> warnings) {
        this.warnings = warnings;
    }

    public ApiResponse setWarnings(MessageItem messageItem) {
        if(this.warnings==null){
            this.warnings = new ArrayList<>();
        }
        this.warnings.add(messageItem);
        return this;
    }

    private T data;
    private ArrayList<MessageItem> success;
    private ArrayList<MessageItem> errors;
    private ArrayList<MessageItem> warnings;

    ApiResponse(){

    }

    public ApiResponse(T data) {
        setData(data);
    }



    public T getData() {
        return data;
    }

    @JsonSetter
    public void setData(T data){

        this.data = data;

    }

    public ApiResponse<T> withData(T data){
        setData(data);
        return this;
    }

    @JsonIgnore
    public ApiResponse withResourceSupport(ResourceSupport resourceSupport){
        return withLink(resourceSupport.getLinks());
    }

    @JsonIgnore
    public ApiResponse withLink(List<Link> links){
        if(!getAdditionalProperties().containsKey("links")){
            setAdditionalProperty("links", new ArrayList<Link>());
        }
        ((ArrayList) getAdditionalProperties().get("links")).addAll(links);
        return this;
    }
    @JsonIgnore
    public ApiResponse withLink(Link link){
        ArrayList<Link> _a = new ArrayList<>();
        _a.add(link);
        return withLink(_a);
    }

    public static <M> ApiResponse<List<M>> buildPageAble(Page page){
        ApiResponse _result = new ApiResponse<List<M>>();
        _result.setData(page.getContent());
        _result.setAdditionalProperty("total",page.getTotalElements());
        _result.setAdditionalProperty("limit",page.getSize());
        _result.setAdditionalProperty("offset",page.getNumber()*page.getSize());
        return _result;
    }


    public static<T> Builder builder(){
        return new Builder<T>();
    }

    public static class Builder<T>{
        private ApiResponse<T> apiResponse;
        Builder(){
            apiResponse = new ApiResponse<T>();
        }

        public ApiResponse build(){
            return this.apiResponse;
        }

        public Builder<T> setData(T data){
            apiResponse.setData(data);
            return this;
        }

        public Builder success(String message){
            apiResponse.setSuccess(new MessageItem(message));
            return this;
        }

        public Builder success(Integer code,String message){
            apiResponse.setSuccess(new MessageItem(code, message));
            return this;
        }

        public Builder errors(String message){
            apiResponse.setErrors(new MessageItem(message));
            return this;
        }

        public Builder errors(Integer code,String message){
            apiResponse.setErrors(new MessageItem(code,message));
            return this;
        }

        public Builder warnings(String message){
            apiResponse.setWarnings(new MessageItem(message));
            return this;
        }

        public Builder warnings(Integer code,String message){
            apiResponse.setWarnings(new MessageItem(code,message));
            return this;
        }

        public Builder link(Link link){
            apiResponse.withLink(link);
            return this;
        }

        public Builder link(List<Link> links){
            apiResponse.withLink(links);
            return this;
        }
    }
}
