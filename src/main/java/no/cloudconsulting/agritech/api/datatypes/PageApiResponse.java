package no.cloudconsulting.agritech.api.datatypes;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by denz0x13 on 07.12.15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageApiResponse<M> extends ApiResponse<List<M>> {
    public Long limit;
    public Long offset;
    public Long total;

    public PageApiResponse(Page page) {
        super(page.getContent());
        total = (long) page.getTotalElements();
        limit = (long) page.getSize();
        offset = (long) page.getNumber()*page.getSize();
    }

    @Override
    public void setData(List<M> data) {
        super.setData(data);
    }

    @Override
    public ApiResponse<List<M>> withData(List<M> data) {
        return super.withData(data);
    }

    PageApiResponse() {
        super();
    }
}
