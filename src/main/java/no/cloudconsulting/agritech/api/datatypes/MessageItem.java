package no.cloudconsulting.agritech.api.datatypes;

import org.springframework.http.HttpStatus;

/**
 * Created by denz0x13 on 04.11.15.
 */
public class MessageItem {
    public Integer code;
    public String message;
    public String localizedMessage;
    public HttpStatus status;

    public MessageItem(){

    }

    public MessageItem(Integer code){
        this.code = code;
    }

    public MessageItem(String message){
        this.message = message;
        localizedMessage = message;
    }

    public MessageItem(Integer code, String message){
        this(message);
        this.code = code;
    }

    public MessageItem(Integer code, String message,HttpStatus status){
        this(code,message);
        this.status = status;
    }
}
