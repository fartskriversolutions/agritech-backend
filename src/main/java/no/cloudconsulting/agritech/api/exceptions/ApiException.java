package no.cloudconsulting.agritech.api.exceptions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by denz0x13 on 01.12.15.
 */
public class ApiException extends Exception {

    private static ObjectMapper objectMapper = new ObjectMapper();

    private HttpStatus httpStatus;
    private SYSTEM_ERROR errorCode=null;

    private ApiException() {
        super();
    }

    private ApiException(Throwable cause) {
        super(cause);
    }

    private ApiException(String message) {
        super(message);
    }

    public ApiException(String message, HttpStatus httpStatus) {
        this(message);
        this.httpStatus = httpStatus;
    }

    public ApiException(String message, HttpStatus httpStatus, SYSTEM_ERROR requestError) {
        this(message);
        this.errorCode = requestError;
        this.httpStatus = httpStatus;
    }

    public ApiException(Throwable cause, HttpStatus httpStatus) {
        super(cause);
        this.httpStatus = httpStatus;
    }

    public ApiException(Throwable cause, HttpStatus httpStatus, SYSTEM_ERROR requestError) {
        super(cause);
        this.errorCode = requestError;
        this.httpStatus = httpStatus;
    }

    public ApiException(Object message, HttpStatus httpStatus) throws JsonProcessingException {
        this(objectMapper.writeValueAsString(message));
        this.httpStatus = httpStatus;
    }

    public ApiException(Object message, HttpStatus httpStatus, SYSTEM_ERROR requestError) throws JsonProcessingException {
        this(objectMapper.writeValueAsString(message),httpStatus);
        this.errorCode = requestError;
    }

    public ApiException(HttpStatus httpStatus) {
        this(httpStatus.getReasonPhrase());
        this.httpStatus = httpStatus;
    }

    public ApiException(HttpStatus httpStatus, SYSTEM_ERROR requestError) {
        this(requestError.getMessage());
        this.httpStatus = httpStatus;
        this.errorCode = requestError;
    }

    public ResponseEntity<ApiResponse> getResponse(){
        if(errorCode!=null){
            return new ResponseEntity<ApiResponse>(
                    ApiResponse.builder().errors(errorCode.getCode(),errorCode.getMessage()).build(),
                    httpStatus
            );
        }
        return new ResponseEntity(
                ApiResponse.builder().errors(httpStatus.value(), super.getMessage()).build(),
                httpStatus);
    }

    public int getCode(){
        if(errorCode!=null){
            return errorCode.getCode();
        }
        return httpStatus.value();
    }

    public String getMessage(){
        if(errorCode!=null){
            return errorCode.getMessage();
        }
        return super.getMessage();
    }

}
