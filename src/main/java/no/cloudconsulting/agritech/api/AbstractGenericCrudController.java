package no.cloudconsulting.agritech.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.api.datatypes.PageApiResponse;
import no.cloudconsulting.agritech.api.exceptions.ApiException;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.model.base.BaseEntity;
import no.cloudconsulting.agritech.domain.services.BaseService;
import no.cloudconsulting.agritech.domain.services.ServiceException;
import no.cloudconsulting.agritech.helpers.ExtractorHelper;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by denz0x13 on 04.01.16.
 */
public abstract class AbstractGenericCrudController<M extends BaseEntity, S extends BaseService, P extends BaseApiPojo> extends RootAPIController implements IResourceSupportController {

    public AbstractGenericCrudController() {
        super();
    }

    protected abstract S getBaseService();

    protected List<String> extractAdditionalList() {
        return new ArrayList<String>();
    }

    protected HashMap<String, Object> extractAdditional(final MultiValueMap<String, String> params) {
        return ExtractorHelper.extractFromMap(extractAdditionalList(),params);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "limit", value = "Max entities", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "offset", value = "start page number", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "zoneOffset", value = "(+|-) minutes - time zone offset from UTC", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "date_start", value = "timestamp in format yyyy-[m]m-[d]d hh:mm:ss[.f...]", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "date_end", value = "timestamp in format yyyy-[m]m-[d]d hh:mm:ss[.f...]", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "ids", value = "get only object with given ids", dataType = "string", paramType = "query", example = "ids=3,5,78")
    })
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public PageApiResponse<M> getAll(@ApiIgnore @RequestParam(required = false) MultiValueMap<String, String> allParams) {
        HashMap<String, Object> params = getCommonParams().asHash();
        params.putAll(extractAdditional(allParams));
        Page result = getBaseService().findAll(params);
        return new PageApiResponse<>(result);
        //return responseBuilder().setData(getBaseService().findAll(_p)).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ApiResponse<M> get(@PathVariable("id") Long id) throws ServiceException, ApiException {
        try {
            return responseBuilder().setData(getBaseService().findOne(id)).build();
        } catch (AccessDeniedException ex) {
            throw new ApiException(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    public ApiResponse<Long> create(@RequestBody P entity) throws ServiceException, ApiException {
        if (!getBaseService().canCreate()) {
            throw new ApiException(HttpStatus.FORBIDDEN);
        }
        return responseBuilder().setData(getBaseService().create(entity).getId()).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    @ResponseBody
    public ApiResponse<Long> update(@PathVariable("id") Long id, @RequestBody P entity) throws ApiException, ServiceException {
        if (!getBaseService().canUpdate(id)) {
            throw new ApiException(HttpStatus.FORBIDDEN);
        }
        return responseBuilder().setData(getBaseService().update(id, entity).getId()).build();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    @ResponseBody
    public ApiResponse delete(@RequestBody List<Long> ids) {
        getBaseService().delete(ids);
        return responseBuilder().build();
    }

    @RequestMapping(value = "/permit_to/{role_id}/{permission}", method = RequestMethod.POST)
    @ResponseBody
    public ApiResponse grantPermission(@PathVariable("role_id") Long roleId, @PathVariable("permission") String permission) throws ApiException, ServiceException {
        if (!getBaseService().isAdmin()) {
            throw new ApiException(HttpStatus.FORBIDDEN);
        }
        getBaseService().grantToRole(roleId, permission);
        return responseBuilder().build();
    }

    @Override
    public ResourceSupport getSupportedResources() {
        ResourceSupport resourceSupport = new ResourceSupport();
        // resourceSupport.add(linkTo(methodOn(this.getClass()).getAll()).withSelfRel());
        resourceSupport.add(linkTo(methodOn(this.getClass())).withSelfRel());
//        resourceSupport.add(linkTo(methodOn(this.getClass()).get(1L)).withSelfRel());
//        resourceSupport.add(linkTo(methodOn(this.getClass()).create()).withSelfRel());
//        resourceSupport.add(linkTo(methodOn(this.getClass()).update()).withSelfRel());
//        resourceSupport.add(linkTo(methodOn(this.getClass()).delete()).withSelfRel());
        return resourceSupport;
    }

}
