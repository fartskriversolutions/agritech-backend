package no.cloudconsulting.agritech.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.api.datatypes.CommonParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * Created by denz0x13 on 10.09.15.
 */
@RestController
public abstract class RootAPIController {

    protected static final Logger log = LoggerFactory.getLogger(RootAPIController.class);
    private CommonParams commonParams = new CommonParams();
    protected static ObjectMapper objectMapper = new ObjectMapper();


    public static final String API_ROOT ="/api";
    public RootAPIController(){

    }

    public ApiResponse.Builder responseBuilder(){
        return ApiResponse.builder();
    }

    public CommonParams getCommonParams() {
        commonParams = new CommonParams(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest());
        return commonParams;
    }

}
