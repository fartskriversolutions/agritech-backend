package no.cloudconsulting.agritech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by denz0x13 on 05.12.16.
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"no.cloudconsulting"})
public class AppMain {
    public static void main(String[] args){
        SpringApplication.run(AppMain.class,args);
    }


}
