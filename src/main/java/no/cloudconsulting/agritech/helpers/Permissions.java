package no.cloudconsulting.agritech.helpers;

import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denz0x13 on 12/21/16.
 */
public enum Permissions {
    READ(BasePermission.READ),
    WRITE(BasePermission.WRITE),
    CREATE(BasePermission.CREATE),
    DELETE(BasePermission.DELETE),
    ADMIN(BasePermission.ADMINISTRATION);

    private Permission _permission;

    Permissions(Permission permission) {
        this._permission = permission;
    }

    public int getMask(){
        return _permission.getMask();
    }

    public static Permission fromString(String perm){
        for(Permissions p: Permissions.values()){
            if(p.toString().equalsIgnoreCase(perm)){
                return p._permission;
            }
        }
        return null;
    }

    public static String fromMask(int mask){
        for(Permissions p: Permissions.values()){
            if(p.getMask() == mask){
                return p.toString();
            }
        }
        return null;
    }

    public static List<String> asList(){
        List<String> _r = new ArrayList<>();
        for(Permissions p:Permissions.values()){
            _r.add(p.toString());
        }
        return _r;
    }
}
