package no.cloudconsulting.agritech.helpers;

import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;

/**
 * Created by denz0x13 on 13.01.16.
 */
public class ResourceHelper {
    public static String readResourceFile(String filePath) throws Exception {
        ClassPathResource classPathResource = new ClassPathResource(filePath);
        InputStream inputStream = classPathResource.getInputStream();
        byte[] _buffer = new byte[inputStream.available()];
        inputStream.read(_buffer);
        return new String(_buffer);
    }
}
