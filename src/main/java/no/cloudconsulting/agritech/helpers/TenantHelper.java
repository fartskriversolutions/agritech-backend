package no.cloudconsulting.agritech.helpers;

import com.google.common.net.InternetDomainName;
import org.springframework.core.NamedInheritableThreadLocal;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by denz0x13 on 05.12.16.
 */

public class TenantHelper {

    private static final ThreadLocal<String> threadLocalTenantId = new NamedInheritableThreadLocal<>("CURRENT_TENANT_IDENTIFIER");

    public static String getCurrentTenantName(){
        if (threadLocalTenantId.get() == null) {
            setDefaultTenant();
        }
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

        if(sra!=null) {
            HttpServletRequest request = sra.getRequest();
            if (request != null) {
                setTenant(fromRequestAttributes(request));
            }
        }
        return threadLocalTenantId.get();
    }

    public static String fromRequestAttributes(HttpServletRequest request) {
        String tenant = "";
        if (request.getParameter("tenant_id") != null) {
            tenant = request.getParameter("tenant_id");
            return tenant;
        }
        if (request.getServerName() != null) {
            List<String> parts = InternetDomainName.from(request.getServerName()).parts();
            if (parts.size() > 2) {
                if ("www".equals(parts.get(0))) {
                    tenant = parts.get(1);
                } else {
                    tenant = parts.get(0);
                }
            }
        } else {
            return threadLocalTenantId.get();
        }

        return tenant;
    }

    public static void setTenant(String tenantId) {
            threadLocalTenantId.set(tenantId);
    }

    public static void setDefaultTenant() {
        setTenant("");

    }

    public static Boolean isDefault() {
        return getCurrentTenantName().isEmpty();
    }


    private static final HandlerInterceptor handlerInterceptor = new HandlerInterceptor() {
        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
            String tenant = TenantHelper.fromRequestAttributes(request);
            TenantHelper.setTenant(tenant);
            return true;
        }

        @Override
        public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

        }

        @Override
        public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        }
    };

    public static HandlerInterceptor getHandlerInterceptor(){
        return handlerInterceptor;
    }
}