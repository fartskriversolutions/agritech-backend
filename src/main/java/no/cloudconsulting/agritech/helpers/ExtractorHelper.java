package no.cloudconsulting.agritech.helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import no.cloudconsulting.agritech.datatypes.AbstractJsonType;
import org.springframework.util.MultiValueMap;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by denz0x13 on 13.01.17.
 */
public class ExtractorHelper {

    static ObjectMapper objectMapper = new ObjectMapper();

    public static final Pattern integerPattern = Pattern.compile("\\d+");
    public static final Pattern floatPattern = Pattern.compile("\\d+.\\d+");
    public static final Pattern hashPattern = Pattern.compile("^[_,a-z,A-Z]+\\w+\\[\\w+\\]$");
    public static final Pattern hashArrayPattern = Pattern.compile("^[_,a-z,A-Z]+\\w+\\[\\w+\\]\\[\\]$");
    public static final Pattern arrayPattern = Pattern.compile("^[_,a-z,A-Z]+\\w+\\[\\]$");
    public static final Pattern validKeyPattern = Pattern.compile("^[_,a-z,A-Z]+\\w+$");

    public static Boolean isInteger(String source) {
        return integerPattern.matcher(source).matches();
    }

    public static Boolean isFloat(String source) {
        return floatPattern.matcher(source).matches() && !isInteger(source);
    }

    public static Boolean isHashKey(String source) {
        return hashPattern.matcher(source).matches();
    }

    public static Boolean isHashArrayKey(String source) {
        return hashArrayPattern.matcher(source).matches();
    }

    public static Boolean isArrayKey(String source) {
        return arrayPattern.matcher(source).matches();
    }

    public static Boolean isValidKey(String source) {
        return validKeyPattern.matcher(source).matches();
    }

    public static Object extractObject(String source) {
        if (ExtractorHelper.isInteger(source)) {
            try {
                return Long.parseLong(source);
            } catch (IllegalFormatException e) {
                return source;
            }
        } else if (ExtractorHelper.isFloat(source)) {
            try {
                return Double.parseDouble(source);
            } catch (IllegalFormatException e) {
                return source;
            }
        }
        return source;

    }

    public static HashMap<String, Object> extractFromMap(MultiValueMap<String, String> params) {
        return extractFromMap(Lists.newArrayList(params.keySet()), params);
    }

    public static HashMap<String, Object> extractFromMap(List<String> keys, MultiValueMap<String, String> params) {
        HashMap<String, Object> _result = new HashMap<>();
        ArrayList<String> _array_keys = new ArrayList<String>();
        HashMap<String, List<String>> _unparsed_hash = new HashMap<>();
        for (String p : keys) {
            if (params.containsKey(p)) {
                if (isArrayKey(p)) {
                    String _new_key = p.replaceAll("\\[\\]", "");
                    if (!_array_keys.contains(_new_key)) {
                        _array_keys.add(_new_key);
                    }
                    continue;
                }
                if (isHashKey(p)) {
                    _unparsed_hash.put(p, params.get(p));
                    continue;
                }
                if (params.get(p).size() == 1) {
                    String _source = params.get(p).get(0);
                    _result.put(p, ExtractorHelper.extractObject(_source));
                } else if (params.get(p).size() >= 1) {
                    List<Object> _array = new ArrayList<Object>();
                    params.get(p).forEach(c -> {
                        _array.add(ExtractorHelper.extractObject(c));
                    });
                    _result.put(p, _array);
                }
            }
        }
        for (String p : _array_keys) {
            if (!_result.containsKey(p)) {
                _result.put(p, new ArrayList<Object>());
            }
            ((ArrayList<Object>) _result.get(p)).addAll(params.get(p + "[]"));
        }
        return _result;
    }



    public static AbstractJsonType asJsonType(MultiValueMap<String, String> params) {
        return asJsonType(Lists.newArrayList(params.keySet()), params);
    }

    public static AbstractJsonType asJsonType(List<String> keys, MultiValueMap<String, String> params) {
        return new AbstractJsonType() {
            private AbstractJsonType init(HashMap<String, Object> _props) {
                _props.forEach((k, v) -> setAdditionalProperty(k, v));
                return this;
            }
        }.init(extractFromMap(keys, params));
    }


    public static JsonNode parseQueryString(String query){

        JsonNode result = objectMapper.createObjectNode();
        try {
            String[] _q = URLDecoder.decode(query,"UTF-8").split("&");
            for(int i=0;i<_q.length;i++){
                String _kv[] = _q[i].split("=");
                if(_kv.length >= 2 ) {
                    result = normalizeParams(result, _kv[0], _kv[1]);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static JsonNode normalizeParams(JsonNode params,String name, Object v){
        Pattern namePattern = Pattern.compile("\\A[\\[\\]]*([^\\[\\]]+)\\]*");
        Pattern afterPattern = Pattern.compile("^\\[\\](.+)$");


        return params;
    }

}
