package no.cloudconsulting.agritech.config.aaa.exceptions;

/**
 * Created by denz0x13 on 05.12.16.
 */
public class InvalidJwtToken extends RuntimeException {
    private static final long serialVersionUID = -294671188037098603L;
}
