package no.cloudconsulting.agritech.config.aaa;

import no.cloudconsulting.agritech.domain.model.base.AuthEntity;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by denz0x13 on 28.10.15.
 */

public class AuditorAwareImpl implements AuditorAware<AuthEntity> {
    @Override
    public AuthEntity getCurrentAuditor() {
        try {
            return ((AuthEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        }catch (Exception e){

        }
        return null;
    }


}
