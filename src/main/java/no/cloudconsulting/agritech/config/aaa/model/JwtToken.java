package no.cloudconsulting.agritech.config.aaa.model;

/**
 * Created by denz0x13 on 05.12.16.
 */
public interface JwtToken {
    String getToken();
}
