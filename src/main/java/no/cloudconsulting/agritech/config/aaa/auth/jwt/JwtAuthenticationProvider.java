package no.cloudconsulting.agritech.config.aaa.auth.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import no.cloudconsulting.agritech.config.aaa.JwtSettings;
import no.cloudconsulting.agritech.config.aaa.auth.JwtAuthenticationToken;
import no.cloudconsulting.agritech.config.aaa.model.RawAccessJwtToken;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.services.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by denz0x13 on 05.12.16.
 */

@Component
@SuppressWarnings("unchecked")
public class JwtAuthenticationProvider implements AuthenticationProvider {
    private final JwtSettings jwtSettings;

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    public JwtAuthenticationProvider(JwtSettings jwtSettings) {
        this.jwtSettings = jwtSettings;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        RawAccessJwtToken rawAccessToken = (RawAccessJwtToken) authentication.getCredentials();

        Jws<Claims> jwsClaims = rawAccessToken.parseClaims(jwtSettings.getTokenSigningKey());
        String subject = jwsClaims.getBody().getSubject();
        User user = userManagementService.getByLoginName(subject);
        if(user == null){
            throw new UsernameNotFoundException(String.format("%s - not found",subject));
        }
        String tenantId = (String)jwsClaims.getBody().get("tenant_id");
        if(!user.tenantId().equals(tenantId)){
            throw new UsernameNotFoundException(String.format("%s - not found",subject));
        }
        List<String> scopes = jwsClaims.getBody().get("scopes", List.class);
        List<GrantedAuthority> authorities = scopes.stream()
                .map(authority -> new SimpleGrantedAuthority(authority))
                .collect(Collectors.toList());


        return new JwtAuthenticationToken(user, user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }
}