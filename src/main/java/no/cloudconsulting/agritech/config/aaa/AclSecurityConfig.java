package no.cloudconsulting.agritech.config.aaa;

import no.cloudconsulting.agritech.hibernate.MultiTenantAclCache;
import no.cloudconsulting.agritech.hibernate.MultiTenantDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.acls.AclPermissionEvaluator;
import org.springframework.security.acls.domain.*;
import org.springframework.security.acls.jdbc.BasicLookupStrategy;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.model.AclCache;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.acls.model.PermissionGrantingStrategy;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * Created by denz0x13 on 08.12.16.
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class AclSecurityConfig {
    @Autowired
    DataSource dataSource;

    DataSource getDataSource(){
        return new MultiTenantDataSource(dataSource);
    }

    @Bean
    public MutableAclService mutableAclService() throws PropertyVetoException {
        final JdbcMutableAclService mutableAclService = new JdbcMutableAclService(getDataSource(), lookupStrategy(), aclCache());
        mutableAclService.setClassIdentityQuery("select currval(pg_get_serial_sequence('acl_class', 'id'))");
        mutableAclService.setSidIdentityQuery("select currval(pg_get_serial_sequence('acl_sid', 'id'))");
        return (MutableAclService)mutableAclService;
    }

    @Bean
    public BasicLookupStrategy lookupStrategy() throws PropertyVetoException {
        return new BasicLookupStrategy(getDataSource(), aclCache(), aclAuthorizationStrategy(), auditLogger());
    }

    @Bean
    public AclCache aclCache() {
        return new MultiTenantAclCache(grantingStrategy(),aclAuthorizationStrategy());
        //return new EhCacheBasedAclCache(aclEhCacheFactoryBean().getObject(), grantingStrategy(), aclAuthorizationStrategy());
    }

    @Bean
    AclPermissionEvaluator permissionEvaluator() throws PropertyVetoException{
        return new AclPermissionEvaluator(mutableAclService());
    }

//    @Bean
//    public EhCacheFactoryBean aclEhCacheFactoryBean() {
//        EhCacheFactoryBean ehCacheFactoryBean = new EhCacheFactoryBean();
//        ehCacheFactoryBean.setCacheManager(ehCacheManagerFactoryBean().getObject());
//        ehCacheFactoryBean.setCacheName("aclCache");
//        return ehCacheFactoryBean;
//    }
//
//    @Bean
//    public EhCacheManagerFactoryBean ehCacheManagerFactoryBean() {
//        EhCacheManagerFactoryBean cacheManagerFactoryBean = new EhCacheManagerFactoryBean();
//
////        cacheManagerFactoryBean.setConfigLocation(new ClassPathResource("ehcache.xml"));
//        cacheManagerFactoryBean.setShared(true);
//
//        return cacheManagerFactoryBean;
//    }

    @Bean
    public AclAuthorizationStrategy aclAuthorizationStrategy() {
        return new AclAuthorizationStrategyImpl(
                new SimpleGrantedAuthority(Authorities.ADMIN.getRoleName()),
                new SimpleGrantedAuthority(Authorities.ADMIN.getRoleName()),
                new SimpleGrantedAuthority(Authorities.ADMIN.getRoleName()));
    }

    @Bean
    public PermissionGrantingStrategy grantingStrategy(){
        return new DefaultPermissionGrantingStrategy(auditLogger());
    }

    @Bean
    public AuditLogger auditLogger(){
        return new ConsoleAuditLogger();
    }

}
