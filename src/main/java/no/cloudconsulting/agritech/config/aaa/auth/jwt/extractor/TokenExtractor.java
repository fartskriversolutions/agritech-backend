package no.cloudconsulting.agritech.config.aaa.auth.jwt.extractor;

/**
 * Created by denz0x13 on 05.12.16.
 */
public interface TokenExtractor {
    public String extract(String payload);
}
