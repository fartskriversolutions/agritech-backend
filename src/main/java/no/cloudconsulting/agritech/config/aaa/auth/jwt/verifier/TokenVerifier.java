package no.cloudconsulting.agritech.config.aaa.auth.jwt.verifier;

/**
 * Created by denz0x13 on 05.12.16.
 */
public interface TokenVerifier {
    public boolean verify(String jti);
}
