package no.cloudconsulting.agritech.config.aaa.exceptions;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * Created by denz0x13 on 05.12.16.
 */

public class AuthMethodNotSupportedException extends AuthenticationServiceException {
    private static final long serialVersionUID = 3705043083010304496L;

    public AuthMethodNotSupportedException(String msg) {
        super(msg);
    }
}