package no.cloudconsulting.agritech.config.aaa.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by denz0x13 on 12.12.16.
 */
public class LoginResponse {
    @JsonProperty
    public String token="";
    @JsonProperty
    public String refreshToken="";
}
