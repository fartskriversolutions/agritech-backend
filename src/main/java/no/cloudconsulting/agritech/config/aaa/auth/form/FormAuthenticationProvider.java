package no.cloudconsulting.agritech.config.aaa.auth.form;

import no.cloudconsulting.agritech.config.aaa.Authorities;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.services.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * Created by denz0x13 on 15.12.16.
 */
@Component
public class FormAuthenticationProvider implements AuthenticationProvider {

    final UserManagementService userService;

    @Autowired
    public FormAuthenticationProvider(final UserManagementService userManagementService){
        this.userService = userManagementService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No authentication data provided");

        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        User user = userService.getByPasswordAndUserName(username,password);

        if(user == null){
            throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
        }

        if(!userService.hasRole(user,Authorities.ADMIN.getRoleName())){
            throw new InsufficientAuthenticationException("User has no roles assigned");
        }


        return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return  (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
