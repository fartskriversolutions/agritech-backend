package no.cloudconsulting.agritech.config.aaa.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by denz0x13 on 07.12.16.
 */
public class LoginForm {
    @JsonProperty
    public String username;
    @JsonProperty
    public String password;

    public LoginForm(){}

    public LoginForm(String u,String p){
        this.username = u;
        this.password = p;
    }
}
