package no.cloudconsulting.agritech.config;

import no.cloudconsulting.agritech.config.aaa.AuditorAwareImpl;
import no.cloudconsulting.agritech.domain.model.base.AuthEntity;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextListener;

/**
 * Created by denz0x13 on 15.12.15.
 */
@Component
@Configuration
public class AppConfig {
    @Bean
    public AuditorAware<AuthEntity> auditorProvider() {
        return new AuditorAwareImpl();
    }


    @Bean
    public TomcatEmbeddedServletContainerFactory tomcatEmbeddedServletContainerFactory() {
        TomcatEmbeddedServletContainerFactory tomcatEmbeddedServletContainerFactory = new TomcatEmbeddedServletContainerFactory();
        return tomcatEmbeddedServletContainerFactory;
    }

//    @Bean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
//    DispatcherServlet dispatcherServlet(ApplicationContext applicationContext, WebApplicationContext webApplicationContext){
//        DispatcherServlet dispatcherServlet = new DispatcherServlet(webApplicationContext);
//        dispatcherServlet.setThreadContextInheritable(true);
//        dispatcherServlet.setApplicationContext(applicationContext);
//        dispatcherServlet.setDispatchTraceRequest(true);
//        return dispatcherServlet;
//    }

//    @Bean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
//    DispatcherServlet dispatcherServlet(ApplicationContext applicationContext){
//        DispatcherServlet dispatcherServlet = new DispatcherServlet();
//        dispatcherServlet.setApplicationContext(applicationContext);
//        dispatcherServlet.setThreadContextInheritable(true);
//        dispatcherServlet.setDispatchTraceRequest(true);
//        return dispatcherServlet;
//    }
//
//    @Bean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME)
//    @ConditionalOnBean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME )
//    public ServletRegistrationBean dispatcherServletRegistration(DispatcherServlet dispatcherServlet){
//
//        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(dispatcherServlet, "/*");
//        servletRegistrationBean.setLoadOnStartup(1);
//        servletRegistrationBean.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
//        return servletRegistrationBean;
//    }


    @Bean
    public RequestContextListener requestContextListener(){
        return new RequestContextListener();
    }

//    @Bean
//    public FilterRegistrationBean requestContextFilter() {
//        FilterRegistrationBean bean = new FilterRegistrationBean();
//        bean.setFilter(new RequestContextFilter());
//        bean.setName(RequestContextFilter.class.getSimpleName());
//        bean.addUrlPatterns("/*");
//        return bean;
//    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
