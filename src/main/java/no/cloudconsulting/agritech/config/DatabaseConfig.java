package no.cloudconsulting.agritech.config;

import liquibase.integration.spring.MultiTenantSpringLiquibase;
import no.cloudconsulting.agritech.hibernate.CurrentTenantIdentifierResolverImpl;
import no.cloudconsulting.agritech.hibernate.CustomMultiTenantSpringLiquibase;
import no.cloudconsulting.agritech.hibernate.MultiTenantConnectionProviderImpl;
import org.hibernate.MultiTenancyStrategy;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by denz0x13 on 05.12.16.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("no.cloudconsulting.*")
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class DatabaseConfig implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    DataSource dataSource;

    @Autowired
    JpaProperties jpaProperties;

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }


    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        localContainerEntityManagerFactoryBean.setDataSource(dataSource);
        localContainerEntityManagerFactoryBean.setPackagesToScan("no.cloudconsulting.*");
        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
        Map<String, Object> jpaProperties = new HashMap<>();
        jpaProperties.put(org.hibernate.cfg.Environment.MULTI_TENANT, MultiTenancyStrategy.SCHEMA);
        jpaProperties.put(org.hibernate.cfg.Environment.MULTI_TENANT_CONNECTION_PROVIDER,
                multiTenantConnectionProvider());
        jpaProperties.put(org.hibernate.cfg.Environment.MULTI_TENANT_IDENTIFIER_RESOLVER,
                currentTenantIdentifierResolver());
        jpaProperties.put(org.hibernate.cfg.Environment.SHOW_SQL,true);
        localContainerEntityManagerFactoryBean.setJpaPropertyMap(jpaProperties);
        return localContainerEntityManagerFactoryBean;
    }

    @Bean
    public MultiTenantConnectionProvider multiTenantConnectionProvider() {
        return new MultiTenantConnectionProviderImpl();
    }

    @Bean
    public CurrentTenantIdentifierResolver currentTenantIdentifierResolver() {
        return new CurrentTenantIdentifierResolverImpl();
    }

    @Bean(name = "multiTenantSpringLiquibase")
    public MultiTenantSpringLiquibase liquibaseMultitenant(DataSource dataSource) {
        CustomMultiTenantSpringLiquibase customMultiTenantSpringLiquibase = new CustomMultiTenantSpringLiquibase();
        customMultiTenantSpringLiquibase.setChangeLog("classpath:liquibase/update.sql");
        customMultiTenantSpringLiquibase.setDataSource(dataSource);
        return customMultiTenantSpringLiquibase;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        event.getApplicationContext().getAutowireCapableBeanFactory().destroyBean(event.getApplicationContext().getBean("multiTenantSpringLiquibase"));
    }
}
