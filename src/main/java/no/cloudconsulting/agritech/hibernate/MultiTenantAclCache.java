package no.cloudconsulting.agritech.hibernate;

import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.springframework.security.acls.domain.AclAuthorizationStrategy;
import org.springframework.security.acls.domain.EhCacheBasedAclCache;
import org.springframework.security.acls.model.AclCache;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.PermissionGrantingStrategy;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by denz0x13 on 12.12.16.
 */
public class MultiTenantAclCache implements AclCache {

    private final HashMap<String,EhCacheBasedAclCache> _caches = new HashMap<>();
    private final PermissionGrantingStrategy permissionGrantingStrategy;
    private final AclAuthorizationStrategy aclAuthorizationStrategy;


    public MultiTenantAclCache(PermissionGrantingStrategy permissionGrantingStrategy,
                                AclAuthorizationStrategy aclAuthorizationStrategy) {
        Assert.notNull(permissionGrantingStrategy, "PermissionGrantingStrategy required");
        Assert.notNull(aclAuthorizationStrategy, "AclAuthorizationStrategy required");
        this.permissionGrantingStrategy = permissionGrantingStrategy;
        this.aclAuthorizationStrategy = aclAuthorizationStrategy;
    }


    EhCacheBasedAclCache getWrapped(){
        String currentTenant = TenantHelper.getCurrentTenantName();
        if(_caches.containsKey(currentTenant)){
            return _caches.get(currentTenant);
        }
        net.sf.ehcache.CacheManager singletonManager = net.sf.ehcache.CacheManager.create();
        String newCacheName = String.format("aclCache_%s",currentTenant);
        net.sf.ehcache.Ehcache managedCache = singletonManager.getEhcache(newCacheName);

        if (managedCache == null) {
            // add to the manager
            // TODO How do we get specific configurations??
            singletonManager.addCache(newCacheName);
            managedCache = singletonManager.getEhcache(newCacheName);
        }

        EhCacheBasedAclCache ehCacheBasedAclCache = new EhCacheBasedAclCache(managedCache,permissionGrantingStrategy,aclAuthorizationStrategy);
        _caches.put(currentTenant,ehCacheBasedAclCache);

        return ehCacheBasedAclCache;


    }


    @Override
    public void evictFromCache(Serializable pk) {
        getWrapped().evictFromCache(pk);
    }

    @Override
    public void evictFromCache(ObjectIdentity objectIdentity) {
        getWrapped().evictFromCache(objectIdentity);
    }

    @Override
    public MutableAcl getFromCache(ObjectIdentity objectIdentity) {
        return getWrapped().getFromCache(objectIdentity);
    }

    @Override
    public MutableAcl getFromCache(Serializable pk) {
        return getWrapped().getFromCache(pk);
    }

    @Override
    public void putInCache(MutableAcl acl) {
        getWrapped().putInCache(acl);
    }

    @Override
    public void clearCache() {
        getWrapped().clearCache();
    }
}
