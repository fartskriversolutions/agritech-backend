package no.cloudconsulting.agritech.hibernate;

import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * Created by denz0x13 on 09.12.16.
 */

public class MultiTenantDataSource implements DataSource {

    DataSource dataSource;

    public MultiTenantDataSource(DataSource dataSource){
        this.dataSource = dataSource;
    }

    Connection getAnyConnection() throws SQLException {
        return dataSource.getConnection();
    }

    Connection getAnyConnection(String username, String password) throws SQLException {
        return dataSource.getConnection(username,password);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return setup4Tenant(dataSource.getConnection());
    }

    Connection setup4Tenant(final Connection connection){
        String tenantIdentifier = TenantHelper.getCurrentTenantName();
        if (StringUtils.isBlank(tenantIdentifier)) {
            tenantIdentifier = "default";
        }
        try {
            connection.createStatement().execute("set search_path = " + tenantIdentifier);
        } catch (SQLException e) {
            throw new HibernateException("Could not alter JDBC connection to specified schema [" + tenantIdentifier + "]", e);
        }
        return connection;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return setup4Tenant(dataSource.getConnection(username,password));
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return dataSource.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return dataSource.isWrapperFor(iface);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return dataSource.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        dataSource.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        dataSource.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return dataSource.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
