package no.cloudconsulting.agritech.hibernate;

import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {

    private final Logger log = LoggerFactory.getLogger(CurrentTenantIdentifierResolverImpl.class);

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenant = TenantHelper.getCurrentTenantName();
        log.info("++++ Curennt tenat is ==> "+tenant);
        return tenant;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }

}
