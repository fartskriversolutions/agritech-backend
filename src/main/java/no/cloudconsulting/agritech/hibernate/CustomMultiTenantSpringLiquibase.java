package no.cloudconsulting.agritech.hibernate;

import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.MultiTenantSpringLiquibase;
import liquibase.integration.spring.SpringLiquibase;
import no.cloudconsulting.agritech.domain.services.UserManagementService;
import no.cloudconsulting.agritech.tenant.Tenant;
import no.cloudconsulting.agritech.tenant.TenantService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.core.io.DefaultResourceLoader;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by evilsun.
 * at 9/15/15
 */
public class CustomMultiTenantSpringLiquibase extends MultiTenantSpringLiquibase implements DisposableBean {

    @Value("${spring.datasource.url}")
    private String datasourceUrl;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    @Autowired
    protected TenantService tenantService;

    @Autowired
    protected UserManagementService userManagementService;

    private Logger log = Logger.getLogger(CustomMultiTenantSpringLiquibase.class);

    private Map<String, DataSource> tenantsDataSources = new HashMap<>();

    public CustomMultiTenantSpringLiquibase() {
    }

    public CustomMultiTenantSpringLiquibase(String datasourceUrl, String username, String password, String driverClassName) {
        this.datasourceUrl = datasourceUrl;
        this.username = username;
        this.password = password;
        this.driverClassName = driverClassName;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        tenantsDataSources = getEnabledTenantsIdsAndDataSource();
        if (tenantsDataSources == null) {
            log.warn("Unable to perform database upgrade - tenantsDataSources IS NULL");
            return;
        }
        createSchemasIfnotExists();
        runOnAllDataSources();
    }

    public void createTenantSchema(String tenantId) {
        tenantsDataSources.put(tenantId, getDatasourceByTenantId(tenantId));
        createSchemasIfnotExists();
        runOnAllDataSources();
    }

    private void createSchemasIfnotExists() {
        for (Map.Entry<String, DataSource> entry : tenantsDataSources.entrySet()) {
            try (PreparedStatement stmt = getDataSource().getConnection().prepareStatement("create schema if not exists " + entry.getKey())) {
                stmt.execute();
            } catch (SQLException e) {
                log.error("Exception during schema " + entry.getKey() + " creation ", e);
            }
        }
    }

    private void runOnAllDataSources() {
        for (Map.Entry<String, DataSource> entry : tenantsDataSources.entrySet()) {
            log.info("Initializing Liquibase for data source " + entry.getValue());
            try {
                getSpringLiquibase(entry.getKey(), entry.getValue()).afterPropertiesSet();

            } catch (LiquibaseException e) {
                log.error("Error during schema creation ", e);
            }
            log.info("Liquibase ran for data source " + entry.getValue());
        }
    }

    private SpringLiquibase getSpringLiquibase(String tenantId, DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog(getChangeLog());
        liquibase.setChangeLogParameters(getParameters());
        liquibase.setContexts(getContexts());
        liquibase.setLabels(getLabels());
        liquibase.setDropFirst(isDropFirst());
        liquibase.setShouldRun(isShouldRun());
        liquibase.setRollbackFile(getRollbackFile());
        liquibase.setResourceLoader(new DefaultResourceLoader());
        liquibase.setDataSource(dataSource);
        liquibase.setDefaultSchema(tenantId);
        return liquibase;
    }

    @Override
    public void destroy() {
        if (tenantsDataSources != null) {
            tenantsDataSources.entrySet().stream().forEach(e -> ((org.apache.tomcat.jdbc.pool.DataSource) e.getValue()).close());
            tenantsDataSources = null;
        }
        log.info(" ++++ Destroying " + this.getClass().getSimpleName());
    }

    public DataSource getDatasourceByTenantId(String tenantId) {
        return DataSourceBuilder.create()
                .username(username)
                .password(password)
                .url(datasourceUrl + "?currentSchema=" + tenantId)
                .driverClassName(driverClassName)
                .build();
    }

    public Map<String, DataSource> getEnabledTenantsIdsAndDataSource() {
        return tenantService.getEnabledTenants().stream()
                .collect(Collectors.toMap(Tenant::getTenantId, tenant -> getDatasourceByTenantId(tenant.getTenantId())));
    }

}
