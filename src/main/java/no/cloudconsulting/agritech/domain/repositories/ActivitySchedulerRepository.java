package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.ActivityScheduler;

import java.util.List;

/**
 * Created by denz0x13 on 23.12.16.
 */
public interface ActivitySchedulerRepository extends IRepository<ActivityScheduler> {
    List<ActivityScheduler> findByActivityId(Long activityId);
}
