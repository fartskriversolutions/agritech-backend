package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.Location;
import org.springframework.stereotype.Repository;

/**
 * Created by denz0x13 on 28.12.16.
 */
@Repository
public interface LocationRepository extends IRepository<Location> {
}
