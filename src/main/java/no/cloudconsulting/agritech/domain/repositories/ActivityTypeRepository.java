package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.ActivityType;
import org.springframework.stereotype.Repository;

/**
 * Created by denz0x13 on 23.12.16.
 */
@Repository
public interface ActivityTypeRepository extends IRepository<ActivityType> {
    ActivityType findFirstByName(String name);
}
