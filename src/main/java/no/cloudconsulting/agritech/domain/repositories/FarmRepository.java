package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.Farm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by denz0x13 on 23.12.16.
 */
@Repository
public interface FarmRepository extends ULRepository<Farm> {
    Page<Farm> findByClusterId(Long clusterId, Pageable pageable);
    List<Farm> findByClusterId(Long clusterId);
    Long countByClusterId(List<Long> ids);
}
