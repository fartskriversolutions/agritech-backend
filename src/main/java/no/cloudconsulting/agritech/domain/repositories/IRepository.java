package no.cloudconsulting.agritech.domain.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * Created by denz0x13 on 02.12.15.
 */
@NoRepositoryBean
public interface IRepository<T> extends CrudRepository<T,Long>,JpaSpecificationExecutor<T> {
    @Query("select u.id from #{#entityName} u")
    List<Long> findAllIds();
}
