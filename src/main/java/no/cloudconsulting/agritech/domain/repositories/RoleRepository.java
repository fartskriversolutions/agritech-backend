package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.Role;
import org.springframework.stereotype.Repository;

/**
 * Created by denz0x13 on 15.12.15.
 */
@Repository
public interface RoleRepository extends IRepository<Role> {
    Role findFirstByName(String name);
}
