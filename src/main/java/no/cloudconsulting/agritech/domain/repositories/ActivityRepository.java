package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.Activity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by denz0x13 on 23.12.16.
 */
@Repository
public interface ActivityRepository extends IRepository<Activity> {
    List<Activity> findByActivityTypeId(Long activityTypeId);
    Long countByActivityTypeId(Long activityTypeId);
}
