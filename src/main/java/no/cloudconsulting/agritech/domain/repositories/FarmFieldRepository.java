package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.FarmField;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by denz0x13 on 23.12.16.
 */
@Repository
public interface FarmFieldRepository extends ULRepository<FarmField> {
    Long countByFarmId(List<Long> ids);
}
