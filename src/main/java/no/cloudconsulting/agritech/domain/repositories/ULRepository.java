package no.cloudconsulting.agritech.domain.repositories;

import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * Created by denz0x13 on 23.12.16.
 */
@NoRepositoryBean
public interface ULRepository<T> extends IRepository<T> {
    List<T> findByUserId(Long userId);
    List<T> findByLocationId(Long locationId);
}
