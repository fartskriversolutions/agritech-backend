package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.UsersRoles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by denz0x13 on 14.12.16.
 */
public interface UsersRolesRepository extends JpaRepository<UsersRoles,Long> {
    List<UsersRoles> findAllByRoleId(Long roleId);
}
