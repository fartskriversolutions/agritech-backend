package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.Cluster;
import org.springframework.stereotype.Repository;

/**
 * Created by denz0x13 on 23.12.16.
 */
@Repository
public interface ClusterRepository extends ULRepository<Cluster>  {
}
