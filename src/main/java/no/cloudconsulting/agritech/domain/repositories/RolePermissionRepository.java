package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.RolePermission;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by denz0x13 on 12/20/16.
 */
@Repository
public interface RolePermissionRepository extends IRepository<RolePermission>  {
    List<RolePermission> findByMask(Integer mask);
    List<RolePermission> findByClassName(String className);
    List<RolePermission> findByRoleId(Long roleId);
    List<RolePermission> findByRoleIdAndClassName(Long roleId,String className);
    List<RolePermission> findByRoleIdAndClassNameAndMask(Long roleId, String className, Integer mask);
    List<RolePermission> findByRoleIdAndClassNameAndMask(Long roleId, String className, List<Integer> masks);
    void deleteByRoleId(Long roleId);
    void deleteByRoleIdAndClassNameAndMask(Long roleId, String className, Integer mask);
}
