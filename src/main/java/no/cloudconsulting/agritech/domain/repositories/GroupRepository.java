package no.cloudconsulting.agritech.domain.repositories;

import no.cloudconsulting.agritech.domain.model.Group;
import org.springframework.stereotype.Repository;

/**
 * Created by denz0x13 on 08.12.16.
 */
@Repository
public interface GroupRepository extends IRepository<Group> {
}
