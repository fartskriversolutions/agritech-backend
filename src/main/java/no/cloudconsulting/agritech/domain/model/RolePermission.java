package no.cloudconsulting.agritech.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import no.cloudconsulting.agritech.domain.model.base.UserAuditEntity;
import no.cloudconsulting.agritech.domain.services.RolifyEntityService;
import no.cloudconsulting.agritech.helpers.Permissions;

import javax.persistence.*;

/**
 * Created by denz0x13 on 19.12.16.
 */
@Entity(name = "RolePermission")
@Table(name = "role_permissions")
public class RolePermission extends UserAuditEntity{

    private Long roleId;
    private Role role;
    private String className;
    private Integer mask;
    private Boolean granting;


    @JsonIgnore
    @Column(name = "mask", nullable = false, unique = false)
    public Integer getMask() {
        return mask;
    }

    public void setMask(Integer mask) {
        this.mask = mask;
    }

    @JsonIgnore
    @Column(name = "granting", nullable = false, unique = false)
    public Boolean getGranting() {
        return granting;
    }

    public void setGranting(Boolean granting) {
        this.granting = granting;
    }

    @JsonIgnore
    @Column(name = "class_name")
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @JsonIgnore
    @JoinColumn(name = "role_id")
    @OneToOne
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @JsonIgnore
    @Column(name = "role_id", insertable = false, updatable = false)
    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Transient
    @JsonProperty
    public RolifyEntityService.RolifyEntry entityName(){
        return RolifyEntityService.displayNameFor(className);
    }

    @Transient
    @JsonProperty
    public  String permission(){
        return Permissions.fromMask(mask);
    }
}
