package no.cloudconsulting.agritech.domain.model;

import no.cloudconsulting.agritech.domain.model.base.AuditAbleEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by denz0x13 on 12/22/16.
 */
@Entity(name = "Location")
@Table(name = "locations")
public class Location extends AuditAbleEntity {
    private Double latitude;
    private Double longitude;

    @Column(name = "latitude")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Column(name = "longitude")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
