package no.cloudconsulting.agritech.domain.model.api;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by denz0x13 on 11.01.17.
 */
public class ActivityApiPojo extends BaseApiPojo {
    public String name;
    public Long activityTypeId;
    public String activityTypeName;
    public Long farmFieldId;
    public String description;
    public JsonNode reportFields;
}
