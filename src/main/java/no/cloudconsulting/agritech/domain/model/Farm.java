package no.cloudconsulting.agritech.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import no.cloudconsulting.agritech.domain.annotations.RolifyEntity;
import no.cloudconsulting.agritech.domain.model.base.IAmChildOf;
import no.cloudconsulting.agritech.domain.model.base.IAmParentOf;
import no.cloudconsulting.agritech.domain.model.base.ULUserAuditEntity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by denz0x13 on 12/22/16.
 */
@Entity(name = "Farm")
@Table(name = "farms")
@RolifyEntity(name = "Farm")
public class Farm extends ULUserAuditEntity implements IAmChildOf<Cluster>,IAmParentOf<FarmField> {
    private String name;
    private Cluster cluster;
    private Long clusterId;
    private Boolean enabled;

    private Set<FarmField> farmFields;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @JoinColumn(name = "cluster_id")
    @ManyToOne
    @JsonIgnore
    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    @Column(name = "enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "cluster_id",insertable = false,updatable = false, nullable = true)
    public Long getClusterId() {
        return clusterId;
    }

    public void setClusterId(Long clusterId) {
        this.clusterId = clusterId;
    }

    @Override
    @Transient
    @JsonIgnore
    public Long getParentId() {
        return getClusterId();
    }

    @Override
    @Transient
    @JsonIgnore
    public Cluster getParent() {
        return getCluster();
    }

    @Override
    public void setParent(Cluster parent) {
        setCluster(cluster);
    }

    @Override
    @Transient
    @JsonIgnore
    public Set<FarmField> getChildren() {
        return getFarmFields();
    }

    @OneToMany
    @JoinColumn(name = "farm_id")
    public Set<FarmField> getFarmFields() {
        return farmFields;
    }


    public void setFarmFields(Set<FarmField> farmFields) {
        this.farmFields = farmFields;
    }
}
