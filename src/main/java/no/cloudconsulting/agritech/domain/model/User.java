package no.cloudconsulting.agritech.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import no.cloudconsulting.agritech.domain.annotations.RolifyEntity;
import no.cloudconsulting.agritech.domain.model.base.AuthEntity;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by denz0x13 on 15.12.15.
 */
@Entity(name = "user")
@Table(name = "users")
@JsonInclude(JsonInclude.Include.NON_NULL)
@RolifyEntity(name = "User")
public class User extends AuthEntity<Role> {

    private Long profileId;
    private Set<Role> roles=new HashSet<>();


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "users_roles",joinColumns = @JoinColumn(name="user_id"),inverseJoinColumns = @JoinColumn(name = "role_id"))
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    @Transient
    @JsonIgnore
    public Set<Role> getAuthRoles() {
        return getRoles();
    }

    private String firstName;

    @Basic
    @Column(name = "first_name", nullable = true, insertable = true, updatable = true, length = 255)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String lastName;

    @Basic
    @Column(name = "last_name", nullable = true, insertable = true, updatable = true, length = 255)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void addRole(Role role){


        if(roles == null){
            roles = new HashSet<>();
        }
        if(roles.stream().filter(r -> r.getName().equals(role.getName())).count() == 0) {
            roles.add(role);
        }
    }

    @Column(name = "profile_id")
    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    @Override
    public String toString() {
        try {
            return String.format("%s %s [%s]",getFirstName(),getLastName(),getLoginName());
        }catch (Exception e){

        }
        return super.toString();
    }


    @Override
    @Transient
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName()))
                .collect(Collectors.toList());
    }

    @Override
    @Transient
    public String getUsername() {
        return getLoginName();
    }

    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    @Transient
    public boolean isEnabled() {
        return true;
    }

    @Transient
    public String tenantId(){
        return TenantHelper.getCurrentTenantName();
    }


}
