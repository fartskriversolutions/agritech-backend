package no.cloudconsulting.agritech.domain.model;

import com.fasterxml.jackson.databind.JsonNode;
import no.cloudconsulting.agritech.domain.annotations.RolifyEntity;
import no.cloudconsulting.agritech.domain.model.base.UserAuditEntity;
import no.cloudconsulting.agritech.hibernate.JSONObjectUserType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * Created by denz0x13 on 12/22/16.
 */
@Entity(name = "Activity")
@Table(name = "activities")
@TypeDefs({ @TypeDef(name = "JsonObject", typeClass = JSONObjectUserType.class)})
@RolifyEntity(name = "Activity")
public class Activity extends UserAuditEntity {
    private String name;
    private ActivityType activityType;
    private Long activityTypeId;
    private FarmField farmField;
    private Long farmFieldId;
    private String description;
    private JsonNode reportFields;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JoinColumn(name = "activity_type_id")
    @ManyToOne
    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Type(type = "JsonObject")
    @Column(name = "report_fields")
    public JsonNode getReportFields() {
        return reportFields;
    }

    public void setReportFields(JsonNode reportFields) {
        this.reportFields = reportFields;
    }

    @Column(name = "activity_type_id", updatable = false, insertable = false)
    public Long getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(Long activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "farm_field_id")
    public FarmField getFarmField() {
        return farmField;
    }

    public void setFarmField(FarmField farmField) {
        this.farmField = farmField;
    }

    @Column(name = "farm_field_id",insertable = false,updatable = false)
    public Long getFarmFieldId() {
        return farmFieldId;
    }

    public void setFarmFieldId(Long farmFieldId) {
        this.farmFieldId = farmFieldId;
    }
}
