package no.cloudconsulting.agritech.domain.model;

import no.cloudconsulting.agritech.domain.model.base.UserAuditEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by denz0x13 on 08.12.16.
 */
@Entity(name = "group")
@Table(name = "groups")
public class Group extends UserAuditEntity {
    private String name;
    private String description;
    @Basic
    @Column(name = "name", nullable = true, insertable = true, updatable = true, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 255)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private Set<Role> roles=new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "groups_roles",joinColumns = @JoinColumn(name="group_id"),inverseJoinColumns = @JoinColumn(name = "role_id"))
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
