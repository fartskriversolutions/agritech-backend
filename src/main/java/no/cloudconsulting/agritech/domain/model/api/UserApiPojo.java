package no.cloudconsulting.agritech.domain.model.api;

import java.util.List;

/**
 * Created by denz0x13 on 12/22/16.
 */
public class UserApiPojo extends BaseApiPojo {
    public String firstName;
    public String lastName;
    public String email;
    public String loginName;
    public String password;
    public List<Long> roleIds;
}
