package no.cloudconsulting.agritech.domain.model.api;

/**
 * Created by denz0x13 on 11.01.17.
 */
public class ActivitySchedulerApiPojo extends BaseApiPojo {
    public String name;
    public Long activityId;
    public Long startTime;
    public Long endTime;
}
