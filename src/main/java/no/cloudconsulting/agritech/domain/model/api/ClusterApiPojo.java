package no.cloudconsulting.agritech.domain.model.api;

/**
 * Created by denz0x13 on 28.12.16.
 */
public class ClusterApiPojo extends ULBaseApiPojo {
    public String name;

    public String typography;
    public String infrastructure;
    public String irrigationSource;
    public Boolean soilAnalysis;

}
