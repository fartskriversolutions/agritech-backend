package no.cloudconsulting.agritech.domain.model.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by denz0x13 on 12/22/16.
 */
public class RoleApiPojo extends BaseApiPojo {
    @JsonProperty
    public String name;
    @JsonProperty
    public String description;
    @JsonProperty
    public List<PermissionsMapPojo> entityPermissions;

    @JsonIgnore
    public RoleApiPojo addEntityPermission(String entity,String... perm){
        PermissionsMapPojo permissionsMapPojo = new PermissionsMapPojo();
        permissionsMapPojo.entityName = entity;
        permissionsMapPojo.permissions = Arrays.asList(perm);
        if(entityPermissions==null){
            entityPermissions = new ArrayList<>();
        }
        entityPermissions.add(permissionsMapPojo);
        return this;
    }

}
