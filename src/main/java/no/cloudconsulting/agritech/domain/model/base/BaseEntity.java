package no.cloudconsulting.agritech.domain.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by denz0x13 on 15.12.15.
 */
@MappedSuperclass
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class BaseEntity implements Serializable {
    private Long id;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(insertable = false,updatable = false)
    @ApiModelProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Transient
    @JsonIgnore
    public JsonNode toJson(){
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.valueToTree(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass() || this.getId()==null) return false;

        return this.getId() == ((BaseEntity)o).getId();
    }

    @Override
    public int hashCode() {
        if(this.getId()==null){
            return 0;
        }
        return Long.hashCode(this.getId());
    }
}
