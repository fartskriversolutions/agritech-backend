package no.cloudconsulting.agritech.domain.model;


import no.cloudconsulting.agritech.domain.annotations.RolifyEntity;
import no.cloudconsulting.agritech.domain.model.base.RoleEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by denz0x13 on 15.12.15.
 */
@Entity(name = "role")
@Table(name = "roles")
@RolifyEntity(name = "Role")
public class Role extends RoleEntity {

    private Set<RolePermission> rolePermissions = new HashSet<>();

    @Override
    public String toString() {
        return this.getName();
    }

    @OneToMany
    @JoinColumn(name = "role_id")
    public Set<RolePermission> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(Set<RolePermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }

    public void addRolePermissions(RolePermission rolePermission){
        rolePermissions.add(rolePermission);
    }
}
