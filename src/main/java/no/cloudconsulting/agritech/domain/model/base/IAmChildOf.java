package no.cloudconsulting.agritech.domain.model.base;

/**
 * Created by denz0x13 on 28.12.16.
 */
public interface IAmChildOf<T extends BaseEntity> {
    Long getParentId();
    T getParent();
    void setParent(T parent);

}
