package no.cloudconsulting.agritech.domain.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import no.cloudconsulting.agritech.domain.model.Location;
import no.cloudconsulting.agritech.domain.model.User;

import javax.persistence.*;

/**
 * Created by denz0x13 on 28.12.16.
 */
@MappedSuperclass
public abstract class ULUserAuditEntity extends UserAuditEntity {
    private Location location;
    private Long locationId;
    private User user;
    private Long userId;

    @Transient
    @JsonProperty
    public Double latitude(){
        if(location!=null){
            return location.getLatitude();
        }
        return null;
    }

    @Transient
    @JsonProperty
    public Double longitude(){
        if(location!=null){
            return location.getLongitude();
        }
        return null;
    }

    @JoinColumn(name = "location_id")
    @OneToOne
    @JsonIgnore
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }



    @JoinColumn(name = "user_id")
    @ManyToOne
    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "location_id",insertable = false,updatable = false)
    @JsonIgnore
    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    @Column(name = "user_id",insertable = false,updatable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
