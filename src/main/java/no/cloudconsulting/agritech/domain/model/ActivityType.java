package no.cloudconsulting.agritech.domain.model;

import no.cloudconsulting.agritech.domain.model.base.AuditAbleEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by denz0x13 on 12/22/16.
 */
@Entity(name = "ActivityType")
@Table(name = "activity_types")
public class ActivityType extends AuditAbleEntity {
    private String name;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
