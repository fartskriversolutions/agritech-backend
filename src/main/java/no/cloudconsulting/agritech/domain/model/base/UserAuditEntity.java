package no.cloudconsulting.agritech.domain.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import no.cloudconsulting.agritech.domain.model.User;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * Created by denz0x13 on 24.12.15.
 */
@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
public abstract class UserAuditEntity extends AuditAbleEntity {

    protected User createdBy;
    protected User updatedBy;

    @JsonIgnore
    @OneToOne(cascade = {CascadeType.MERGE},fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by",referencedColumnName = "id")
    @CreatedBy
    public User getCreatedBy(){
        return this.createdBy;
    }

    public void setCreatedBy(User user){
        this.createdBy = user;
    }

    @JsonIgnore
    @JoinColumn(name = "updated_by",referencedColumnName = "id")
    @OneToOne(cascade = {CascadeType.MERGE},fetch = FetchType.LAZY)
    @LastModifiedBy
    public User getUpdatedBy(){
        return this.updatedBy;
    }

    public void setUpdatedBy(User user){
        this.updatedBy = user;
    }
}
