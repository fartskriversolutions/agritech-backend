package no.cloudconsulting.agritech.domain.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denz0x13 on 26.12.16.
 */
public class PermissionsMapPojo {
    @JsonProperty
    public String entityName;
    @JsonProperty
    public List<String> permissions;

    public PermissionsMapPojo() {
        this.entityName = "";
        permissions = new ArrayList<>();
    }
}
