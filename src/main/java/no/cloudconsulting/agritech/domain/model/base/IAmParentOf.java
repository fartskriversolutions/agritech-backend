package no.cloudconsulting.agritech.domain.model.base;

import com.google.common.reflect.TypeToken;

import java.util.Collections;
import java.util.Set;

/**
 * Created by denz0x13 on 20.01.17.
 */
public interface IAmParentOf<T extends BaseEntity> {

    default Boolean hasChildren(){
        if(getChildren()==null){
            return false;
        }
        return getChildren().size() > 0;
    }
    default Set<T> getChildren(){
        return Collections.emptySet();
    }

    default Class<T> getChildrenClass(){
        return (Class<T>)(new TypeToken<T>(getClass()){}).getRawType();
    }
}
