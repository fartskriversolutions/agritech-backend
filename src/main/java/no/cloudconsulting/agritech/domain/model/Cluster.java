package no.cloudconsulting.agritech.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import no.cloudconsulting.agritech.domain.annotations.RolifyEntity;
import no.cloudconsulting.agritech.domain.model.base.IAmParentOf;
import no.cloudconsulting.agritech.domain.model.base.ULUserAuditEntity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by denz0x13 on 12/22/16.
 */
@Entity(name = "Cluster")
@Table(name = "clusters")
@RolifyEntity(name = "Cluster")
public class Cluster extends ULUserAuditEntity implements IAmParentOf<Farm> {
    private String name;

    private String typography;
    private String infrastructure;
    private String irrigationSource;
    private Boolean soilAnalysis;

    private Set<Farm> farms;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Column(name = "typography")
    public String getTypography() {
        return typography;
    }

    public void setTypography(String typography) {
        this.typography = typography;
    }

    @Column(name = "infrastructure")
    public String getInfrastructure() {
        return infrastructure;
    }

    public void setInfrastructure(String infrastructure) {
        this.infrastructure = infrastructure;
    }

    @Column(name = "irrigation_source")
    public String getIrrigationSource() {
        return irrigationSource;
    }

    public void setIrrigationSource(String irrigationSource) {
        this.irrigationSource = irrigationSource;
    }

    @Column(name = "soil_analysis")
    public Boolean getSoilAnalysis() {
        return soilAnalysis;
    }

    public void setSoilAnalysis(Boolean soilAnalysis) {
        this.soilAnalysis = soilAnalysis;
    }

    @OneToMany
    @JoinColumn(name = "cluster_id")
    public Set<Farm> getFarms() {
        return farms;
    }

    public void setFarms(Set<Farm> farms) {
        this.farms = farms;
    }

    @Override
    @Transient
    @JsonIgnore
    public Set<Farm> getChildren() {
        return getFarms();
    }
}
