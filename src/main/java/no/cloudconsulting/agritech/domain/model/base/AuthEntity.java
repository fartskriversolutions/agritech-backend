package no.cloudconsulting.agritech.domain.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.util.Set;

/**
 * Created by denz0x13 on 15.12.15.
 */
@MappedSuperclass
public abstract class AuthEntity<T extends RoleEntity> extends UserAuditEntity implements UserDetails{
    private String email;

    @Basic
    @Column(name = "email", nullable = false, insertable = true, updatable = true, length = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String loginName;

    @Basic
    @Column(name = "login_name", nullable = false, insertable = true, updatable = true, length = 255)
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }



    private String password;

    @Basic
    @Column(name = "password", nullable = true, insertable = true, updatable = true, length = 255)
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public abstract Set<T> getAuthRoles();


    @Transient
    public String toString(){
        return String.format("%s",getLoginName());
    }

}
