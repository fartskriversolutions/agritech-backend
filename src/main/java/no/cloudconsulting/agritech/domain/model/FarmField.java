package no.cloudconsulting.agritech.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import no.cloudconsulting.agritech.domain.annotations.RolifyEntity;
import no.cloudconsulting.agritech.domain.model.base.IAmChildOf;
import no.cloudconsulting.agritech.domain.model.base.ULUserAuditEntity;

import javax.persistence.*;

/**
 * Created by denz0x13 on 23.12.16.
 */
@Table(name = "farm_fields")
@Entity(name = "FarmField")
@RolifyEntity(name = "FarmField")
public class FarmField extends ULUserAuditEntity implements IAmChildOf<Farm> {
    private String name;
    private Farm farm;
    private Long farmId;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "farm_id")
    @JsonIgnore
    public Farm getFarm() {
        return farm;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

    @Override
    @Transient
    @JsonIgnore
    public Long getParentId() {
        return getFarmId();
    }

    @Override
    @Transient
    @JsonIgnore
    public Farm getParent() {
        return farm;
    }

    @Override
    @Transient
    @JsonIgnore
    public void setParent(Farm parent) {
        this.farm = parent;
    }

    @Column(name = "farm_id", insertable = false, updatable = false)
    public Long getFarmId() {
        return farmId;
    }

    public void setFarmId(Long farmId) {
        this.farmId = farmId;
    }
}
