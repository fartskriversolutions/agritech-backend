package no.cloudconsulting.agritech.domain.model;

import no.cloudconsulting.agritech.domain.annotations.RolifyEntity;
import no.cloudconsulting.agritech.domain.model.base.AuditAbleEntity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by denz0x13 on 23.12.16.
 */
@Entity(name = "ActivityScheduler")
@Table(name = "activity_schedulers")
@RolifyEntity(name = "ActivityScheduler")
public class ActivityScheduler extends AuditAbleEntity {
    private String name;
    private Activity activity;
    private Long activityId;
    private Timestamp startTime;
    private Timestamp endTime;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JoinColumn(name = "activity_id")
    @ManyToOne
    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Column(name = "start_time")
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Column(name= "end_time")
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Column(name = "activity_id",insertable = false,updatable = false)
    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }
}
