package no.cloudconsulting.agritech.domain.model.api;

/**
 * Created by denz0x13 on 29.12.16.
 */
public class FarmFieldApiPojo extends ULBaseApiPojo {
    public String name;
    public Long farmId;
}
