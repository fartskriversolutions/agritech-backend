package no.cloudconsulting.agritech.domain.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by denz0x13 on 14.12.16.
 */
@Entity(name = "UsersRoles")
@Table(name = "user_roles")
@Immutable
public class UsersRoles implements Serializable{
    private Long userId;
    private Long roleId;

    @Id
    @Column(name = "user_id", nullable = true, insertable = false, updatable = false)
    public Long getUserId() {
        return userId;
    }

    @Column(name = "role_id", nullable = true, insertable = false, updatable = false)
    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
