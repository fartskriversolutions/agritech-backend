package no.cloudconsulting.agritech.domain.model.api;

/**
 * Created by denz0x13 on 28.12.16.
 */
public class ULBaseApiPojo extends BaseApiPojo {
    public Long userId;
    public Double latitude;
    public Double longitude;
}
