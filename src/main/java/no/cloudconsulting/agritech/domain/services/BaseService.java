package no.cloudconsulting.agritech.domain.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import no.cloudconsulting.agritech.config.aaa.Authorities;
import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import no.cloudconsulting.agritech.domain.model.Role;
import no.cloudconsulting.agritech.domain.model.RolePermission;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.model.base.AuthEntity;
import no.cloudconsulting.agritech.domain.model.base.BaseEntity;
import no.cloudconsulting.agritech.domain.model.base.IAmParentOf;
import no.cloudconsulting.agritech.domain.repositories.IRepository;
import no.cloudconsulting.agritech.domain.repositories.RolePermissionRepository;
import no.cloudconsulting.agritech.helpers.Permissions;
import no.cloudconsulting.agritech.helpers.SpringHelpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by denz0x13 on 12.11.15.
 */
@Transactional
public abstract class BaseService<T extends BaseEntity, R extends IRepository<T>> {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    MutableAclService mutableAclService;

    @Autowired
    RolePermissionRepository rolePermissionRepository;

    @Autowired
    RoleService roleService;

    public static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public AuthEntity getCurrentUser() {
        try {
            return ((AuthEntity) (SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        } catch (Exception e) {
            return userManagementService.loginSA();
        }

    }


    public PrincipalSid getPrincipalSid() {
        AuthEntity user = getCurrentUser();
        if (user == null) {
            return UserManagementService.getSASid();
        }

        return new PrincipalSid(user.getUsername());
    }

    public Boolean isPermitted(String perm) {
        AuthEntity user = getCurrentUser();
        if (user == null) {
            return false;
        }
        Optional<? extends GrantedAuthority> _f = user.getAuthorities().
                stream().filter(grantedAuthority -> grantedAuthority.getAuthority().equals(perm)).findAny();

        return _f.isPresent();
    }

    public Boolean hasRole(String... roles) {
        AuthEntity user = getCurrentUser();
        if (user == null) {
            return false;
        }
        final List _roles = Arrays.asList(roles);
        Optional<? extends GrantedAuthority> _f = user.getAuthorities().
                stream().filter(grantedAuthority -> _roles.contains(grantedAuthority.getAuthority())).findAny();

        return _f.isPresent();
    }

    public List<Role> getRoles() {
        AuthEntity user = getCurrentUser();
        if (user == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(user.getAuthRoles());
    }

    public Boolean isAdmin() {
        return hasRole(Authorities.ADMIN.getRoleName());
    }


    public Boolean isPermittedByRole(Permission... permissions) {
        if (isAdmin()) {
            return true;
        }
        List<Integer> _masks = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            _masks.add(permissions[i].getMask());
        }
        if (_masks.size() < 1) {
            return false;
        }
        for (Role role : getRoles()) {
            List<RolePermission> rp = rolePermissionRepository.findByRoleIdAndClassNameAndMask(
                    role.getId(),
                    getModelName(),
                    _masks);
            if (rp.size() > 0) {
                return true;
            }
        }
        return false;
    }


    public Boolean isPermittedByRole(AuthEntity _user, Class klazz, Permission... permissions) {
        if (isAdmin()) {
            return true;
        }
        List<Integer> _masks = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            _masks.add(permissions[i].getMask());
        }
        if (_masks.size() < 1) {
            return false;
        }
        List<Role> roles = new ArrayList<>(_user.getAuthRoles());
        for (Role role : roles) {
            List<RolePermission> rp = rolePermissionRepository.findByRoleIdAndClassNameAndMask(
                    role.getId(),
                    klazz.getName(),
                    _masks);
            if (rp.size() > 0) {
                return true;
            }
        }
        return false;
    }



    public Boolean isPermittedRead(Long id) {
        return isPermittedByRole(BasePermission.READ, BasePermission.ADMINISTRATION);
        //return isPermitted(id, Arrays.asList(BasePermission.READ, BasePermission.ADMINISTRATION));
    }

    public Boolean isPermitted(Long id, Permission permission) {
        List _r = isPermitted(Arrays.asList(id), permission);
        return _r.size() > 0;
    }

    public Boolean isPermitted(Long id, List<Permission> permission) {
        List _r = isPermitted(Arrays.asList(id), permission);
        return _r.size() > 0;
    }

    public List<Long> isPermitted(List<Long> ids, Permission permission) {
        return isPermitted(ids, Arrays.asList(permission));
    }

    public List<Long> isPermitted(List<Long> ids, List<Permission> permission) {

        List<Long> _result = new ArrayList<>();
        final PrincipalSid _sid = getPrincipalSid();
        final List<Sid> _sids = new ArrayList<>();
        _sids.add(_sid);
        getRoles().forEach(r -> {
            _sids.add(new PrincipalSid(r.getName()));
        });

        if (_sid == null) {
            return _result;
        }

        Class klazz = typeToken.getRawType();

        ids.stream().forEach(id -> {
            ObjectIdentity oi = new ObjectIdentityImpl(klazz, id);
            try {
                MutableAcl acl = (MutableAcl) getAclService().readAclById(oi, _sids);
                if (acl.isGranted(permission, Arrays.asList(_sid), false)) {
                    _result.add(id);
                }

            } catch (NotFoundException ex) {

            }
        });
        return _result;
    }

    public List<Long> isPermitted(List<Permission> permissions) {
        return isPermitted(getRepository().findAllIds(), permissions);
    }


    public abstract R getRepository();

    public MutableAclService getAclService() {
        return mutableAclService;
    }


    private final TypeToken<T> typeToken = new TypeToken<T>(getClass()) {
    };
    private final Type type = typeToken.getType();

    @SuppressWarnings("unchecked")
    public String getModelName() {
        return type.getTypeName();
    }

    public Class<T> getModelClass() {
        return (Class<T>) typeToken.getRawType();
    }

    public static final Integer DEF_LIMIT = 10000;
    public static final Integer DEF_OFFSET = 0;

    public Predicate JoinPredicates(CriteriaQuery<?> query, ArrayList<Predicate> predicates) {
        return query.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
    }

    public Sort getSort() {
        return null;
    }

    protected ArrayList<Predicate> MakePredicates(HashMap<String, Object> params, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        ArrayList<Predicate> _predicates = new ArrayList<>();
        _predicates.add(query.where().getRestriction());
        return _predicates;
    }

    @SuppressWarnings("unchecked")
    public final Specification<T> makeSpecs(final HashMap<String, Object> params) {
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                ArrayList<Predicate> _predicates = MakePredicates(params, root, query, cb);
                if (params.containsKey("ids")) {
                    Collection<Long> _ids = (List) params.get("ids");
                    _predicates.add(cb.toLong(root.get("id")).in(_ids));
                }
                return JoinPredicates(query, _predicates);
            }
        };
    }


    public HashMap<String, Object> filterParams(HashMap<String, Object> params) {
        return filterParams(params, Arrays.asList(BasePermission.READ, BasePermission.ADMINISTRATION));
    }

    public HashMap<String, Object> filterParams(HashMap<String, Object> params, List<Permission> permissions) {
        if (params.containsKey("ids")) {
            if (params.get("ids") instanceof List) {
                List<Long> _ids = (List<Long>) params.get("ids");
                List<Long> _p_ids = isPermitted(_ids, permissions);
                if (_p_ids.size() < 1) {
                    _p_ids.add(0L);
                }
                params.replace("ids", _p_ids);
                return params;
            }
        }
        params.put("ids", isPermitted(permissions));
        return params;
    }

    public Page<T> findAll(HashMap<String, Object> params) {
        if (isPermittedByRole(BasePermission.READ, BasePermission.ADMINISTRATION)) {
            return getRepository().findAll(makeSpecs(params), getPage(params));
        }
        return new PageImpl<T>(new ArrayList<T>());
    }

    public Page<T> findAll(HashMap<String, Object> params, Pageable pageable) {
        if (isPermittedByRole(BasePermission.READ, BasePermission.ADMINISTRATION)) {
            return getRepository().findAll(makeSpecs(params), pageable);
        }
        return new PageImpl<T>(new ArrayList<T>());
    }

    public List<T> findAll() {

        return (List) getRepository().findAll(isPermitted(Arrays.asList(BasePermission.READ, BasePermission.ADMINISTRATION)));
    }

    public List<T> findAll(List<Long> ids) {
        return (List) getRepository().findAll(isPermitted(ids, Arrays.asList(BasePermission.READ, BasePermission.ADMINISTRATION)));
    }


    public Long count(HashMap<String, Object> params) {
        return getRepository().count(makeSpecs(params));
    }

    Pageable getPage(HashMap<String, Object> params) {
        Integer limit = DEF_LIMIT;
        Integer offset = DEF_OFFSET;
        if (params.containsKey("limit")) {
            limit = (Integer) params.get("limit");
        }
        if (params.containsKey("offset")) {
            offset = (Integer) params.get("offset");
        }

        if (getSort() != null) {
            return new PageRequest(offset, limit, getSort());
        }
        Pageable page = new PageRequest(offset, limit);
        return page;
    }


    public T findOne(Long id) throws ServiceException, AccessDeniedException {
        if (!isPermittedRead(id)) {
            throw new AccessDeniedException("Access denied");
        }
        T _result = getRepository().findOne(id);
        if (_result == null) {
            throw new ServiceException(SYSTEM_ERROR.OBJECT_NOT_FOUND, id, getModelName());
        }
        return _result;
    }


    public T save(T entity) {
        return getRepository().save(entity);
    }

    public Iterable<T> save(List<T> entities) {
        return getRepository().save(entities);
    }


    public void delete(Long id) {
        if (canDelete(id)) {
            getRepository().delete(id);
        }
    }

    public void delete(List<Long> ids) {
        getRepository().delete(getRepository().findAll(canDelete(ids)));
    }

    public abstract T newEntity();


    public T create(JsonNode node) throws ServiceException {
        T t = newEntity();
        t = merge(t, node);
        return save(t);
    }

    public T update(Long id, JsonNode node) throws ServiceException {
        T t = merge(findOne(id), node);
        t.setId(id);
        return save(t);
    }

    public T updateIgnoringNulls(Long id, T sourse) throws ServiceException {
        T t = mergeIgnoringNulls(findOne(id), sourse);
        return save(t);
    }

    public T mergeIgnoringNulls(T source, T target) {
        SpringHelpers.copyProperties(source, target, false);
        return target;
    }

    public T merge(T entity, JsonNode node) throws ServiceException {
        return entity;
    }

    public boolean canDelete(Long id) {
        return isPermittedByRole(BasePermission.DELETE, BasePermission.ADMINISTRATION);
    }

    public List<Long> canDelete(List<Long> ids) {
        if (isPermittedByRole(BasePermission.DELETE, BasePermission.ADMINISTRATION)) {
            return ids;
        }
        return new ArrayList<>();
        //return ids.stream().filter(id -> canDelete(id)).collect(Collectors.toList());
    }

    public List<Long> canDeleteEach(List<Long> ids) {
        return ids.stream().filter(id -> canDelete(id)).collect(Collectors.toList());
    }


    public boolean canCreate() {
        return isPermittedByRole(BasePermission.CREATE, BasePermission.ADMINISTRATION);
    }

    public Boolean canCreate(Long parentId) {
        return true;
    }


    public boolean canUpdate(Long id) {
        return isPermittedByRole(BasePermission.WRITE, BasePermission.ADMINISTRATION);
        //return canUpdate(Arrays.asList(id)).contains(id);
    }

    public List<Long> canUpdate(List<Long> ids) {
        if (isPermittedByRole(BasePermission.WRITE, BasePermission.ADMINISTRATION)) {
            return ids;
        }
        return new ArrayList<>();
        //return isPermitted(ids, BasePermission.WRITE);
    }

    public T treeToEntity(JsonNode node) throws ServiceException {
        return treeToEntity(node, null);
    }

    public T treeToEntity(JsonNode node, String key) throws ServiceException {
        JsonNode jsonEntity;
        if (key != null) {
            if (!node.has(key)) {
                throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
            }
        }
        if (key != null) {
            jsonEntity = node.get(key);
        } else {
            jsonEntity = node;
        }

        try {
            T entity = objectMapper.treeToValue(jsonEntity, (Class<T>) typeToken.getRawType());
            return entity;
        } catch (JsonProcessingException e) {
            throw new ServiceException(e);
        }
    }

    public T create(T entity) throws ServiceException {
        T _r = save(merge(newEntity(), entity));
        grantPermission(_r, BasePermission.ADMINISTRATION);
        return _r;
    }

    public T create(BaseApiPojo entity) throws ServiceException {
        T _r = save(merge(newEntity(), entity));
        grantPermission(_r, BasePermission.ADMINISTRATION);
        return _r;
    }

    public void grantToSystemService(T entity) {
        grantPermission(entity, new PrincipalSid(UserManagementService.SYSTEM_SERVICE_USERNAME), BasePermission.ADMINISTRATION);
    }

    public void grantPermission(T entity, Permission permission) {
        grantPermission(entity, getPrincipalSid(), permission);
    }

    public void grantToRole(Role role, Permission permission) {
        roleService.grantToRole(getModelName(), role, permission);
    }

    public void grantToRole(Long roleId, Permission permission) throws ServiceException {
        roleService.grantToRole(getModelName(), roleId, permission);
    }

    public void grantToRole(Long roleId, String permission) throws ServiceException {
        Permission _p = Permissions.fromString(permission);
        if (_p == null) {
            return;
        }
        grantToRole(roleId, _p);
    }

    //@Transactional(propagation = Propagation.NESTED)
    public void grantPermission(T entity, Sid sid, Permission permission) {
        ObjectIdentity oi = new ObjectIdentityImpl(typeToken.getRawType(), entity.getId());
        MutableAcl acl = null;

        try {
            acl = (MutableAcl) getAclService().readAclById(oi);
        } catch (NotFoundException nfe) {
            acl = getAclService().createAcl(oi);
        }
        acl.insertAce(acl.getEntries().size(), permission, sid, true);
        getAclService().updateAcl(acl);

        if (!sid.equals(UserManagementService.getSASid())) {
            grantToSystemService(entity);
        }
        grantToChildren(entity, sid, permission);
    }

    public void grantToChildren(T entity, Sid sid, Permission permission) {
        getChildren(entity).stream().forEach(p -> grantPermission(entity, sid, permission));
    }

    //TODO need to implement check access right
    //@Transactional(propagation = Propagation.NESTED)
    public void grantToUser(T entity, AuthEntity user, Permission permission) {
        ObjectIdentity oi = new ObjectIdentityImpl(typeToken.getRawType(), entity.getId());
        PrincipalSid sid = new PrincipalSid(user.getUsername());
        MutableAcl acl = null;

        try {
            acl = (MutableAcl) getAclService().readAclById(oi);
        } catch (NotFoundException nfe) {
            acl = getAclService().createAcl(oi);
        }
        acl.insertAce(acl.getEntries().size(), permission, sid, true);
        getAclService().updateAcl(acl);
        grantToChildren(entity, sid, permission);
    }

    public Set<? extends BaseEntity> getChildren(T entity) {
        if (entity instanceof IAmParentOf) {
            if (((IAmParentOf) entity).hasChildren()) {
                return ((IAmParentOf) entity).getChildren();
            }
        }
        return Collections.emptySet();
    }

    public T update(Long id, T entity) throws ServiceException {
        T t = findOne(id);
        t = merge(t, entity);
        return save(t);
    }

    public T update(Long id, BaseApiPojo pojo) throws ServiceException {
        T t = findOne(id);
        t = merge(t, pojo);
        return save(t);
    }

    public T merge(T oldEntity, T newEntity) throws ServiceException {
        try {
            SpringHelpers.copyProperties(newEntity, oldEntity, false, "id");
        } catch (Exception e) {
            throw new ServiceException(e);
        }
        return oldEntity;
    }

    public T merge(T oldEntity, BaseApiPojo pojo) throws ServiceException {
        try {
            JsonNode json = pojo.asJson();
            T ne = objectMapper.treeToValue(json, getModelClass());
            SpringHelpers.copyProperties(ne, oldEntity, false, "id");
            //return (T) CopyObjectFields.copy(newEntity,oldEntity);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException(e);
        }
        return oldEntity;
    }
}
