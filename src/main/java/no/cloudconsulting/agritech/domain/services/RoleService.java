package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.config.aaa.Authorities;
import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import no.cloudconsulting.agritech.domain.model.Role;
import no.cloudconsulting.agritech.domain.model.RolePermission;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.model.api.RoleApiPojo;
import no.cloudconsulting.agritech.domain.repositories.RoleRepository;
import no.cloudconsulting.agritech.domain.repositories.UsersRolesRepository;
import no.cloudconsulting.agritech.helpers.Permissions;
import no.cloudconsulting.agritech.helpers.SpringHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.Permission;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by denz0x13 on 14.12.16.
 */
@Service
public class RoleService extends BaseService<Role, RoleRepository> {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    UsersRolesRepository usersRolesRepository;

    @Autowired
    RolifyEntityService rolifyEntityService;

    @Override
    public RoleRepository getRepository() {
        return roleRepository;
    }

    @Override
    public Role newEntity() {
        return new Role();
    }

    @Override
    public boolean canCreate() {
        return hasRole(Authorities.ADMIN.getRoleName());
    }

    public boolean canGrant(String className) {
        if (hasRole(Authorities.ADMIN.getRoleName())) {
            return true;
        }

        return false;
    }

    public void grantToRoleEntity(String entityName, Long roleId, String permission,Boolean grant) throws ServiceException {
        if (!rolifyEntityService.isValidEntity(entityName)) {
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }
        Role role = findOne(roleId);
        Permission _p = Permissions.fromString(permission);
        if (_p == null) {
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }
        if(grant) {
            grantToRole(rolifyEntityService.fromString(entityName).klazz.getName(), role, _p);
        }else{
            removeFromRole(rolifyEntityService.fromString(entityName).klazz.getName(), role, permission);
        }
    }



    public void grantToRole(String className, Long roleId, Permission permission) {
        Role role = roleRepository.findOne(roleId);
        if (role == null) {

            return;
        }
        grantToRole(className, role, permission);
    }


    public void removeFromRole(String className, Role role, String permission) {
        Permission _p = Permissions.fromString(permission);
        if (_p == null) {
            logger.error("Invalid permission %s", permission);
            return;
        }
        rolePermissionRepository.deleteByRoleIdAndClassNameAndMask(role.getId(), className, _p.getMask());
        Class chClass = SpringHelpers.getChildClass(className);
        if (chClass != null) {
            if (rolifyEntityService.isRolify(chClass)) {
                removeFromRole(chClass.getName(), role, permission);
            }
        }
    }

    public void grantToRole(String className, Role role, String permission) {
        Permission _p = Permissions.fromString(permission);
        if (_p == null) {
            logger.error("Invalid permission %s", permission);
            return;
        }
        grantToRole(className, role, _p);
    }

    public void grantToRole(String className, Role role, Permission permission) {
        List<RolePermission> _roles = rolePermissionRepository.findByRoleIdAndClassNameAndMask(role.getId(), className, permission.getMask());
        if (_roles.size() < 1) {
            RolePermission rp = new RolePermission();
            rp.setClassName(className);
            rp.setGranting(true);
            rp.setMask(permission.getMask());
            rp.setRole(role);
            rp = rolePermissionRepository.save(rp);
        }
        Class chClass = SpringHelpers.getChildClass(className);
        if (chClass != null) {
            if (rolifyEntityService.isRolify(chClass)) {
                grantToRole(chClass.getName(), role, permission);
            }
        }
    }

    @Override
    public boolean canUpdate(Long id) {
        if (!hasRole(Authorities.ADMIN.getRoleName())) {
            return false;
        }

        if (isMandatoryRole(id)) {
            return false;
        }

        return true;
    }

    public List<String> systemRoles() {
        return Arrays.asList(Authorities.ADMIN.getRoleName(),
                Authorities.USER.getRoleName(),
                Authorities.ANONYMOUS.getRoleName()
        );
    }

    @Override
    public boolean canDelete(Long id) {
        if (!hasRole(Authorities.ADMIN.getRoleName())) {
            return false;
        }

        if (usersRolesRepository.findAllByRoleId(id).size() > 0) {
            return false;
        }

        if (isMandatoryRole(id)) {
            return false;
        }

        return true;
    }

    public Boolean isMandatoryRole(Long id) {
        Role role = roleRepository.findOne(id);
        if (role == null) {
            return false;
        }

        if (systemRoles().contains(role.getName())) {
            return false;
        }

        return false;
    }

    @Override
    public Role create(BaseApiPojo entity) throws ServiceException {
        if (entity instanceof RoleApiPojo) {
            return create((RoleApiPojo) entity);
        }
        return super.create(entity);
    }

    public Role create(RoleApiPojo entity) throws ServiceException {
        Role role = roleRepository.findFirstByName(entity.name);
        if (role != null) {
            return role;
        }
        Role newRole = super.create(entity);
        updatePermissionsToRole(newRole, entity);
        return newRole;
    }

    @Override
    public Role update(Long id, BaseApiPojo entity) throws ServiceException {
        if (entity instanceof RoleApiPojo) {
            return update(id, (RoleApiPojo) entity);
        }
        return super.update(id, entity);
    }

    public Role update(Long id, RoleApiPojo entity) throws ServiceException {
        Role role = super.update(id, entity);
        rolePermissionRepository.deleteByRoleId(id);
        updatePermissionsToRole(role, entity);
        return role;
    }

    private void updatePermissionsToRole(final Role role, RoleApiPojo entity) {
        if (entity.entityPermissions != null) {

            entity.entityPermissions.stream()
                    .filter(pm -> rolifyEntityService.isValidEntity(pm.entityName))
                    .forEach(pm -> {
                        pm.permissions.stream().forEach(sp -> {
                            RolifyEntityService.RolifyEntry re = rolifyEntityService.fromString(pm.entityName);
                            grantToRole(re.klazz.getName(), role, sp);
                        });

                    });
        }
    }
}
