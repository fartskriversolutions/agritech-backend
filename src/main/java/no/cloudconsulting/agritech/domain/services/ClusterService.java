package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.repositories.ClusterRepository;
import no.cloudconsulting.agritech.domain.repositories.FarmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by denz0x13 on 28.12.16.
 */
@Service
public class ClusterService extends ULBaseService<Cluster, ClusterRepository> {

    @Autowired
    ClusterRepository clusterRepository;

    @Autowired
    FarmRepository farmRepository;

    @Override
    public ClusterRepository getRepository() {
        return clusterRepository;
    }

    @Override
    public Cluster newEntity() {
        return new Cluster();
    }

    @Override
    public List<Long> canDelete(List<Long> ids) {
        if(farmRepository.countByClusterId(ids) == ids.size()){
            return new ArrayList<>();
        }
        return ids.stream().filter(id -> canDelete(id)).collect(Collectors.toList());
        //return super.canDelete(ids);
    }

    @Override
    public boolean canDelete(Long id) {
        if(farmRepository.countByClusterId(Arrays.asList(id)) > 0){
            return false;
        }
        return super.canDelete(id);
    }

    @Override
    public Set<Farm> getChildren(Cluster entity) {
        return new HashSet<>(farmRepository.findByClusterId(entity.getId()));
    }
}
