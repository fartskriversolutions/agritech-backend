package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import no.cloudconsulting.agritech.domain.model.Cluster;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.model.api.FarmApiPojo;
import no.cloudconsulting.agritech.domain.model.base.BaseEntity;
import no.cloudconsulting.agritech.domain.repositories.ClusterRepository;
import no.cloudconsulting.agritech.domain.repositories.FarmFieldRepository;
import no.cloudconsulting.agritech.domain.repositories.FarmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by denz0x13 on 28.12.16.
 */
@Service
public class FarmService extends ULBaseService<Farm,FarmRepository> {

    @Autowired
    FarmRepository farmRepository;

    @Autowired
    ClusterRepository clusterRepository;

    @Autowired
    ClusterService clusterService;

    @Autowired
    FarmFieldRepository farmFieldRepository;

    @Override
    public FarmRepository getRepository() {
        return farmRepository;
    }

    @Override
    public Farm newEntity() {
        return new Farm();
    }


    @Override
    public void checkParent(Farm entity, BaseApiPojo pojo) throws ServiceException {
        if(!(pojo instanceof FarmApiPojo)){
            return;
        }

        FarmApiPojo farmApiPojo = (FarmApiPojo) pojo;
        if(farmApiPojo.clusterId == null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }

        Cluster cluster = clusterService.findOne(farmApiPojo.clusterId);

        entity.setCluster(cluster);
    }


    @Override
    public BaseEntity extractParent(BaseApiPojo pojo) throws ServiceException {
        if(!(pojo instanceof FarmApiPojo)){
            return null;
        }
        FarmApiPojo farmApiPojo = (FarmApiPojo) pojo;
        if(farmApiPojo.clusterId == null){
            return null;
        }
        Cluster cluster = clusterService.findOne(farmApiPojo.clusterId);
        farmApiPojo.clusterId = null;
        return cluster;
    }

    @Override
    public Farm setParent(Farm entity, BaseEntity parent) throws ServiceException {
        entity.setCluster((Cluster) parent);
        entity = farmRepository.save(entity);
        return entity;
    }

    @Override
    protected ArrayList<Predicate> MakePredicates(HashMap<String, Object> params, Root<Farm> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        ArrayList<Predicate> _predicates = super.MakePredicates(params, root, query, cb);

        if(params.get("clusterId")!=null){
            _predicates.add(cb.toLong(root.get("clusterId")).in(params.get("clusterId")));
        }

        return _predicates;
    }

    @Override
    public boolean canDelete(Long id) {
        if(farmFieldRepository.countByFarmId(Arrays.asList(id))>0){
            return false;
        }
        return super.canDelete(id);
    }

    @Override
    public List<Long> canDelete(List<Long> ids) {
        if(farmFieldRepository.countByFarmId(ids) == ids.size()){
            return new ArrayList<>();
        }
        return ids.stream().filter(id -> canDelete(id)).collect(Collectors.toList());
        //return super.canDelete(ids);
    }
}
