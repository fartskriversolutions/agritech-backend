package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import no.cloudconsulting.agritech.domain.model.Activity;
import no.cloudconsulting.agritech.domain.model.ActivityScheduler;
import no.cloudconsulting.agritech.domain.model.api.ActivitySchedulerApiPojo;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.repositories.ActivitySchedulerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by denz0x13 on 11.01.17.
 */
@Service
public class ActivitySchedulerService extends BaseService<ActivityScheduler,ActivitySchedulerRepository> {
    @Autowired
    ActivitySchedulerRepository activitySchedulerRepository;

    @Autowired
    ActivityService activityService;


    @Override
    public ActivitySchedulerRepository getRepository() {
        return activitySchedulerRepository;
    }

    @Override
    public ActivityScheduler newEntity() {
        return new ActivityScheduler();
    }


    @Override
    public ActivityScheduler create(BaseApiPojo pojo) throws ServiceException {
        if(pojo instanceof ActivitySchedulerApiPojo){
            return create((ActivitySchedulerApiPojo)pojo);
        }
        return super.create(pojo);
    }

    public ActivityScheduler create(ActivitySchedulerApiPojo pojo) throws ServiceException{
        if(pojo.activityId==null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }
        Activity activity = activityService.findOne(pojo.activityId);

        ActivityScheduler ac = merge(newEntity(),pojo);
        ac.setActivity(activity);
        ac = save(ac);

        return ac;

    }

    @Override
    public ActivityScheduler update(Long id, BaseApiPojo pojo) throws ServiceException {
        if(pojo instanceof ActivitySchedulerApiPojo){
            return create((ActivitySchedulerApiPojo)pojo);
        }
        return super.update(id, pojo);
    }

    public ActivityScheduler update(Long id, ActivitySchedulerApiPojo pojo) throws ServiceException {
        ActivityScheduler activityScheduler = findOne(id);
        activityScheduler = merge(activityScheduler,pojo);
        activityScheduler.setId(id);
        if(pojo.activityId!=null){
            Activity activity = activityService.findOne(pojo.activityId);
            activityScheduler.setActivity(activity);
        }
        activityScheduler = save(activityScheduler);
        return activityScheduler;
    }

    @Override
    protected ArrayList<Predicate> MakePredicates(HashMap<String, Object> params, Root<ActivityScheduler> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        ArrayList<Predicate> _predicates = super.MakePredicates(params, root, query, cb);
        if(params.containsKey("activity_id")){
            _predicates.add(cb.toLong(root.get("activityId")).in(params.get("activity_id")));
        }
        return _predicates;
    }
}
