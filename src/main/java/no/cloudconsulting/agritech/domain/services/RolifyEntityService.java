package no.cloudconsulting.agritech.domain.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import no.cloudconsulting.agritech.domain.annotations.RolifyEntity;
import no.cloudconsulting.agritech.domain.model.*;
import no.cloudconsulting.agritech.helpers.Permissions;
import no.cloudconsulting.agritech.helpers.SpringHelpers;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by denz0x13 on 23.12.16.
 */
@Service
public class RolifyEntityService {

    public class RolifyEntry{
        public String id;
        @JsonIgnore
        public Class klazz;
        public String displayName;

        public RolifyEntry(String id, Class klazz){
            this.id = id;
            this.klazz = klazz;
            this.displayName = id;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj==null){
                return false;
            }
            return obj.toString().equals(toString());
        }

        @Override
        public String toString() {
            return String.format("{\"%s\",\"%s\",\"%s\"}",id,klazz,displayName);
        }
    }

    private final static List<RolifyEntry> entityToDictionary = new ArrayList<>();

    public RolifyEntityService(){
        init();
    }

    private void init(){
        scanClasses();
    }

    private void initManual(){
        entityToDictionary.clear();
        entityToDictionary.add(new RolifyEntry("User",User.class));
        entityToDictionary.add(new RolifyEntry("Role",Role.class));
        entityToDictionary.add(new RolifyEntry("Cluster",Cluster.class));
        entityToDictionary.add(new RolifyEntry("Farm",Farm.class));
        entityToDictionary.add(new RolifyEntry("FarmField",FarmField.class));
        entityToDictionary.add(new RolifyEntry("Activity",Activity.class));
        entityToDictionary.add(new RolifyEntry("ActivityScheduler",ActivityScheduler.class));
    }

    private void scanClasses(){
        entityToDictionary.clear();
        for(Class<?> klass: SpringHelpers.getAnnotatedClasses(RolifyEntity.class)){
            RolifyEntity rolifyEntity = klass.getAnnotation(RolifyEntity.class);
            RolifyEntry re = new RolifyEntry(rolifyEntity.name(),klass);
            if(!rolifyEntity.displayName().isEmpty()){
                //TODO will implemented soon
            }
            entityToDictionary.add(re);
        }
    }

    public static RolifyEntry displayNameFor(final String className){
        return entityToDictionary.stream().filter(re -> re.klazz.getName().equals(className)).findFirst().get();
    }

    public List<RolifyEntry> getEntityToDictionary(){
        if(entityToDictionary.size() < 1){
            init();
        }
        return new ArrayList<>(this.entityToDictionary);
    }

    public Boolean isRolify(final Class klass){
        return entityToDictionary.stream().anyMatch(re -> re.klazz.equals(klass));
    }

    public List<String> getAvailPermissions(){
        return Permissions.asList();
    }

    public Boolean isValidEntity(final String en){
        return entityToDictionary.stream().filter(re -> re.id.equals(en)).findFirst().isPresent();
    }

    public RolifyEntry fromString(final String en){
         Optional<RolifyEntry> _res = entityToDictionary.stream().filter(re -> re.id.equals(en)).findFirst();
         if(_res.isPresent()) {
             return _res.get();
         }
         return null;
    }
}
