package no.cloudconsulting.agritech.domain.services;

import com.fasterxml.jackson.databind.JsonNode;
import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import no.cloudconsulting.agritech.domain.model.Role;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.model.api.UserApiPojo;
import no.cloudconsulting.agritech.domain.repositories.RoleRepository;
import no.cloudconsulting.agritech.domain.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by denz0x13 on 17.12.15.
 */
@Service
public class UserService extends BaseService<User, UserRepository> {

    private static Pattern BCRYPT_PATTERN = Pattern
            .compile("\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}");

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;


    public static boolean isEncoded(String pwd){
        if(pwd==null){
            return false;
        }
        return BCRYPT_PATTERN.matcher(pwd).matches();
    }

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }


    @Override
    public Page<User> findAll(HashMap<String, Object> params) {

        return super.findAll(params);
    }

    @Override
    public User newEntity() {
        return new User();
    }


    public void assignToRole(Long user_id,Long role_id) throws ServiceException {
        User user = userRepository.findOne(user_id);
        if(user==null){
            throw new ServiceException(SYSTEM_ERROR.OBJECT_NOT_FOUND,user_id,User.class.getSimpleName());
        }
        Role role = roleRepository.findOne(role_id);

        if(role == null){
            throw new ServiceException(SYSTEM_ERROR.OBJECT_NOT_FOUND,role_id,Role.class.getSimpleName());
        }

        user.addRole(role);
        userRepository.save(user);
    }

    @Override
    public boolean canCreate() {
        return isAdmin();
    }

    @Override
    public User create(JsonNode node) throws ServiceException {
        User user = new User();
        user = merge(user, node);
        user = getRepository().save(user);
        return user;
    }

    @Override
    public User merge(User user, JsonNode node) throws ServiceException {
        JsonNode userObj;
        if (node.has("user")) {
            userObj = node.get("user");
        } else {
            throw new ServiceException(SYSTEM_ERROR.NO_OBJECT_DATA);
        }
        if (userObj.has("login_name")) {
            user.setEmail(userObj.get("login_name").asText());
        }
        if (userObj.has("email")) {
            user.setEmail(userObj.get("email").asText());
        }
        if (userObj.has("first_name")) {
            user.setFirstName(userObj.get("first_name").asText());
        }
        if (userObj.has("last_name")) {
            user.setLastName(userObj.get("last_name").asText());
        }
        if (userObj.has("password")) {
            user.setPassword(userObj.get("password").asText());
        }
        return user;
    }


    @Override
    public User save(User entity) {
        if(entity.getPassword()!=null) {
            if (!isEncoded(entity.getPassword())) {
                entity.setPassword(passwordEncoder.encode(entity.getPassword()));
            }
        }
        return super.save(entity);
    }

    public User encodePassword(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        return entity;
    }

    @Override
    public User create(BaseApiPojo entity) throws ServiceException {
        if(entity instanceof UserApiPojo){
            return create((UserApiPojo)entity);
        }
        return super.create(entity);
    }

    @Override
    public User update(Long id, BaseApiPojo entity) throws ServiceException {
        if(entity instanceof UserApiPojo){
            return update(id,(UserApiPojo)entity);
        }
        return super.update(id, entity);
    }

    public User update(Long id, UserApiPojo entity) throws ServiceException {
        User user = findOne(id);
        String _oldPassword = user.getPassword();
        Set<Role> _oldRoles = new HashSet<>(user.getAuthRoles());

        user = merge(user,entity);
        user.setId(id);
        if(entity.password!=null){
            user.setPassword(passwordEncoder.encode(entity.password));
        }else{
            user.setPassword(_oldPassword);
        }

        if(entity.roleIds!=null){
            if(entity.roleIds.size()>0){
                List<Role> _newRoles = (List)roleRepository.findAll(entity.roleIds);
                if(_newRoles.size()<1){
                    throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
                }
                _newRoles.stream().forEach(user::addRole);
            }
        }else{
            user.setRoles(_oldRoles);
        }

        return save(user);
    }

    public User create(UserApiPojo entity) throws ServiceException {
        if(entity.password == null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }
        if(entity.roleIds==null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }
        List<Role> roles = (List)roleRepository.findAll(entity.roleIds);
        if(roles.size() < 1){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }
        User user = super.create(entity);
        user.setRoles(new HashSet<>(roles));
        user.setPassword(passwordEncoder.encode(entity.password));
        user = userRepository.save(user);
        return user;
    }
}
