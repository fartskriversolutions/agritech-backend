package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.domain.model.ActivityType;
import no.cloudconsulting.agritech.domain.model.api.ActivityTypeApiPojo;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.repositories.ActivityRepository;
import no.cloudconsulting.agritech.domain.repositories.ActivityTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by denz0x13 on 11.01.17.
 */
@Service
public class ActivityTypeService extends BaseService<ActivityType,ActivityTypeRepository> {

    @Autowired
    ActivityTypeRepository activityTypeRepository;

    @Autowired
    ActivityRepository activityRepository;

    @Override
    public ActivityTypeRepository getRepository() {
        return activityTypeRepository;
    }

    @Override
    public ActivityType newEntity() {
        return new ActivityType();
    }

    @Override
    public ActivityType create(BaseApiPojo pojo) throws ServiceException {
        if(pojo instanceof ActivityTypeApiPojo){
            return create((ActivityTypeApiPojo)pojo);
        }
        return super.create(pojo);
    }

    public ActivityType create(ActivityTypeApiPojo pojo) throws ServiceException {
        ActivityType at = activityTypeRepository.findFirstByName(pojo.name);
        if(at != null){
            return at;
        }
        return super.create(pojo);
    }

    @Override
    public boolean canDelete(Long id) {
        if(activityRepository.countByActivityTypeId(id)>0){
            return false;
        }
        return super.canDelete(id);
    }

    @Override
    public List<Long> canDelete(List<Long> ids) {
        return canDeleteEach(ids);
    }
}
