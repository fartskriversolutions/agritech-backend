package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import no.cloudconsulting.agritech.domain.model.Activity;
import no.cloudconsulting.agritech.domain.model.ActivityType;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.ActivityApiPojo;
import no.cloudconsulting.agritech.domain.model.api.ActivityTypeApiPojo;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.repositories.ActivityRepository;
import no.cloudconsulting.agritech.domain.repositories.ActivityTypeRepository;
import no.cloudconsulting.agritech.domain.repositories.FarmFieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by denz0x13 on 11.01.17.
 */
@Service
public class ActivityService extends BaseService<Activity,ActivityRepository> {
    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    ActivityTypeRepository activityTypeRepository;

    @Autowired
    ActivityTypeService activityTypeService;

    @Autowired
    FarmFieldRepository farmFieldRepository;

    @Override
    public ActivityRepository getRepository() {
        return activityRepository;
    }

    @Override
    public Activity newEntity() {
        return new Activity();
    }

    @Override
    public Activity create(BaseApiPojo pojo) throws ServiceException {
        if(pojo instanceof ActivityApiPojo){
            return create((ActivityApiPojo)pojo);
        }
        return super.create(pojo);
    }

    public Activity create(ActivityApiPojo pojo) throws ServiceException{
        if(pojo.activityTypeId==null && pojo.activityTypeName==null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }
        if(pojo.farmFieldId == null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }
        ActivityType at = null;
        if(pojo.activityTypeId!=null){
            at = activityTypeRepository.findOne(pojo.activityTypeId);
        }
        if(at==null && pojo.activityTypeName!=null){
            ActivityTypeApiPojo ap = new ActivityTypeApiPojo();
            ap.name = pojo.activityTypeName;
            at = activityTypeService.create(ap);
        }
        if(at==null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }

        FarmField ff = farmFieldRepository.findOne(pojo.farmFieldId);
        if(ff==null){
            throw  new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }

        Activity activity = merge(newEntity(),pojo);
        activity.setFarmField(ff);
        activity.setActivityType(at);
        activity = activityRepository.save(activity);

        return activity;

    }

    @Override
    public Activity update(Long id, BaseApiPojo pojo) throws ServiceException {
        if(pojo instanceof ActivityApiPojo){
            return update(id,(ActivityApiPojo)pojo);
        }
        return super.update(id, pojo);
    }

    public Activity update(Long id, ActivityApiPojo pojo) throws ServiceException {
        Activity activity = findOne(id);
        activity = merge(activity,pojo);
        activity.setId(id);
        if(pojo.activityTypeId != null){
            ActivityType at = activityTypeService.findOne(pojo.activityTypeId);
            activity.setActivityType(at);
        }
        if(pojo.farmFieldId != null){
            FarmField ff = farmFieldRepository.findOne(pojo.farmFieldId);
            activity.setFarmField(ff);
        }
        return save(activity);
    }

    @Override
    protected ArrayList<Predicate> MakePredicates(HashMap<String, Object> params, Root<Activity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        ArrayList<Predicate> _predicates = super.MakePredicates(params, root, query, cb);
        Long activityTypeId = null;
        if(params.get("activityTypeId")!=null){
            activityTypeId = (Long) params.get("activityTypeId");
        }
        if(activityTypeId==null && params.containsKey("activity_type_id")){
            activityTypeId = (Long) params.get("activity_type_id");
        }
        if(activityTypeId!=null){
            _predicates.add(cb.toLong(root.get("activityTypeId")).in(activityTypeId));
        }

        return _predicates;
    }
}
