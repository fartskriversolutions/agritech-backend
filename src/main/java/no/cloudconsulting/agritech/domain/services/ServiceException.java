package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.api.datatypes.ApiResponse;
import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by denz0x13 on 18.12.15.
 */
public class ServiceException extends Exception {
    private SYSTEM_ERROR errorCode;
    Throwable throwable;
    public ServiceException(SYSTEM_ERROR system_error){
        super(system_error.getMessage());
        this.errorCode =system_error;
    }

    public ServiceException(SYSTEM_ERROR system_error,Long objectId,String klazzName){
        super(String.format("%s. Object ID: %d, Class: %s",system_error.getMessage(),objectId,klazzName));
        this.errorCode = system_error;
    }

    public ServiceException(Throwable e){
        super(e);
        this.throwable=e;
        this.errorCode = SYSTEM_ERROR.UNKNOWN_ERROR;
    }

    public Integer getCode(){
        return errorCode.getCode();
    }

    public ResponseEntity<ApiResponse> getResponse(){
        return new ResponseEntity<ApiResponse>(
                ApiResponse.builder().errors(errorCode.getCode(),errorCode.getMessage()).build(),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    public String getMessage(){
        if(throwable!=null){
            return throwable.getMessage();
        }
        return super.getMessage();
    }
}
