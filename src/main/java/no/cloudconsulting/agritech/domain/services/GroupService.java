package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.domain.model.Group;
import no.cloudconsulting.agritech.domain.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.stereotype.Service;

/**
 * Created by denz0x13 on 08.12.16.
 */
@Service
public class GroupService extends BaseService<Group,GroupRepository> {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    MutableAclService mutableAclService;

    @Override
    public GroupRepository getRepository() {
        return groupRepository;
    }


    @Override
    public Group newEntity() {
        return new Group();
    }
}
