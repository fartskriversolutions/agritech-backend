package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import no.cloudconsulting.agritech.domain.model.Location;
import no.cloudconsulting.agritech.domain.model.User;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.model.api.ULBaseApiPojo;
import no.cloudconsulting.agritech.domain.model.base.BaseEntity;
import no.cloudconsulting.agritech.domain.model.base.ULUserAuditEntity;
import no.cloudconsulting.agritech.domain.repositories.LocationRepository;
import no.cloudconsulting.agritech.domain.repositories.ULRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.BasePermission;

/**
 * Created by denz0x13 on 28.12.16.
 */
public abstract class ULBaseService<T extends ULUserAuditEntity,R extends ULRepository<T>> extends BaseService<T,R>  {

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    UserService userService;

    public Location extractLocation(ULBaseApiPojo pojo){
        if(pojo.latitude !=null && pojo.longitude!=null){
            Location location = new Location();
            location.setLongitude(pojo.longitude);
            location.setLatitude(pojo.latitude);

            return location;
        }
        return null;
    }

    public Location saveLocation(ULBaseApiPojo pojo){
        Location location = extractLocation(pojo);
        if(location!=null){
            return locationRepository.save(location);
        }
        return null;
    }

    public T updateLocation(T entity,ULBaseApiPojo pojo){
        Location ol = locationRepository.findOne(entity.getLocationId());
        if(ol == null){
            return entity;
        }
        Location el = extractLocation(pojo);
        if(el == null){
            return entity;
        }

        ol.setLatitude(el.getLatitude());
        ol.setLongitude(el.getLongitude());
        ol = locationRepository.save(ol);

        return entity;
    }

    public User extractUser(ULBaseApiPojo pojo) throws ServiceException {
        if(pojo.userId==null){
            return null;
        }
        User user = userService.findOne(pojo.userId);
        return user;
    }

    public void checkParent(T entity,BaseApiPojo pojo) throws ServiceException {

    }

    public BaseEntity extractParent(BaseApiPojo pojo) throws ServiceException{
        return null;
    }

    public T setParent(T entity,BaseEntity parent) throws ServiceException {
        return entity;
    }

    @Override
    public T create(BaseApiPojo pojo) throws ServiceException {
        if(!(pojo instanceof ULBaseApiPojo)){
            return super.create(pojo);
        }
        T ulEntity = merge(newEntity(),pojo);
        Location location = saveLocation((ULBaseApiPojo) pojo);
        if(location == null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }

        User user = extractUser((ULBaseApiPojo)pojo);

        if(!isPermittedByRole(user,getModelClass(),BasePermission.ADMINISTRATION)){
            throw new ServiceException(SYSTEM_ERROR.NOT_PERMITTED);
        }
        ulEntity.setUser(user);
        ulEntity.setLocation(location);
        checkParent(ulEntity,pojo);
        ulEntity = save(ulEntity);
        grantToUser(ulEntity,user,BasePermission.ADMINISTRATION);
        grantPermission(ulEntity, BasePermission.ADMINISTRATION);
        return ulEntity;
    }

    @Override
    public T update(Long id, BaseApiPojo pojo) throws ServiceException {
        if(!(pojo instanceof ULBaseApiPojo)){
            return super.update(id,pojo);
        }
        User user = extractUser((ULBaseApiPojo)pojo);
        BaseEntity parent = extractParent(pojo);
        ((ULBaseApiPojo) pojo).userId = null;
        T ulEntity = super.update(id, pojo);
        if(parent!=null){
            ulEntity = setParent(ulEntity,parent);
        }
        ulEntity = updateLocation(ulEntity,(ULBaseApiPojo) pojo);

        if(user!=null) {
            if(user.getId()!=ulEntity.getUserId()) {
                if (!isPermittedByRole(user, getModelClass(), BasePermission.ADMINISTRATION)) {
                    throw new ServiceException(SYSTEM_ERROR.NOT_PERMITTED);
                }
                ulEntity.setUser(user);
                ulEntity = save(ulEntity);
                grantToUser(ulEntity, user, BasePermission.ADMINISTRATION);
            }
        }
        return ulEntity;
    }
}
