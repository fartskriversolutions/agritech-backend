package no.cloudconsulting.agritech.domain.services;

import no.cloudconsulting.agritech.dictionaries.SYSTEM_ERROR;
import no.cloudconsulting.agritech.domain.model.Farm;
import no.cloudconsulting.agritech.domain.model.FarmField;
import no.cloudconsulting.agritech.domain.model.api.BaseApiPojo;
import no.cloudconsulting.agritech.domain.model.api.FarmFieldApiPojo;
import no.cloudconsulting.agritech.domain.model.base.BaseEntity;
import no.cloudconsulting.agritech.domain.repositories.FarmFieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by denz0x13 on 29.12.16.
 */
@Service
public class FarmFieldService extends ULBaseService<FarmField,FarmFieldRepository> {

    @Autowired
    FarmFieldRepository farmFieldRepository;

    @Autowired
    FarmService farmService;

    @Override
    public FarmFieldRepository getRepository() {
        return farmFieldRepository;
    }

    @Override
    public FarmField newEntity() {
        return new FarmField();
    }


    @Override
    public void checkParent(FarmField entity, BaseApiPojo pojo) throws ServiceException {
        if(!(pojo instanceof FarmFieldApiPojo)){
            return;
        }

        FarmFieldApiPojo ffApiPojo = (FarmFieldApiPojo) pojo;
        if(ffApiPojo.farmId == null){
            throw new ServiceException(SYSTEM_ERROR.INVALID_OBJECT_DATA);
        }

        Farm farm = farmService.findOne(ffApiPojo.farmId);

        entity.setFarm(farm);
    }


    @Override
    public BaseEntity extractParent(BaseApiPojo pojo) throws ServiceException {
        if(!(pojo instanceof FarmFieldApiPojo)){
            return null;
        }
        FarmFieldApiPojo ffApiPojo = (FarmFieldApiPojo) pojo;
        if(ffApiPojo.farmId == null){
            return null;
        }
        Farm farm = farmService.findOne(ffApiPojo.farmId);
        ffApiPojo.farmId = null;
        return farm;
    }

    @Override
    public FarmField setParent(FarmField entity, BaseEntity parent) throws ServiceException {
        entity.setFarm((Farm) parent);
        entity = farmFieldRepository.save(entity);
        return entity;
    }

    @Override
    protected ArrayList<Predicate> MakePredicates(HashMap<String, Object> params, Root<FarmField> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        ArrayList<Predicate> _predicates = super.MakePredicates(params, root, query, cb);

        if(params.get("farmId")!=null){
            _predicates.add(cb.toLong(root.get("farmId")).in(params.get("farmId")));
        }

        return _predicates;
    }
}
