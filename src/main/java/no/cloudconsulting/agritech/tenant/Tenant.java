package no.cloudconsulting.agritech.tenant;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by evilsun.
 */
@Entity(name = "tenants")
public class Tenant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "tenant_id")
    public String tenantId;

    @NotNull
    public boolean enabled;


    public Tenant(){

    }

    public Tenant(String tenantId,boolean enabled){
        this.tenantId = tenantId;
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
