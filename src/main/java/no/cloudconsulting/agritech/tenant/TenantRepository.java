package no.cloudconsulting.agritech.tenant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by evilsun.
 */

@Repository
interface TenantRepository extends JpaRepository<Tenant, Long> {

    List<Tenant> findByEnabledTrue();
    List<Tenant> findAll();
    Tenant getByTenantId(String tenantId);
}
