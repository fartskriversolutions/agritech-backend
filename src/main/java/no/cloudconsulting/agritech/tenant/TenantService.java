package no.cloudconsulting.agritech.tenant;

import no.cloudconsulting.agritech.domain.services.UserManagementService;
import no.cloudconsulting.agritech.helpers.TenantHelper;
import no.cloudconsulting.agritech.hibernate.CustomMultiTenantSpringLiquibase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.acls.model.AclCache;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by evilsun.
 * at 9/25/15
 */
@Service
@Transactional(readOnly = true)
public class TenantService {

    public enum TenantStatus {
        PRESENT(0x0000),
        NO_SCHEMA(0x0001),
        NO_TENANT(0x0010),
        NOT_PRESENT(0x0011)
        ;
        private int val;

        TenantStatus(int i) {
            this.val = i;
        }
    }

    private static final Logger log = LoggerFactory.getLogger(TenantService.class);

    @Autowired
    TenantRepository tenantRepository;

    @Autowired
    DataSource dataSource;

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    AclCache aclCache;

    @Value("${spring.datasource.url}")
    private String datasourceUrl;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    public List<Tenant> getEnabledTenants() {
        return tenantRepository.findByEnabledTrue();
    }

    public List<Tenant> getAllTenants(){ return tenantRepository.findAll();}

    @Transactional
    public void createTenant(String tenantId, boolean enabled) {
        if (tenantRepository.getByTenantId(tenantId) == null) {
            tenantRepository.save(new Tenant(tenantId, enabled));
            CustomMultiTenantSpringLiquibase customMultiTenantSpringLiquibase = new CustomMultiTenantSpringLiquibase(datasourceUrl, username, password, driverClassName);
            customMultiTenantSpringLiquibase.setDataSource(dataSource);
            customMultiTenantSpringLiquibase.setChangeLog("classpath:liquibase/update.sql");
            customMultiTenantSpringLiquibase.createTenantSchema(tenantId);
            customMultiTenantSpringLiquibase.destroy();

            return;
        }
        log.warn("Tenant with name {} already exists", tenantId);
    }

    @Transactional
    public void setupTenant(String tenantId){
        if(!getStatus(tenantId).equals(TenantStatus.PRESENT)){
            return;
        }
        TenantHelper.setTenant(tenantId);
        userManagementService.checkUsers();
        TenantHelper.setDefaultTenant();
    }

    @Transactional
    public void dropTenant(String tenantId) {
        Tenant tenant = tenantRepository.getByTenantId(tenantId);
        if (tenant != null) {
            TenantHelper.setTenant(tenantId);
            aclCache.clearCache();
            TenantHelper.setDefaultTenant();
            tenantRepository.delete(tenant.getId());
            try (PreparedStatement stmt = dataSource.getConnection().prepareStatement("drop schema " + escapeSQL(tenantId) + " cascade")) {
                stmt.execute();
            } catch (SQLException e) {
                log.error("Exception during schema " + tenantId + " creation ", e);
            }
            return;
        }
        log.warn("Tenant with name {} is NOT exists", tenantId);
    }


    public TenantStatus getStatus(String tenantId){
        Tenant tenant = tenantRepository.getByTenantId(tenantId);
        Boolean isExists = isTenantSchemaExists(tenantId);

        if((tenant==null) && !isExists){
            return TenantStatus.NOT_PRESENT;
        }

        if((tenant==null) && isExists){
            return TenantStatus.NO_TENANT;
        }

        if((tenant != null) && !isExists){
            return TenantStatus.NO_SCHEMA;
        }

        return TenantStatus.PRESENT;
    }

    public Boolean isTenantSchemaExists(String tenantId){
        String _sql = String.format("SELECT count(schema_name) FROM information_schema.schemata WHERE schema_name = '%s'",tenantId);
        try(PreparedStatement stmt = dataSource.getConnection().prepareStatement(_sql)){
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs.getInt("count") == 1;
            }

        }catch (SQLException e){
            log.error("Error in tenant count",e);
        }
        return false;
    }

    public Boolean isOrphanedTenant(String tenantId){
        Tenant tenant = tenantRepository.getByTenantId(tenantId);
        Boolean isExists = isTenantSchemaExists(tenantId);
        return (tenant!=null) && !isExists;
    }

    public Boolean isOrphanedSchema(String tenantId){
        Tenant tenant = tenantRepository.getByTenantId(tenantId);
        Boolean isExists = isTenantSchemaExists(tenantId);
        return (tenant == null) && isExists;
    }

    public String escapeSQL(String constant) {
        if (constant == null) {
            return null;
        }
        String fixedConstant = constant;
        fixedConstant = fixedConstant.replaceAll("\\\\x00", "");
        fixedConstant = fixedConstant.replaceAll("\\\\x1a", "");
        fixedConstant = fixedConstant.replaceAll("'", "''");
        fixedConstant = fixedConstant.replaceAll("\\\\", "\\\\\\\\");
        fixedConstant = fixedConstant.replaceAll("\\\"", "\\\\\"");
        return fixedConstant;
    }

}
