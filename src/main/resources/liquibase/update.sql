--liquibase formatted sql
--changeset denz0x13:1
CREATE TABLE users
(
  id SERIAL PRIMARY KEY NOT NULL,
  email VARCHAR(255) DEFAULT '' NOT NULL,
  password VARCHAR(255) DEFAULT '',
  profile_id INTEGER,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  login_name VARCHAR(255) DEFAULT '' NOT NULL
);
CREATE UNIQUE INDEX index_users_on_email ON users (email);
CREATE UNIQUE INDEX index_users_on_login_name ON users (login_name);
--rollback drop table users

--changeset denz0x13:2
CREATE TABLE roles
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  description VARCHAR(255)
);
CREATE INDEX index_roles_on_name ON roles (name);
--rollback drop table roles


-- changeset denz0x13:3
create TABLE users_roles (
  user_id integer,
  role_id integer
);
CREATE INDEX index_users_roles_on_user_id_and_role_id ON users_roles (user_id, role_id);
-- rollback drop table user_roles

--changeset denz0x13:5
ALTER TABLE users ADD COLUMN created_by INTEGER;
ALTER TABLE users ADD COLUMN updated_by INTEGER;

ALTER TABLE roles ADD COLUMN created_by INTEGER;
ALTER TABLE roles ADD COLUMN updated_by INTEGER;

--changeset denz0x13:6
create table groups (
  id BIGSERIAL not null PRIMARY KEY,
  name VARCHAR(255),
  description VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by INTEGER,
  updated_by INTEGER
);
create index index_groups_on_name on groups (name);

create table users_groups (
  user_id INTEGER,
  group_id INTEGER
);
create INDEX index_users_groups_on_user_id_group_id on users_groups (user_id,group_id);


create table groups_roles (
  group_id INTEGER,
  role_id INTEGER
);

create INDEX index_groups_roles_on_group_id_role_id on groups_roles (group_id,role_id);



create table acl_sid(
  id bigserial not null primary key,
  principal boolean not null,
  sid varchar(100) not null,
  constraint unique_uk_1 unique(sid,principal)
);

create table acl_class(
  id bigserial not null primary key,
  class varchar(100) not null,
  constraint unique_uk_2 unique(class)
);

create table acl_object_identity(
  id bigserial primary key,
  object_id_class bigint not null,
  object_id_identity bigint not null,
  parent_object bigint,
  owner_sid bigint,
  entries_inheriting boolean not null,
  constraint unique_uk_3 unique(object_id_class,object_id_identity),
  constraint foreign_fk_1 foreign key(parent_object)references acl_object_identity(id),
  constraint foreign_fk_2 foreign key(object_id_class)references acl_class(id),
  constraint foreign_fk_3 foreign key(owner_sid)references acl_sid(id)
);

create table acl_entry(
  id bigserial primary key,
  acl_object_identity bigint not null,
  ace_order int not null,
  sid bigint not null,
  mask integer not null,
  granting boolean not null,
  audit_success boolean not null,
  audit_failure boolean not null,
  constraint unique_uk_4 unique(acl_object_identity,ace_order),
  constraint foreign_fk_4 foreign key(acl_object_identity) references acl_object_identity(id),
  constraint foreign_fk_5 foreign key(sid) references acl_sid(id)
);


-- changeset denz0x13:7

INSERT INTO users (email, password, profile_id, created_at, updated_at, first_name, last_name, login_name, created_by, updated_by) VALUES ('system.service@cloudconsulting.no', '$2a$10$9bXw7Wc1pu6l69wEMM44peVZY0UkvLonYG2rFL24CnRxQB7EfBiqa', null, '2016-12-15 16:55:53.568000', '2016-12-15 16:55:53.568000', 'System', 'Service', 'system_service', null, null);
INSERT INTO users (email, password, profile_id, created_at, updated_at, first_name, last_name, login_name, created_by, updated_by) VALUES ('admin@example.com', '$2a$10$icJKzeujPYyk8k5yDKxlWuRURJ0CW..xfm6z.88BoWy7QK06EmpSC', null, '2016-12-15 16:55:54.302000', '2016-12-15 16:55:54.302000', 'Admin', 'Super', 'admin_super', 1, 1);

INSERT INTO roles (name, created_at, updated_at, description, created_by, updated_by) VALUES ('ROLE_ADMIN', '2016-12-15 16:55:53.254000', '2016-12-15 16:55:53.254000', null, null, null);

INSERT INTO users_roles (user_id, role_id) VALUES (1, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (2, 1);

INSERT INTO acl_sid (principal, sid) VALUES (true, 'system_service');
INSERT INTO acl_sid (principal, sid) VALUES (true, 'admin_super');

INSERT INTO acl_class (class) VALUES ('no.cloudconsulting.agritech.domain.model.User');

INSERT INTO acl_object_identity (object_id_class, object_id_identity, parent_object, owner_sid, entries_inheriting) VALUES (1, 1, null, 1, true);
INSERT INTO acl_object_identity (object_id_class, object_id_identity, parent_object, owner_sid, entries_inheriting) VALUES (1, 2, null, 1, true);

INSERT INTO acl_entry (acl_object_identity, ace_order, sid, mask, granting, audit_success, audit_failure) VALUES (1, 0, 1, 16, true, false, false);
INSERT INTO acl_entry (acl_object_identity, ace_order, sid, mask, granting, audit_success, audit_failure) VALUES (2, 0, 1, 16, true, false, false);
INSERT INTO acl_entry (acl_object_identity, ace_order, sid, mask, granting, audit_success, audit_failure) VALUES (2, 1, 2, 16, true, false, false);


-- changeset denz0x13:8

create table role_permissions(
  id bigserial primary key,
  class_name varchar(255) not null,
  role_id bigint,
  mask integer not null,
  granting boolean not null DEFAULT true,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by BIGINT,
  updated_by bigint,
  constraint foreign_fk_4 foreign key(role_id) references roles(id)
);


CREATE table locations(
  id BIGSERIAL PRIMARY KEY,
  latitude DOUBLE PRECISION NOT NULL,
  longitude DOUBLE PRECISION NOT NULL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by BIGINT,
  updated_by BIGINT
);

create table clusters(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) not null,
  location_id BIGINT,
  user_id BIGINT,
  typography VARCHAR(255),
  infrastructure VARCHAR(255),
  irrigation_source varchar(255),
  soil_analysis BOOLEAN DEFAULT false,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by BIGINT,
  updated_by BIGINT,
  CONSTRAINT fk_1_location FOREIGN KEY(location_id) REFERENCES locations(id),
  CONSTRAINT fk_2_manager FOREIGN KEY(user_id) REFERENCES users(id)
);


create table farms(
  id BIGSERIAL PRIMARY KEY,
  NAME VARCHAR(255) NOT NULL,
  location_id BIGINT,
  user_id BIGINT NOT NULL,
  cluster_id BIGINT,
  enabled BOOLEAN DEFAULT TRUE,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by BIGINT,
  updated_by BIGINT,
  CONSTRAINT fk_1_location FOREIGN KEY(location_id) REFERENCES locations(id),
  CONSTRAINT fk_2_manager FOREIGN KEY(user_id) REFERENCES users(id),
  CONSTRAINT fk_3_cluster FOREIGN KEY(cluster_id) REFERENCES clusters(id)
);

create table farm_fields(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) not null,
  farm_id BIGINT,
  location_id BIGINT,
  user_id BIGINT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by BIGINT,
  updated_by BIGINT,
  CONSTRAINT fk_1_farm FOREIGN KEY(farm_id) REFERENCES farms(id),
  CONSTRAINT fk_2_manager FOREIGN KEY(user_id) REFERENCES users(id),
  CONSTRAINT fk_3_location FOREIGN KEY(location_id) REFERENCES locations(id)
);


CREATE TABLE activity_types(
  id BIGSERIAL PRIMARY KEY,
  NAME VARCHAR(255) NOT NULL UNIQUE,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by BIGINT,
  updated_by BIGINT
);

CREATE table activities(
  id BIGSERIAL PRIMARY KEY,
  NAME VARCHAR(255) not null,
  activity_type_id BIGINT,
  description TEXT,
  farm_field_id BIGINT,
  report_fields JSON,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by BIGINT,
  updated_by BIGINT,
  CONSTRAINT fk_1_activity_type FOREIGN KEY(activity_type_id) REFERENCES activity_types(id),
  CONSTRAINT fk_2_farm_field FOREIGN KEY(farm_field_id) REFERENCES farm_fields(id)
);

CREATE TABLE activity_schedulers(
  id BIGSERIAL PRIMARY KEY,
  NAME VARCHAR(255) NOT NULL,
  activity_id BIGINT,
  start_time TIMESTAMP,
  end_time TIMESTAMP,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  created_by BIGINT,
  updated_by BIGINT,
  CONSTRAINT fk_1_activity FOREIGN KEY(activity_id) REFERENCES activities(id)
);

