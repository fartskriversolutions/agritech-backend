--liquibase formatted sql

--changeset denz0x13:1
CREATE SEQUENCE tenants_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;

CREATE TABLE tenants (
  id    BIGINT                 NOT NULL DEFAULT nextval('tenants_seq'),
  tenant_id  CHARACTER VARYING(255) NOT NULL,
  enabled boolean NOT NULL default false,
  CONSTRAINT tenant_id_key UNIQUE (tenant_id),
  CONSTRAINT tenants_pkey PRIMARY KEY (id)
);
--rollback drop table tenants; drop sequence tenants_seq;


--changeset denz0x13:2
insert into tenants (tenant_id,enabled) values ('admin', true);